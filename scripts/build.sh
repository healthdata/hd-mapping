#!/bin/bash

mkdir -p dist

cp -R commit.txt config.js config_hd4dp.js config_hd4res.js config_hd4prc.js lib \
  mappingsServer.js mappings_hd4dp.sh mappings_hd4res.sh mappings_hd4prc.sh npm-shrinkwrap.json \
  package.json setProxy.js \
  web.config node_modules scripts index.js logs dist

cd dist

npm prune --production

zip -r healthdata-api.zip *

cd ..

mv dist/healthdata-api.zip healthdata-api.zip

