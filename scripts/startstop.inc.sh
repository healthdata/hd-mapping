
# always stop on errors
set -e

function usage {
  echo "Usage: $0 (start|stop)"
  exit 1
}

case "$1" in
  start)  mkdir -p logs
          cp "config_${CONFIG}.js" "config.js"
          TZ='Europe/Brussels' $FOREVER start -a -l `pwd`/logs/$FVRLOG $SCRIPT
          ;;
  stop)   $FOREVER stop $SCRIPT
          ;;
  '')     echo "Missing argument"
          usage
          ;;
  *)      echo "Unrecoognized command: $1"
          usage
          ;;
esac
