# Mapping API for HD4DP and HD4RES

## Prerequisites
Node and NPM (https://nodejs.org/)

## Building

Run `npm install` to install dependencies.

Run `npm test` to run the tests.

Run `npm run bamboo-test` to run the tests as they are run by the build server.

Run `npm run coverage` to generate code coverage.

Run `npm run codestyle` to verify code style using JSHint and JSCS.

Run `./scripts/build.sh` to create the `healthdata-api.zip` deployable.

## Running

 - unzip the deployable
 - edit `config_hd4dp.js` or `config_hd4res.js`
 - run `./mappings_hd4dp.sh start` or `./mappings_hd4res.sh start`

## Contact

For questions, please contact support@healthdata.be.

## License
Apache License, Version 2.0