/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var restify = require('restify');
var asyncParallel = require('../asyncParallel');
var fs = require('fs');
var hdFlow = require('./hdFlow');

require('../directives');

var VERSION = require('../../package').version;

function mappingsAPI(options) {

  options = options || {};

  var server = restify.createServer({ name: 'mappings server', log: options.log });

  server
    .use(restify.fullResponse())
    .use(restify.bodyParser());

  server.pre(function (req, res, next) {
    req.log.debug({ req: req }, 'start');
    return next();
  });

  server.on('after', function (req, res, route, err) {
    if (err) {
      // If a request was processed by using next(err), we log the error here.
      // This can be the case in 4xx or 5xx responses. Since this is not necessarily
      // a bug (esp. in the case of a 4xx response) we use the 'warn' log level.
      req.log.warn(err);
    }
    req.log.debug({ res: res }, 'finished');
  });

  server.on('uncaughtException', function (req, res, route, err) {
    // This case always indicates a bug, so we log the error using the 'error' log
    // level. Ideally, we would perform a graceful shutdown but this seems to be
    // tricky, because of leep-alive HTTP connections. Therefore, we simply send
    // a 500 reponse and keep going.
    if (err) {
      req.log.error(err);
    }
    res.send(500, new restify.InternalError(err.message));
    req.log.error({ res: res }, 'ERROR');
  });

  /* Route mappings */
  server.get('/mappings/status', function (req, res, next) {
    res.send(204);
    next();
  });
  server.get('/mappings/version', function (req, res, next) {
    asyncParallel({
      commit: function (cb) {
        fs.readFile('./commit.txt', 'utf8', cb);
      }
    }, function (err, results) {
      if (err) {
        return next(err);
      }
      res.send(200, {
        documentModel: VERSION,
        commit: results.commit
      });
      next();
    });
  });
  server.get('/mappings/elasticsearchIndexSettings', getIndexSettings);
  server.post('/mappings/refresh', _.partial(processMappingFlowRequest, [ 'refresh' ]));
  server.post('/mappings/merge', _.partial(processMappingFlowRequest, [ 'merge' ]));
  server.post('/mappings/importCSV', _.partial(processMappingFlowRequest, [ 'importCsv' ]));
  server.post('/mappings/importBasicDataset',
    _.partial(processMappingFlowRequest, [ 'importBasicDataset' ]));
  server.post('/mappings', _.partial(processMappingFlowRequest, []));
  return server;
}

function getIndexSettings(req, res, next) {
  res.send(200, {
    settings: {
      analysis: {
        analyzer: {
          case_insensitive_sort: {
            tokenizer: 'keyword',
            filter: [ 'lowercase' ]
          }
        }
      }
    }
  });
  next();
}

function processMappingFlowRequest(defaultActions, req, res, next) {
  if (!req.params.input || !req.params.targets) {
    return next(new restify.BadRequestError('Missing parameters "input" and "targets"'));
  }
  var input = req.params.input;
  var targets = req.params.targets;
  var actions = (defaultActions || []).concat(req.params.actions || []);
  hdFlow(actions, input, targets, function callback(err, results) {
    if (err) {
      if (err.badRequest) {
        return next(new restify.BadRequestError(err.toString()));
      } else {
        return next(new restify.InternalError(err.toString()));
      }
    }
    res.send(200, {
      targets: results
    });
    next();
  });
}

module.exports = mappingsAPI;
