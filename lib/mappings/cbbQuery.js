/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = {
  build: makeCBBQuery
};

function makeCBBQuery(f) {
  return normalizeQuery(f({
    identity: identityQuery,
    sequence: sequenceQueries,
    concept: conceptQuery,
    dataElement: dataElementQuery,
    atPosition: atPositionQuery,
    filter: filterQuery,
    select: selectQuery
  }));
}

function normalizeQuery(query) {
  return pruneConcepts(_.compact(_.flatten([ query ])));
}

function pruneConcepts(query) {
  return _.reduce(query, function (memo, step) {
    if (step.type !== 'concept') {
      return {
        conceptSoFar: memo.conceptSoFar,
        querySoFar: memo.querySoFar.concat([ step ])
      };
    }

    if (!_.isEmpty(_.without(_.pluck(memo.querySoFar, 'type'), 'concept', 'dataElement'))) {
      throw new Error('Invalid query: nested concept in ' + JSON.stringify(query));
    }
    return {
      conceptSoFar: step.concept,
      querySoFar: [ step ]
    };
  }, { querySoFar: [] }).querySoFar;
}

function identityQuery() {
  return null;
}

function sequenceQueries(queries) {
  return queries;
}

function conceptQuery(conceptName) {
  if (!conceptName) { return identityQuery(); }
  return {
    type: 'concept',
    concept: conceptName
  };
}

function dataElementQuery(dataElementName) {
  if (!dataElementName) { return identityQuery(); }
  return {
    type: 'dataElement',
    dataElement: dataElementName
  };
}

function atPositionQuery(position) {
  return {
    type: 'position',
    position: position
  };
}

function filterQuery(filterPredicate) {
  if (!filterPredicate || _.isEmpty(filterPredicate)) { return identityQuery(); }
  return {
    type: 'filter',
    filterPredicate: filterPredicate
  };
}

function selectQuery(selectCriteria) {
  if (!selectCriteria || _.isEmpty(selectCriteria)) { return identityQuery(); }
  return {
    type: 'select',
    selectCriteria: selectCriteria
  };
}
