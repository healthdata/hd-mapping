/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment');

module.exports = {
  evaluateQuery: evaluateQuery
};

function executeQuery(query, state, evalxpath) {
  _.each(query, function (queryStep) {
    if (_.has(queryStep, 'xpath')) {
      state = evalxpath(queryStep.xpath, state);
      return;
    }
    if (_.has(queryStep, 'convertTo')) {
      if (queryStep.convertTo === 'text') {
        state = _.pluck(state, 'nodeValue');
        return;
      }
      if (queryStep.convertTo === 'nothing') {
        state = _.map(state, _.constant(null));
        return;
      }
      if (queryStep.convertTo === 'date') {
        state = _.map(_.pluck(state, 'nodeValue'), _.partial(parseDate, 'YYYY-MM-DD'));
        return;
      }
      if (queryStep.convertTo === 'boolean') {
        state = _.map(_.pluck(state, 'nodeValue'), Boolean);
        return;
      }
      if (queryStep.convertTo === 'gender') {
        state = _.map(_.pluck(state, 'nodeValue'), function (data) {
          if (!data) { return null; }
          // Map allowed values for KMEHR CD-SEX list.
          // https://www.ehealth.fgov.be/standards/kmehr/content/page/tables/210/sex-codes
          // Not all options are availbale in CBB.
          if (data === 'male') { return 'M'; }
          if (data === 'female') { return 'F'; }
          if (data === 'unknown') { return 'U'; }
          return null;
        });
        return;
      }
      if (queryStep.convertTo === 'number') {
        state = _.map(_.pluck(state, 'nodeValue'), parseNumber);
        return;
      }
      if (queryStep.convertTo === 'hasResults') {
        state = [ Boolean(state.length) ];
        return;
      }
      throw new Error('unkown format: ' + queryStep.convertTo);
    }
    if (_.has(queryStep, 'mostRecent')) {
      var dateXPath = queryStep.mostRecent;
      if (_.isEmpty(state)) {
        return;
      }
      var theMaximum = _.max(state, function (node) {
        var referenceDate = parseDate('YYYY-MM-DD',
          singleText(evalxpath(dateXPath, node))).unix();
        return referenceDate;
      });
      state = [ theMaximum ];
      return;
    }
    throw new Error('Unrecognized query step ' + JSON.stringify(queryStep));
  });
  return state;
}

function compileQuery(query, compilerConfig) {
  var rootCompiler = makeQueryCompiler(compilerConfig);
  return simplifyQuery(_.reduce(query, step, rootCompiler).done());
  function step(compiler, queryStep) {
    return compiler.step(queryStep);
  }
}

function simplifyQuery(query) {
  if (!query) { return query; }
  // make deep copy before optmizing, because the optimizations rewrite the
  // query in place
  query = JSON.parse(JSON.stringify(query));
  var keepGoing = true;
  while (keepGoing) {
    var newQuery = simplifyQueryOnce(query);
    if (newQuery) {
      query = newQuery;
    } else {
      keepGoing = false;
    }
  }
  return query;
}

function simplifyQueryOnce(query) {
  return mergeConsecutiveXPaths(query) ||
    rewriteMatchesCodes(query) ||
    rewriteAfterDate(query);
}

function mergeConsecutiveXPaths(query) {
  var match = _.findIndex(query, function (step, pos) {
    return _.has(step, 'xpath') &&
      _.isObject(query[pos + 1]) &&
      _.has(query[pos + 1], 'xpath');
  });
  if (match === -1) { return null; }
  var second = query.splice(match + 1, 1)[0].xpath;
  query[match].xpath = query[match].xpath + second;
  return query;
}

function rewriteMatchesCodes(query) {
  var match = _.findIndex(query, function (step) {
    return _.has(step, 'matchesCodes');
  });
  if (match === -1) { return null; }
  var codeXPath = query[match].matchesCodes;
  var codes = query[match].codes;
  if (!_.isArray(codes) || _.isEmpty(codes)) {
    throw new Error('matchesCodes requires at least one code');
  }
  var predicates = _.map(query[match].codes, function (code) {
    return codeXPath + ' = "' + code + '"';
  });
  var predicate = predicates.join(' or ');
  query[match] = {
    xpath: '/self::node()[' + predicate + ']'
  };
  return query;
}

function rewriteAfterDate(query) {
  var match = _.findIndex(query, function (step) {
    return _.has(step, 'afterDate');
  });
  if (match === -1) { return null; }
  var dateXPath = query[match].afterDate;
  var comparisonDate = query[match].comparisonValue;
  if (!_.isString(comparisonDate) || _.isEmpty(comparisonDate)) {
    throw new Error('afterDate requires a comparison date');
  }
  var predicate = 'number(translate(' + dateXPath + ',\'-\',\'\')) > ' +
    comparisonDate.split('-').join('');
  query[match] = {
    xpath: '/self::node()[' + predicate + ']'
  };
  return query;
}

function makeQueryCompiler(config, partialQuery) {
  return {
    done: function () {
      var convertTo;
      if (_.has(config, 'convertTo')) {
        convertTo = { convertTo: config.convertTo };
      }
      return _.compact(_.flatten([ partialQuery, convertTo ]));
    },
    step: function (queryStep) {
      if (queryStep.type === 'concept') {
        return this.concept(queryStep);
      }
      if (queryStep.type === 'dataElement') {
        return this.dataElement(queryStep);
      }
      if (queryStep.type === 'position') {
        return this.position(queryStep);
      }
      if (queryStep.type === 'select') {
        return this.select(queryStep);
      }
      if (queryStep.type === 'filter') {
        return this.filter(queryStep);
      }
      return this.giveUp();
    },
    concept: function (queryStep) {
      var conceptConfig = (config.concept || {})[queryStep.concept];
      return this.applyPartialQueryAndContinueWith(conceptConfig);
    },
    dataElement: function (queryStep) {
      var dataElementConfig = (config.dataElement || {})[queryStep.dataElement];
      return this.applyPartialQueryAndContinueWith(dataElementConfig);
    },
    position: function (queryStep) {
      return this.continueWith(config.position, {
        xpath: '/self::node()[position() = ' + (queryStep.position + 1) + ']'
      });
    },
    select: function (queryStep) {
      var maximizeBy = queryStep.selectCriteria.maximum.data_element;
      var extraQuery = ((config.select || {}).maximum || {})[maximizeBy];
      if (!extraQuery) { return this.giveUp(); }
      return this.continueWith(config, extraQuery);
    },
    filter: function (queryStep) {
      return this.continueWith(config,
        filterQuery(queryStep.filterPredicate, config.filter));
    },
    applyPartialQueryAndContinueWith: function (nextConfig) {
      return this.continueWith(nextConfig, (nextConfig || {}).partialQuery);
    },
    continueWith: function (nextConfig, extraQuery) {
      if (!nextConfig) { return this.giveUp(); }
      return makeQueryCompiler(nextConfig,
        [ partialQuery, extraQuery ]);
    },
    giveUp: function () {
      return makeIGaveUpCompiler();
    }
  };
}

function filterQuery(filterPredicate, config) {
  if (_.has(filterPredicate, 'and')) {
    return _.map(filterPredicate.and, _.partial(filterQuery, _, config));
  }
  if (_.has(filterPredicate, 'cbb_data_element')) {
    var dataElement = filterPredicate.cbb_data_element;
    return filterQuery(_.omit(filterPredicate, 'cbb_data_element'),
      config.dataElement[dataElement]);
  }
  if (_.has(filterPredicate, 'matchesCodes')) {
    return _.extend({
      codes: filterPredicate.matchesCodes.codes
    },
    config.matchesCodes);
  }
  if (_.has(filterPredicate, 'greaterThanOrEquals')) {
    return _.extend({
      comparisonValue: filterPredicate.greaterThanOrEquals
    },
    config.greaterThanOrEquals);
  }
  throw new Error('Unrecognized filter predicate');
}

function makeIGaveUpCompiler() {
  return {
    done: function () { return null; },
    step: function () { return this; }
  };
}

function evaluateQuery(query, evalxpath, xml, compilerConfig) {
  var compiled = compileQuery(query, compilerConfig);
  if (compiled) {
    var result = executeQuery(compiled, xml, evalxpath);
    return result;
  }
}

function singleText(xpathResult) {
  if (xpathResult.length !== 1) { return; }
  return xpathResult[0].nodeValue;
}

function parseDate(format, value) {
  if (!_.isString(value)) { return; }
  return moment(value, format);
}

function parseNumber(value) {
  if (!_.isString(value)) { return; }
  return Number(value);
}
