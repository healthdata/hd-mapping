/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var util = require('util');
var documentmodel = require('documentmodel');
var xmldom = require('xmldom');
var xpath = require('xpath');
var moment = require('moment');

function autocompleteDirective() {}

var queries = {};

function singleMatch(nodes) {
  if (nodes.length === 1) {
    return nodes[0].nodeValue;
  }
  return null;
}

function transferToManipulator(kmehrValue, manipulator, strategy) {
  strategy = strategy || 'auto';

  var description = manipulator.description;

  if (strategy === 'auto') {
    if (manipulator.hasOwnProperty('selectedOption')) {
      manipulator.selectedOption = description.kmehrOptionValues[kmehrValue];
      return;
    }
    manipulator.value = kmehrValue;
  }
}

queries.kmehr_boolean = function kmehrBoolean(q, paramName) {
  var path =
    '/*[local-name()="kmehrmessage"]' +
    '/*[local-name()="folder"]' +
    '/*[local-name()="transaction"]' +
    '/*[local-name()="item"]' +
    '/*[local-name()="content"]' +
    '/*[local-name()="cd" and @SL="CD-ITEM-HEALTHDATA"][text()="%s"]' +
    '/../..' +
    '/*[local-name()="content"]' +
    '/*[local-name()="boolean"]' +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrBoolean',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_boolean(inputData.kmehr, config.name);
    if (result === 'true') {
      manipulator.value = true;
    }
    if (result === 'false') {
      manipulator.value = false;
    }
  }
});

autocompleteDirective({
  on: 'kmehrMultiChoiceOption',
  require: [ 'skrOption' ],
  autocomplete: function (manipulator, inputData, config, skrOption) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_boolean(inputData.kmehr, config.name);
    var optionManipulator = manipulator.optionManipulators[skrOption.name];
    if (result === 'true') {
      optionManipulator.isSelected = true;
    }
    if (result === 'false') {
      optionManipulator.isSelected = false;
    }
  }
});

queries.kmehr_inss = function kmehrINSS(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='patient']" +
    "/*[local-name()='id']" +
    '/text()';
  return singleMatch(q(path));
};

autocompleteDirective({
  on: 'kmehrINSS',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_inss(inputData.kmehr);
    if (result) {
      manipulator.value = parseInt(result);
    }
  }
});

queries.kmehr_nationality = function kmehrNationality(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='patient']" +
    "/*[local-name()='nationality']" +
    "/*[local-name()='cd']" +
    '/text()';
  return singleMatch(q(path));
};

autocompleteDirective({
  on: 'kmehrNationality',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_nationality(inputData.kmehr);
    if (result) {
      manipulator.value = result;
    }
  }
});

queries.kmehr_sex = function kmehrSex(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='patient']" +
    "/*[local-name()='sex']" +
    "/*[local-name()='cd']" +
    '/text()';
  return singleMatch(q(path));
};

autocompleteDirective({
  on: 'kmehrSex',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_sex(inputData.kmehr);
    if (result) {
      transferToManipulator(result, manipulator);
    }
  }
});

queries.kmehr_birthdate = function kmehrBirthdate(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='patient']" +
    "/*[local-name()='birthdate']" +
    "/*[local-name()='date']" +
    '/text()';
  return singleMatch(q(path));
};

queries.kmehr_deathdate = function kmehrDeathdate(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='patient']" +
    "/*[local-name()='deathdate']" +
    "/*[local-name()='date']" +
    '/text()';
  return singleMatch(q(path));
};

autocompleteDirective({
  on: 'kmehrDeathdate',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_deathdate(inputData.kmehr);
    if (!result) { return; }
    result = moment(result, 'YYYY-MM-DD');
    if (result.isValid()) {
      manipulator.value = result.toDate();
    }
  }
});

autocompleteDirective({
  on: 'kmehrDeceased',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_deathdate(inputData.kmehr);
    // the presence of a deathdate determines whether deceased or not
    manipulator.value = Boolean(result);
  }
});

queries.kmehr_register = function kmehrRegister(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='cd' and @SL='CD-HEALTHDATA-REGISTER']" +
    '/text()';
  return singleMatch(q(path));
};

queries.kmehr_parameterContent = function kmehrParameterContent(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='cd' and @S='CD-PARAMETER'][text()='%s']" +
    '/..' +
    "/*[local-name()='content']" +
    "/*[local-name()='decimal']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrParameterContent',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_parameterContent(inputData.kmehr, config.name);
    if (result) {
      manipulator.value = parseFloat(result);
    }
  }
});

queries.kmehr_parameterUnit = function kmehrParameterUnit(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='cd' and @S='CD-PARAMETER'][text()='%s']" +
    '/..' +
    "/*[local-name()='content']" +
    "/*[local-name()='unit']" +
    "/*[local-name()='cd']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

queries.kmehr_parameterDateTime = function kmehrParameterDateTime(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='cd' and @S='CD-PARAMETER'][text()='%s']" +
    '/..' +
    "/*[local-name()='recorddatetime']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrParameterDateTime',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_parameterDateTime(inputData.kmehr, config.name);
    if (!result) { return; }
    result = moment(result);
    // discard the time component
    // TODO allow the time component
    result.hours(0).minutes(0).seconds(0);
    if (result.isValid()) {
      manipulator.value = result.toDate();
    }
  }
});

queries.kmehr_listValue = function kmehrListValue(q, listName, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='content']" +
    "/*[local-name()='cd' and @SL='CD-ITEM-HEALTHDATA'][text()='%s']" +
    '/../..' +
    "/*[local-name()='content']" +
    "/*[local-name()='cd' and @SL='%s']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName, listName)));
};

autocompleteDirective({
  on: 'kmehrListValue',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_listValue(inputData.kmehr, config.listName, config.name);
    if (result) {
      transferToManipulator(result, manipulator);
    }
  }
});

queries.kmehr_text = function kmehrText(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='content']" +
    "/*[local-name()='cd' and @SL='CD-ITEM-HEALTHDATA'][text()='%s']" +
    '/../..' +
    "/*[local-name()='content']" +
    "/*[local-name()='text']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrText',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_text(inputData.kmehr, config.name);
    if (result) {
      transferToManipulator(result, manipulator);
    }
  }
});

queries.kmehr_number = function kmehrText(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='content']" +
    "/*[local-name()='cd' and @SL='CD-ITEM-HEALTHDATA'][text()='%s']" +
    '/../..' +
    "/*[local-name()='content']" +
    "/*[local-name()='decimal']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrNumber',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_number(inputData.kmehr, config.name);
    if (result) {
      manipulator.value = parseFloat(result);
    }
  }
});

queries.kmehr_year = function kmehrYear(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='content']" +
    "/*[local-name()='cd' and @SL='CD-ITEM-HEALTHDATA'][text()='%s']" +
    '/../..' +
    "/*[local-name()='content']" +
    "/*[local-name()='year']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrYear',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_year(inputData.kmehr, config.name);
    if (result) {
      manipulator.value = parseFloat(result);
    }
  }
});

queries.kmehr_date = function kmehrText(q, paramName) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='folder']" +
    "/*[local-name()='transaction']" +
    "/*[local-name()='item']" +
    "/*[local-name()='content']" +
    "/*[local-name()='cd' and @SL='CD-ITEM-HEALTHDATA'][text()='%s']" +
    '/../..' +
    "/*[local-name()='content']" +
    "/*[local-name()='date']" +
    '/text()';
  return singleMatch(q(util.format(path, paramName)));
};

autocompleteDirective({
  on: 'kmehrDate',
  autocomplete: function (manipulator, inputData, config) {
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_date(inputData.kmehr, config.name);
    if (!result) { return; }
    result = moment(result, 'YYYY-MM-DD');
    if (result.isValid()) {
      manipulator.value = result.toDate();
    }
  }
});

queries.kmehr_messagedate = function kmehrText(q) {
  var path =
    "/*[local-name()='kmehrmessage']" +
    "/*[local-name()='header']" +
    "/*[local-name()='date']" +
    '/text()';
  return singleMatch(q(util.format(path)));
};

autocompleteDirective({
  on: 'kmehrMessageDate',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.kmehr) { return; }
    var result = queries.kmehr_messagedate(inputData.kmehr);
    if (!result) { return; }
    result = moment(result, 'YYYY-MM-DD');
    if (result.isValid()) {
      manipulator.value = result.toDate();
    }
  }
});

function directive() {}

directive({
  on: 'kmehrOptionValue',
  require: [ 'skrDescription', 'skrOption' ],
  compile: function (node, env, skrDescription, skrOption) {
    var optionValue = node.kmehrOptionValue;
    var values = skrDescription.kmehrOptionValues;
    if (!values) {
      values = skrDescription.kmehrOptionValues = {};
    }
    values[optionValue] = skrOption.name;
  }
});

module.exports = {
  queries: queries,
  importDocumentFromKMEHR: function (inputData, metaData, dataDefinition, cb) {
    try {
      var xml = new xmldom.DOMParser().parseFromString(inputData);
      var doc = documentmodel.autocomplete.fromScratch(dataDefinition, {
        kmehr: function queryKMEHR(q) {
          return xpath.select(q, xml);
        }
      });
      return cb(null, JSON.stringify(doc.target));
    } catch (e) {
      return cb(e);
    }
  }
};
