/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports.merge = function (primary, secondary) {
  var log = [];
  primary.inExtendedEditContext('merge', function () {
    doTheMerge(primary, secondary, log);
  });
};

function doTheMerge(primary, secondary, log) {
  doTheMergeLoop(primary.root, secondary.root, log);
}

function doTheMergeLoop(primary, secondary, log) {
  var type = primary.description.type;
  if (type !== secondary.description.type) {
    throw new Error('non-matching description');
  }
  if (type === 'object') {
    mergeCorrespondingProperties(primary.propertyManipulators, secondary.propertyManipulators, log);
    return;
  }
  if (type === 'number') {
    mergeSimpleValue(primary, secondary, log);
    return;
  }
  if (type === 'text') {
    mergeSimpleValue(primary, secondary, log);
    return;
  }
  if (type === 'date') {
    mergeSimpleValue(primary, secondary, log);
    return;
  }
  if (type === 'choice') {
    mergeChoice(primary, secondary, log);
    return;
  }
  if (type === 'multichoice') {
    mergeMultiChoice(primary, secondary, log);
    return;
  }
  if (type === 'list') {
    mergeList(primary, secondary, log);
    return;
  }
  if (type === 'patientID') {
    mergePatientID(primary, secondary, log);
    return;
  }
  mergeGeneric(primary, secondary, log);
}

function isEmpty(manipulator) {
  var description = manipulator.description;
  var data = manipulator.read();
  return description.isAbsent(data, manipulator) || description.isEmpty(data, manipulator);
}

function mergeLogSet(primary, secondary, log, info) {
  log.push({
    action: 'set',
    ppath: primary.path,
    spath: secondary.path,
    type: primary.description.type,
    info: info
  });
}

function mergeLogSkip(primary, secondary, log) {
  log.push({
    action: 'skip',
    ppath: primary.path,
    spath: secondary.path,
    type: primary.description.type
  });
}

function mergeSimpleValue(primary, secondary, log) {
  if (isEmpty(primary) && !isEmpty(secondary)) {
    mergeLogSet(primary, secondary, log, { value: secondary.value });
    primary.value = secondary.value;
  } else {
    mergeLogSkip(primary, secondary, log);
  }
}

function mergeChoice(primary, secondary, log) {
  if (isEmpty(primary) && !isEmpty(secondary)) {
    mergeLogSet(primary, secondary, log, { selectedOption: secondary.selectedOption });
    primary.selectedOption = secondary.selectedOption;
  } else {
    mergeLogSkip(primary, secondary, log);
  }
  if (!isEmpty(primary) && primary.selectedOption === secondary.selectedOption) {
    var selectedOption = primary.selectedOption;
    var primaryProperties = primary.optionManipulators[selectedOption].propertyManipulators;
    var secondaryProperties = secondary.optionManipulators[selectedOption].propertyManipulators;
    mergeCorrespondingProperties(primaryProperties, secondaryProperties, log);
  }
}

function mergeMultiChoice(primary, secondary, log) {
  if (isEmpty(primary) && !isEmpty(secondary)) {
    mergeLogSet(primary, secondary, log, { selectedOption: secondary.read().selectedOptions });
    _.each(primary.description.options, function (option) {
      primary.optionManipulators[option].isSelected =
        secondary.optionManipulators[option].isSelected;
    });
  } else {
    mergeLogSkip(primary, secondary, log);
  }
  _.each(primary.description.options, function (option) {
    var primaryOptionManipulator = primary.optionManipulators[option];
    var secondaryOptionManipulator = secondary.optionManipulators[option];
    if (primaryOptionManipulator.isSelected && secondaryOptionManipulator.isSelected) {
      mergeCorrespondingProperties(
        primaryOptionManipulator.propertyManipulators,
        secondaryOptionManipulator.propertyManipulators,
        log);
    }
  });
}

function mergeCorrespondingProperties(primaryProperties, secondaryProperties, log) {
  _.each(primaryProperties, function (pm, propertyName) {
    doTheMergeLoop(pm, secondaryProperties[propertyName], log);
  });
}

function mergePatientID(primary, secondary, log) {
  if (isEmpty(primary) && !isEmpty(secondary)) {
    mergeLogSet(primary, secondary, log, { value: secondary.value });
    primary.value = secondary.value;
  } else {
    mergeLogSkip(primary, secondary, log);
  }
  mergeCorrespondingProperties(primary.propertyManipulators, secondary.propertyManipulators, log);
}

function mergeGeneric(primary, secondary, log) {
  if (isEmpty(primary) && !isEmpty(secondary)) {
    mergeLogSet(primary, secondary, log, { generic: secondary.read() });
    primary.edit(function () {
      return secondary.read();
    });
  } else {
    mergeLogSkip(primary, secondary, log);
  }
}

function mergeList(primary, secondary, log) {
  if (primary.elementManipulators.length === 0 && secondary.elementManipulators.length) {
    mergeLogSet(primary, secondary, log, { list: secondary.read() });
    primary.edit(function () {
      return secondary.read();
    });
  } else {
    mergeLogSkip(primary, secondary, log);
  }
}
