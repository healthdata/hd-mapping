/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var cbbQuery = require('./cbbQuery');

var cbbTrait = {
  requires: [ 'field' ],
  provides: [ 'cbb' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: addCBBInfo
    }
  ]
};

module.exports = {
  importWithCBBs: importWithCBBs,
  cbbTrait: cbbTrait,
  traits: {
    number: acceptCBBTrait(numberCBB),
    text: acceptCBBTrait(textCBB),
    date: acceptCBBTrait(dateCBB),
    referencelist: acceptCBBTrait(referencelistCBB),
    choice: acceptCBBTrait(choiceCBB),
    multichoice: acceptCBBTrait(multichoiceCBB),
    list: acceptCBBTrait(listCBB),
    boolean: acceptCBBTrait(booleanCBB)
  }
};

function acceptCBBTrait(f) {
  return {
    require: [ 'field' ],
    provides: [ 'acceptCBB' ],
    behaviour: {
      acceptCBB: f
    }
  };
}

function numberCBB(manipulator, data) {
  data = singleNumber(data);
  if (_.isNumber(data)) {
    manipulator.value = data;
  }
}

function textCBB(manipulator, data) {
  data = singleString(data);
  if (_.isString(data)) {
    manipulator.value = data;
  }
}

function dateCBB(manipulator, data) {
  data = singleDate(data);
  if (data && data.isValid && data.isValid()) {
    manipulator.value = data.toDate();
  }
}

function singleString(data) {
  if (_.isArray(data) && data.length === 1 && _.isString(data[0])) {
    return data[0];
  }
  return data;
}

function singleBoolean(data) {
  if (_.isArray(data) && data.length === 1 && _.isBoolean(data[0])) {
    return data[0];
  }
  return data;
}

function singleNumber(data) {
  if (_.isArray(data) && data.length === 1 && _.isNumber(data[0])) {
    return data[0];
  }
  return data;
}

function singleDate(data) {
  if (_.isArray(data) && data.length === 1 &&
    _.isObject(data[0]) && data[0].isValid && data[0].isValid()) {
    return data[0];
  }
  return data;
}

function referencelistCBB(manipulator, data) {
  data = singleString(data);
  if (typeof data !== 'string') { return; }
  if (data === '') { return; }
  manipulator.setValue(data);
}

function choiceCBB(manipulator, data) {
  if (_.isString(data) && _.has(manipulator.optionManipulators, data)) {
    manipulator.selectedOption = data;
    return;
  }
  if (_.isArray(data)) {
    var hasConflict = false;
    var match;
    var mapping = manipulator.description.cbb.mapping;
    mapping = mapping && mapping.choice_mapping;
    _.each(manipulator.optionManipulators, function (om, name) {
      if (hasConflict) { return; }
      var optionMapping = mapping ? mapping[name] : [ name ];
      if (!_.isEmpty(_.intersection(data, optionMapping))) {
        if (match) {
          hasConflict = true;
          match = undefined;
        }
        match = name;
      }
    });
    if (!hasConflict && match) {
      manipulator.selectedOption = match;
    }
    return;
  }
}

function multichoiceCBB(manipulator, data) {
  if (!_.isArray(data)) { return; }
  var mapping = manipulator.description.cbb.mapping;
  mapping = mapping && mapping.choice_mapping;
  if (!mapping) { return; }
  _.each(manipulator.optionManipulators, function (om, name) {
    var optionMapping = mapping[name];
    if (!_.isEmpty(_.intersection(data, optionMapping))) {
      om.isSelected = true;
    }
  });
}

function listCBB(manipulator, data, context) {
  if (!_.isArray(data)) { return; }
  var count = data.length;
  while (manipulator.elementManipulators.length < count) {
    manipulator.addElement();
  }
  var listCBB = manipulator.description.cbb;
  return _.map(data, function (item, position) {
    return {
      type: 'listItemPosition',
      parentContext: context,
      position: position,
      cbb: listCBB
    };
  });
}

function booleanCBB(manipulator, data) {
  data = singleBoolean(data);
  if (!_.isBoolean(data)) { return; }
  manipulator.value = data;
}

function addCBBInfo(description, config) {
  if (!config.cbb_concept && !config.cbb_data_element) { return; }
  description.cbb = {
    concept: config.cbb_concept,
    dataElement: config.cbb_data_element,
    filter: config.cbb_filter || {},
    select: config.cbb_select || {},
    mapping: config.cbb_mapping
  };
}

function importWithCBBs(document, evaluateQuery) {
  var importWithCBBsLog = [];
  document.inExtendedEditContext('importCBB', function () {
    document.traverseWithContext({},
      _.partial(importManipulatorUsingCBB, _, _, evaluateQuery, importWithCBBsLog));
    document.importWithCBBsLog = importWithCBBsLog;
  });
}

function importManipulatorUsingCBB(manipulator, context, evaluateQuery, importWithCBBsLog) {
  if (!manipulator.description.cbb) {
    // nested fields will be visited with the same context
    return;
  }
  var behaviour = manipulator.description.behaviour;
  if (!behaviour.acceptCBB) {
    return defaultNestedContexts(manipulator, context);
  }
  var query = makeCBBQuery(manipulator.description.cbb, context);
  var data = evaluateQuery(query);
  importWithCBBsLog.push({
    manipulator: manipulator,
    query: query,
    result: data
  });
  return behaviour.acceptCBB(manipulator, data, context) ||
    defaultNestedContexts(manipulator, context);
}

function defaultNestedContexts(manipulator, context) {
  return _.map(manipulator.nestedManipulators,
    _.constant({
      type: 'nestedFields',
      parentContext: context,
      cbb: manipulator.description.cbb
    }));
}

function makeCBBQuery(cbb, context) {
  return cbbQuery.build(function (builder) {
    return builder.sequence([
      makeCBBContextQuery(context, builder),
      makeCBBFieldQuery(cbb, builder)
    ]);
  });
}

function makeCBBContextQuery(context, builder) {
  if (!context.type) { return builder.identity(); }
  if (context.type === 'listItemPosition') {
    return builder.sequence([
      makeCBBContextQuery(context.parentContext, builder),
      builder.concept(context.cbb.concept),
      builder.dataElement(context.cbb.dataElement),
      builder.atPosition(context.position)
    ]);
  }
  if (context.type === 'nestedFields') {
    return builder.sequence([
      makeCBBContextQuery(context.parentContext, builder),
      builder.concept(context.cbb.concept),
      builder.dataElement(context.cbb.dataElement),
      builder.filter(context.cbb.filter),
      builder.select(context.cbb.select)
    ]);
  }
  throw new Error('Unrecognized context');
}

function makeCBBFieldQuery(cbb, builder) {
  return builder.sequence([
    builder.concept(cbb.concept),
    builder.dataElement(cbb.dataElement),
    builder.filter(cbb.filter),
    builder.select(cbb.select)
  ]);
}
