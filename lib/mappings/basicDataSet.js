/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var xmldom = require('xmldom').DOMParser;
var xpath = require('xpath');

var cbb = require('./cbb');

var cbbViaXPath = require('./cbbViaXPath');

module.exports = {
  importFromBasicDataSetWithCBBs: importFromBasicDataSetWithCBBs
};

function importFromBasicDataSetWithCBBs(document, xmlData) {

  /* jshint newcap: false */
  var xml = new xmldom().parseFromString(xmlData);
  var ns = {
    kmehr: 'http://www.ehealth.fgov.be/standards/kmehr/schema/v1'
  };

  var eq = _.partial(cbbViaXPath.evaluateQuery,
    _,
    xpath.useNamespaces(ns),
    xml,
    makeCompilerConfig());

  cbb.importWithCBBs(document, eq);
}

function makeCompilerConfig() {
  return {
    concept: {
      'be.en.hd.Patient': {
        partialQuery: { xpath: xpPatient },
        dataElement: {
          PatientIdentificationNumber: {
            partialQuery: { xpath: '/kmehr:id[@S="ID-PATIENT"]/text()' },
            convertTo: 'text'
          },
          FirstNames: {
            partialQuery: { xpath: '/kmehr:firstname/text()' },
            convertTo: 'text'
          },
          LastName: {
            partialQuery: { xpath: '/kmehr:familyname/text()' },
            convertTo: 'text'
          },
          DateOfBirth: {
            partialQuery: { xpath: '/kmehr:birthdate/kmehr:date/text()' },
            convertTo: 'date'
          },
          Gender: {
            partialQuery: { xpath: '/kmehr:sex/kmehr:cd/text()' },
            convertTo: 'gender'
          },
          Postcode: {
            partialQuery: { xpath: '/kmehr:address/kmehr:zip/text()' },
            convertTo: 'text'
          },
          DeathIndicator: {
            partialQuery: { xpath: '/kmehr:deathdate/kmehr:date/text()' },
            convertTo: 'hasResults'
          },
          DateOfDeath: {
            partialQuery: { xpath: '/kmehr:deathdate/kmehr:date/text()' },
            convertTo: 'date'
          }
        }
      },
      'be.en.hd.TransferConcern': {
        partialQuery: { xpath: xpSumhr + xpHealthcareelement },
        convertTo: 'nothing',
        dataElement: {
          ProblemName: {
            partialQuery: { xpath: '/kmehr:content/kmehr:cd/text()' },
            convertTo: 'text'
          },
          Problem: {
            convertTo: 'nothing',
            position: {
              dataElement: {
                Explanation: {
                  partialQuery: { xpath: '/kmehr:content/kmehr:text/text()' },
                  convertTo: 'text'
                }
              }
            }
          },
          ProblemStartDate: {
            partialQuery: { xpath: '/kmehr:beginmoment/kmehr:date/text()' },
            convertTo: 'date',
            filter: {
              dataElement: {
                ProblemName: {
                  matchesCodes: {
                    matchesCodes:
                      'ancestor::kmehr:item/kmehr:content/kmehr:cd/text()'
                  }
                }
              }
            }
          }
        },
        filter: {
          dataElement: {
            ProblemName: {
              matchesCodes: {
                matchesCodes: 'kmehr:content/kmehr:cd/text()'
              }
            }
          }
        }
      },
      'be.en.hd.Caregiver': {
        dataElement: {
          CareGiverIdentificationNumber: {
            partialQuery: {
              xpath:
                '/kmehr:kmehrmessage/kmehr:header/kmehr:sender' +
                '/kmehr:hcparty[kmehr:cd[@S = "CD-HCPARTY"]/text() = "persphysician"]' +
                '/kmehr:id[@S = "ID-HCPARTY"]' +
                '/text()'
            },
            convertTo: 'text'
          }
        }
      },
      'be.en.hd.BloodPressure': {
        partialQuery: { xpath: xpSumhr },
        dataElement: {
          SystolicBloodPressure: {
            partialQuery: {
              xpath:
                xpParameter('sbp') +
                '/kmehr:content/kmehr:decimal/text()' },
            select: {
              maximum: {
                BloodPressureDateTime: {
                  mostRecent: xpItemBeginMoment
                }
              }
            },
            convertTo: 'number'
          },
          DiastolicBloodPressure: {
            partialQuery: {
              xpath:
                xpParameter('dbp') +
                '/kmehr:content/kmehr:decimal/text()' },
            select: {
              maximum: {
                BloodPressureDateTime: {
                  mostRecent: xpItemBeginMoment
                }
              }
            },
            convertTo: 'number'
          }
        }
      },
      'be.en.hd.BodyWeight': {
        partialQuery: { xpath: xpSumhr + xpParameter('weight') },
        dataElement: {
          WeightValue: {
            partialQuery: { xpath: '/kmehr:content/kmehr:decimal/text()' },
            select: {
              maximum: {
                WeightDateTime: {
                  mostRecent: xpItemBeginMoment
                }
              }
            },
            convertTo: 'number'
          }
        }
      },
      'be.en.hd.BodyHeight': {
        partialQuery: { xpath: xpSumhr + xpParameter('height') },
        dataElement: {
          HeightValue: {
            partialQuery: { xpath: '/kmehr:content/kmehr:decimal/text()' },
            select: {
              maximum: {
                HeightDateTime: {
                  mostRecent: xpItemBeginMoment
                }
              }
            },
            convertTo: 'number'
          }
        }
      },
      'be.en.hd.MedicationPrescription': {
        partialQuery: { xpath: xpSumhr + xpItem('medication') },
        dataElement: {
          MedicationPrescriptionDate: {
            partialQuery: { xpath: '/kmehr:beginmoment/kmehr:date/text()' },
            convertTo: 'date',
            filter: {
              dataElement: {
                ProductCode: {
                  matchesCodes: {
                    matchesCodes:
                      'ancestor::kmehr:item/kmehr:content/kmehr:cd/text()'
                  }
                }
              }
            }
          },
          ProductName: {
            partialQuery: {
              xpath: '/kmehr:content/kmehr:medicinalproduct/kmehr:intendedname/text()'
            },
            convertTo: 'text'
          }
        },
        filter: {
          dataElement: {
            ProductCode: {
              matchesCodes: {
                matchesCodes:
                  'kmehr:content/kmehr:medicinalproduct/kmehr:intendedcd/text()'
              }
            }
          }
        },
        convertTo: 'nothing'
      },
      'be.en.hd.Vaccination': {
        partialQuery: { xpath: xpSumhr + xpItem('vaccine') },
        convertTo: 'nothing',
        filter: {
          dataElement: {
            ProductCode: {
              matchesCodes: {
                matchesCodes:
                  'kmehr:content/kmehr:medicinalproduct/kmehr:intendedcd/text()'
              }
            },
            VaccinationDate: {
              greaterThanOrEquals: {
                afterDate: 'kmehr:beginmoment/kmehr:date/text()'
              }
            }
          }
        },
        dataElement: {
          VaccinationDate: {
            partialQuery: { xpath: '/kmehr:beginmoment/kmehr:date/text()' },
            convertTo: 'date'
          },
          ProductCode: {
            partialQuery: {
              xpath: '/kmehr:content/kmehr:medicinalproduct/kmehr:intendedname/text()'
            },
            convertTo: 'text'
          }
        }
      }
    }
  };
}

var xpItemBeginMoment = 'ancestor::kmehr:item/kmehr:beginmoment/kmehr:date/text()';

var xpPatient =
  '/kmehr:kmehrmessage' +
  '/kmehr:folder' +
  '/kmehr:patient';

var xpSumhr =
  '/kmehr:kmehrmessage' +
  '/kmehr:folder' +
  '/kmehr:transaction[kmehr:cd[@S = "CD-TRANSACTION"]/text() = "sumehr"]';

function xpItem(type) {
  return '/kmehr:item[kmehr:cd[@S = "CD-ITEM"]/text() = "' + type + '"]';
}

function xpParameter(name) {
  return '/kmehr:item[' +
    'kmehr:cd[@S = "CD-ITEM"]/text() = "parameter" and ' +
    'kmehr:cd[@S = "CD-PARAMETER"]/text() = "' + name + '"' +
  ']';
}

var xpHealthcareelement = xpItem('healthcareelement');

function xpItem(type) {
  return '/kmehr:item[kmehr:cd[@S = "CD-ITEM"]/text() = "' + type + '"]';
}
