/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var documentmodelHelpers = require('../documentmodelHelpers');
var documentmodel = require('documentmodel');
var config = require('../../config');

require('../directives');

var csvMapping = require('./csvMapping');
var esMapping = require('./esMapping');
var mergeMapping = require('./mergeMapping');
var basicDataSet = require('./basicDataSet');
var handlebars = require('../handlebars/core');
var notes = require('../notes');
var request = require('request');

module.exports = hdFlow;

function hdFlow(actions, input, targets, cb) {
  documentmodel.flow(
    actionTargets,
    defaultTargets,
    actions, input, targets,
    cb);
}

function documentInfoTargets(documentTargetName) {
  return {
    csv: {
      prerequisites: [ documentTargetName ],
      implementation: csvStep
    },
    stableCsv: {
      prerequisites: [ documentTargetName ],
      implementation: stableCsvStep
    },
    uniqueID: {
      prerequisites: [ documentTargetName ],
      implementation: uniqueIDStep
    },
    progress: {
      prerequisites: [ documentTargetName ],
      implementation: progressStep
    },
    validation: {
      prerequisites: [ documentTargetName ],
      implementation: validationStep
    },
    notesFromComments: {
      prerequisites: [ documentTargetName ],
      implementation: notesFromCommentsStep
    },
    documentContentWithoutComments: {
      prerequisites: [ documentTargetName ],
      implementation: documentContentWithoutCommentsStep
    },
    document: {
      prerequisites: [ documentTargetName ],
      implementation: documentDataStep
    },
    toBeCoded: {
      prerequisites: [ documentTargetName ],
      implementation: toBeCodedStep
    },
    pendingAttachments: {
      prerequisites: [ documentTargetName ],
      implementation: pendingAttachmentsStep
    },
    es: {
      prerequisites: [ documentTargetName ],
      implementation: esStep
    },
    affectedFields: {
      prerequisites: [ 'documentModel' ],
      implementation: affectedFieldsStep
    },
    computedExpressions: {
      prerequisites: [ documentTargetName ],
      implementation: computedExpressionsStep
    }
  };
}

function computedExpressionsStep(document, input, cb) {
  cb(null, _.extend({}, document.root.computedExpressions));
}

function csvStep(document, input, cb) {
  csvMapping.mapDocumentToCSV(document, cb);
}

function stableCsvStep(document, input, cb) {
  csvMapping.mapDocumentToCSV(document, { stable: true }, cb);
}

function uniqueIDStep(document, input, cb) {
  csvMapping.mapDocumentToUniqueID(document, cb);
}

function progressStep(document, input, cb) {
  cb(null, documentmodelHelpers.progressInfo(document));
}

function validationStep(document, input, cb) {
  cb(null, documentmodelHelpers.validationInfo(document));
}

function notesFromCommentsStep(document, input, cb) {
  cb(null, notes.migrateCommentsToNotes(document));
}

function documentContentWithoutCommentsStep(document, input, cb) {
  cb(null, notes.documentContentWithoutNotes(document));
}

function documentDataStep(document, input, cb) {
  // FIXME the conversion via JSON is required to have exactly the same behaviour
  // as the original CSV import. Without it, the BEWSD test case fails because some
  // property gets cleared during the computation of the progress info.
  cb(null, {
    content: JSON.parse(JSON.stringify(document.documentData.content)),
    private: document.documentData.private,
    patientID: document.documentData.patientID
  });
}

function toBeCodedStep(document, input, cb) {
  cb(null, document.toBeCoded());
}

function pendingAttachmentsStep(document, input, cb) {
  cb(null, document.services.attachment.pendingAttachmens());
}

function esmappingStep(dataCollectionDefinition, input, cb) {
  esMapping.mappingFromDCDtoEsmapping(dataCollectionDefinition, cb);
}

function handlebarsStep(dcdComponent, input, cb) {
  cb(null, handlebars.generateHandlebarsTemplate(dcdComponent, undefined, input.language));
}

function esStep(document, input, cb) {
  esMapping.mappingFromDocumentToEs(document, cb);
}

function dataCollectionDefinitionTarget() {
  return {
    dataCollectionDefinition: {
      prerequisites: [ 'dataCollectionDefinitionFull' ],
      implementation: dataCollectionDefinitionStep
    },
    dataCollectionDefinitionComponent: {
      prerequisites: [ 'dataCollectionDefinitionFull' ],
      implementation: dataCollectionDefinitionComponentStep
    },
    dataCollectionDefinitionFull: {
      implementation: dataCollectionDefinitionFullStep
    },
    esmapping: {
      prerequisites: [ 'dataCollectionDefinition' ],
      implementation: esmappingStep
    },
    handlebars: {
      prerequisites: [ 'dataCollectionDefinitionComponent' ],
      implementation: handlebarsStep
    }
  };
}

function dataCollectionDefinitionStep(dcd, input, cb) {
  return cb(null, dcd.description);
}

function dataCollectionDefinitionComponentStep(dcd, input, cb) {
  return cb(null, dcd.component);
}

function dataCollectionDefinitionFullStep(input, cb) {
  expectProperty(input, 'dataCollectionDefinition', _.isObject,
    function (err, dataCollectionDefinition) {
      if (err) { return cb(err); }
      var env = {};
      documentmodelHelpers.compile(dataCollectionDefinition, env);
      return cb(null, {
        description: env.skrDescription,
        component: env.skrEditorComponent
      });
  });
}

function openDocumentTarget() {
  return {
    documentModel: {
      prerequisites: [ 'dataCollectionDefinition' ],
      implementation: _.partial(documentStep, 'document', {})
    }
  };
}

function documentStep(documentPropertyName, options, dataCollectionDefinition, input, cb) {
  expectProperty(input, documentPropertyName, _.isObject, function (err, documentData) {
    if (err) { return cb(err); }
    documentmodelHelpers.openDocument(
      dataCollectionDefinition, {
        content: documentData.content,
        private: documentData.private,
        coded: documentData.coded,
        patientID: documentData.patientID,
        context: input.context
      },
      options,
      cb);
  });
}

function expectProperty(obj, propertyName, check, cb) {
  if (_.isUndefined(cb)) {
    cb = check;
    check = undefined;
  }
  var ok = _.has(obj, propertyName) &&
           !_.isUndefined(obj[propertyName]) &&
           !_.isNull(obj[propertyName]) &&
           (!check || check(obj[propertyName]));
  if (ok) {
    return cb(null, obj[propertyName]);
  }
  var err = new Error('Missing or invalid input parameter "' + propertyName + '"');
  err.badRequest = true;
  cb(err);
}

var actionTargets = {
  refresh: function () {
    return {
      documentModel: {
        prerequisites: [ 'dataCollectionDefinition' ],
        implementation: _.partial(documentStep, 'document', { readOnly: false })
      }
    };
  },
  importCsv: function () {
    return {
      documentModel: {
        prerequisites: [ 'dataCollectionDefinition' ],
        implementation: importCsvStep
      }
    };
  },
  importBasicDataset: function () {
    return {
      documentModel: {
        prerequisites: [ 'dataCollectionDefinition' ],
        implementation: importBasicDatasetStep
      }
    };
  },
  merge: function () {
    return {
      primaryDocumentModel: {
        prerequisites: [ 'dataCollectionDefinition' ],
        implementation: _.partial(documentStep, 'primary', { readOnly: false })
      },
      secondaryDocumentModel: {
        prerequisites: [ 'dataCollectionDefinition' ],
        implementation: _.partial(documentStep, 'secondary', { readOnly: false })
      },
      documentModel: {
        prerequisites: [ 'primaryDocumentModel', 'secondaryDocumentModel' ],
        implementation: mergeStep
      }
    };
  }
};

function openEmptyDocument(dataCollectionDefinition, input, options) {
  var defaultOptions = {
    readOnly: false,
    loadStableData: makeLoadStableData(input),
    enrichPatientID: makeEnrichPatientID(input)
  };
  return documentmodelHelpers.openDocument(
    dataCollectionDefinition,
    { context: input.context }, // no intial content
    _.extend(defaultOptions, options));
}

function makeLoadStableData(input) {
  var accessToken = input.accessToken;
  if (!_.isString(accessToken) || _.isEmpty(accessToken)) {
    return null;
  }
  var dataCollectionName = input.dataCollectionDefinition.label;
  if (!_.isString(dataCollectionName) || _.isEmpty(dataCollectionName)) {
    return null;
  }
  return function (patientID, cb) {
    request.get({
      uri: config.WORKFLOW_ENGINE_BASE_URL + '/stabledata/mapping/' +
        patientID + '/' + dataCollectionName,
      json: true,
      headers: {
        Authorization: accessToken
      }
    }, function (err, response, body) {
      if (err) {
        return cb(err);
      }
      cb(null, body);
    });
  };
}

function makeEnrichPatientID(input) {
  var accessToken = input.accessToken;
  if (!_.isString(accessToken) || _.isEmpty(accessToken)) {
    return null;
  }
  return function (patientID, cb) {
    request.get({
      uri: config.WORKFLOW_ENGINE_BASE_URL + '/patient/mapping/' + patientID,
      json: true,
      headers: {
        Authorization: accessToken
      }
    }, function (err, response, body) {
      if (err) {
        return cb(err);
      }
      cb(null, body);
    });
  };
}

function importCsvStep(dataCollectionDefinition, input, cb) {
  var doc = openEmptyDocument(dataCollectionDefinition, input);
  csvMapping.mapCsvToDocument(doc, input.csv, cb);
}

function affectedFieldsStep(document, input, cb) {
  cb(null, documentmodelHelpers.affectedFields(document));
}

function importBasicDatasetStep(dataCollectionDefinition, input, cb) {
  expectProperty(input, 'basicDataset', _.isString, function (err, basicDataset) {
    if (err) { return cb(err); }
    var doc = openEmptyDocument(dataCollectionDefinition, input);
    basicDataSet.importFromBasicDataSetWithCBBs(doc, basicDataset);
    documentmodelHelpers.waitForDocumentReady(doc, cb);
  });
}

function mergeStep(primaryDocumentModel, secondaryDocumentModel, input, cb) {
  mergeMapping.merge(primaryDocumentModel, secondaryDocumentModel);
  documentmodelHelpers.waitForDocumentReady(primaryDocumentModel, cb);
}

function defaultTargets() {
  return _.extend({},
    dataCollectionDefinitionTarget(),
    openDocumentTarget(),
    documentInfoTargets('documentModel'));
}
