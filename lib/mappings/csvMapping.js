/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var csv = require('csv');
var documentmodel = require('documentmodel');
var _ = require('underscore');
var documentmodelHelpers = require('../documentmodelHelpers');

var makeCSVBuilder = documentmodel.csvMapping.makeCSVBuilder;

function mapCsvToDocument(document, inputData, cb) {
  var parseOptions = {
    columns: true,
    delimiter: ';'
  };
  csv.parse(inputData, parseOptions, function (err, result) {
    if (err) { return cb(err); }
    if (result.length !== 1) {
      var error = new Error('Expected CSV with 1 columns row and 1 data row');
      error.badRequest = true;
      return cb(error);
    }
    result = result[0];
    var affected = document.inExtendedEditContext('importCSV', function () {
      return document.root.autoCompleteWith({ csv: result });
    });
    document.csvAffectedFields = _.object(_.map(affected, function (manipulator) {
      var path = manipulator.path.join('.');
      return [ path, { importedFromCSV: true } ];
    }));
    documentmodelHelpers.waitForDocumentReady(document, cb);
  });
}

function isStableField(manipulator) {
  return Boolean(manipulator.description.isStable);
}

function processDocument(document, csvBuilder, options) {
  options = options || {};
  if (options.stable) {
    options.skip = function (manipulator) {
      return !isStableField(manipulator);
    };
  }
  return documentmodel.csvMapping.exportDocumentToCSV(document, csvBuilder, options);
}

function mapDocumentToCSV(document, options, cb) {
  if (_.isFunction(options)) {
    cb = options;
    options = {};
  }
  var csvBuilder = makeCSVBuilder();
  processDocument(document, csvBuilder, options);
  var protoCSV = csvBuilder.toProtoCSV();
  csv.stringify([ protoCSV.columns ].concat(protoCSV.rows), { delimiter: ';' }, cb);
}

function mapDocumentToUniqueID(document, cb) {
  var uniqueID = document.root.computedExpressions.uniqueID;
  return cb(null, uniqueID);
}

module.exports.mapCsvToDocument = mapCsvToDocument;
module.exports.mapDocumentToUniqueID = mapDocumentToUniqueID;
module.exports.mapDocumentToCSV = mapDocumentToCSV;
module.exports.csvData = documentmodel.csvMapping.csvData;
module.exports.pathAsString = documentmodel.csvMapping.pathAsString;
module.exports.simpleValueCSV = documentmodel.csvMapping.simpleValueCSV;
module.exports.exportDocumentsToCSV = function (documents, options, cb) {
  if (typeof options === 'function') {
    cb = options;
    options = {};
  }
  options = options || {};
  var csvBuilder = makeCSVBuilder();
  _.each(documents, function (document) {
    processDocument(document, csvBuilder, options);
  });
  var protoCSV = csvBuilder.toProtoCSV();
  csv.stringify([ protoCSV.columns ].concat(protoCSV.rows), { delimiter: ';' }, cb);
};
