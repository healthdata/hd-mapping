/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment');
var config = require('../../config');
var request = require('../referenceList/referenceList');
var asyncParallel = require('../asyncParallel');
var documentmodelHelpers = require('../documentmodelHelpers');

var STRING_ANALYZED_SORTABLE = {
  type: 'string',
  analyzer: 'case_insensitive_sort',
  fields: {
    searchable: {
      type: 'string'
    }
  }
};

var STRING_NOT_ANALYZED = {
  type: 'string',
  index: 'not_analyzed'
};

var DATE = {
  type: 'date'
};

var LONG = {
  type: 'long'
};

var BOOLEAN = {
  type: 'boolean'
};

module.exports = {
  mappingFromDCDtoEsmapping: mappingFromDCDtoEsmapping,
  mappingFromDocumentToEs: mappingFromDocumentToEs
};

function mappingFromDCDtoEsmapping(dataCollectionDefinition, cb) {
  return cb(undefined, {
    properties: {
      id: { type: 'long' },
      dataCollectionDefinitionId: LONG,
      createdOn: DATE,
      updatedOn: DATE,
      context: {
        properties: {
          installationId: STRING_NOT_ANALYZED,
          followUps: {
            type: 'nested',
            properties: {
              activationDate: DATE,
              active: BOOLEAN,
              conditions: STRING_NOT_ANALYZED,
              description: { type: 'string' },
              label: { type: 'string' },
              name: STRING_NOT_ANALYZED,
              timing: STRING_NOT_ANALYZED
            }
          }
        }
      },
      flags: {
        properties: {
          corrections: BOOLEAN,
          followUp: BOOLEAN
        }
      },
      dataCollectionName: STRING_NOT_ANALYZED,
      hd4dpWorkflowId: STRING_NOT_ANALYZED,
      firstSaveAction: {
        properties: {
          action: STRING_NOT_ANALYZED,
          executedBy: STRING_NOT_ANALYZED,
          source: STRING_NOT_ANALYZED,
          messageType: STRING_NOT_ANALYZED,
          file: STRING_NOT_ANALYZED,
          executedOn: DATE,
          newStatus: STRING_NOT_ANALYZED
        }
      },
      history: {
        type: 'nested',
        properties: {
          action: STRING_NOT_ANALYZED,
          executedOn: DATE,
          newStatus: STRING_NOT_ANALYZED,
          source: STRING_NOT_ANALYZED,
          executedBy: STRING_NOT_ANALYZED
        }
      },
      status: STRING_NOT_ANALYZED,
      document: {
        properties: {
          documentData: {
            enabled: false
          },
          documentContent: {
            properties: {
              patientID: {
                properties: {
                  patientID: STRING_NOT_ANALYZED,
                  internalPatientID: STRING_NOT_ANALYZED,
                  firstName: STRING_ANALYZED_SORTABLE,
                  lastName: STRING_ANALYZED_SORTABLE,
                  birthdate: DATE
                }
              },
              display: displayValuesMapping(dataCollectionDefinition),
              commentsData: {
                type: 'string'
              },
              originalDocumentData: {
                enabled: false
              },
              original: {
                enabled: false
              }
            }
          }
        }
      },
      metaData: {
        enabled: false
      },
      identificationType: STRING_NOT_ANALYZED,
      identificationValue: STRING_NOT_ANALYZED,
      senderDetails: {
        properties: {
          applicationId: STRING_NOT_ANALYZED,
          codageFilename: STRING_NOT_ANALYZED,
          id: LONG,
          identificationType: STRING_NOT_ANALYZED,
          identificationValue: STRING_NOT_ANALYZED
        }
      }
    }
  });
}

function mappingFromDocumentToEs(document, cb) {

  var translationsBaseURL = config.WORKFLOW_ENGINE_BASE_URL + '/translations';

  asyncParallel({
    nlTranslations: function (cb) {
      request.httpGET(translationsBaseURL + '?language=nl', cb);
    },
    frTranslations: function (cb) {
      request.httpGET(translationsBaseURL + '?language=fr', cb);
    }
  }, returnElasticSearchDocument);

  function returnElasticSearchDocument(err, translations) {
    if (err) {
      return cb(err);
    }
    var result = {};
    result.errors = document.errors.length;
    result.warnings = document.warnings.length;
    result.progress = documentmodelHelpers.progressInfo(document);
    result.comments = document.comments.length;
    if (result.errors) {
      result.errorsInfo = _.map(document.errors, displayErrorWarningInfo);
    }
    if (result.warnings) {
      result.warningsInfo = _.map(document.warnings, displayErrorWarningInfo);
    }
    result.originalDocumentData = document.documentData;
    result.display = displayValues(document.root, translations);
    var commentMessages = [];
    _.each(document.documentData.content.comments, function (perField) {
      _.each(perField, function (comment) {
        if (comment === Object(comment) && typeof comment.message === 'string') {
          commentMessages.push(comment.message);
        }
      });
    });
    if (commentMessages.length) {
      result.commentsData = commentMessages;
    }
    var commentsCounts = _.countBy(document.comments, function (commentInfo) {
      var comment = commentInfo.comment;
      var resolved = comment.solved === true || comment.solved === 'reopen';
      return resolved ? 'resolved' : 'open';
    });
    result.openComments = commentsCounts.open;
    result.resolvedComments = commentsCounts.resolved;
    result.patientID = patientIDInfo(document);

    return cb(undefined, result);
  }
}

function patientIDInfo(document) {
  var patientIDField = primaryPatientID(document);
  if (!patientIDField) { return {}; }
  var props = patientIDField.propertyManipulators;
  return {
    patientID: patientIDField.value,
    internalPatientID: props.internal_patient_id && props.internal_patient_id.value,
    firstName: props.first_name && props.first_name.value,
    lastName: props.name && props.name.value,
    birthdate: props.date_of_birth && date(props.date_of_birth.value)
  };
  function date(value) {
    if (!_.isDate(value)) { return; }
    var m = moment(value);
    if (!m.isValid(m)) { return; }
    return m.format('YYYY-MM-DD');
  }
}

function primaryPatientID(document) {
  var patientIDFields = _.filter(document.registeredManipulators, function (m) {
    return m.description.type === 'patientID';
  });
  if (patientIDFields.length === 1) {
    return patientIDFields[0];
  }
  var byName = _.find(patientIDFields, function (m) {
    return _.last(m.path) === 'patient_id';
  });
  return byName ? byName : patientIDFields[0];
}

var MULTI_LANGUAGE_STRING = {
  properties: {
    en: { type: 'string', analyzer: 'english' },
    fr: { type: 'string', analyzer: 'french' },
    nl: { type: 'string', analyzer: 'dutch' }
  }
};

function displayValuesMapping(description) {
  var type = description.type;
  var result = {
    properties: {
      contentType: STRING_NOT_ANALYZED,
      fieldLabel: { enabled: false },
      nested: {
        properties: {}
      }
    }
  };
  function mappingProperty(name, mapping) {
    result.properties[name] = mapping;
  }
  function property(name, mapping) {
    result.properties.nested.properties[name] = mapping;
  }
  function properties(descriptions) {
    _.each(descriptions, function (nested, name) {
      property(name, displayValuesMapping(nested));
    });
  }
  if (type === 'object') {
    properties(description.nestedDescriptions);
  }
  if (type === 'list') {
    mappingProperty('value', { type: 'integer' });
    mappingProperty('elements', displayValuesMapping(description.nestedDescriptions.listElement));
  }
  if (type === 'patientID') {
    mappingProperty('value', STRING_NOT_ANALYZED);
    properties(description.nestedDescriptions);
  }
  if (type === 'referencelist') {
    mappingProperty('value', STRING_NOT_ANALYZED);
    mappingProperty('labels', MULTI_LANGUAGE_STRING);
  }
  if (type === 'text') {
    mappingProperty('value', STRING_ANALYZED_SORTABLE);
  }
  if (type === 'number') {
    mappingProperty('value', { type: 'double' });
  }
  if (type === 'choice') {
    mappingProperty('value', STRING_NOT_ANALYZED);
    mappingProperty('selectedOptionLabel', MULTI_LANGUAGE_STRING);
    _.each(description.optionProperties, function (optionProperties) {
      properties(optionProperties);
    });
  }
  if (type === 'multichoice') {
    mappingProperty('value', STRING_NOT_ANALYZED);
    mappingProperty('selectedOptionsLabels', MULTI_LANGUAGE_STRING);
    _.each(description.optionProperties, function (optionProperties) {
      properties(optionProperties);
    });
  }
  if (type === 'boolean') {
    mappingProperty('value', { type: 'boolean' });
  }
  if (type === 'date') {
    mappingProperty('value', { type: 'date' });
    mappingProperty('rendered', { type: 'string' });
  }
  return result;
}

function displayValues(rootManipulator, translations) {

  function allLanguages(str) {
    return str && {
      en: str,
      fr: translations.frTranslations[str] || str,
      nl: translations.nlTranslations[str] || str
    };
  }

  return recDV(rootManipulator);

  function recDV(manipulator) {
    try {
      return recDVBody(manipulator);
    } catch (err) {
      return undefined;
    }
  }

  function recDVBody(manipulator) {

    var description = manipulator.description;
    var type = description.type;
    var result = {
      fieldLabel: allLanguages(description.label),
      isActive: manipulator.isActive(),
      contentType: type
    };

    function addProperties(manipulators) {
      _.each(manipulators, function (pm, propertyName) {
        if (!result.nested) {
          result.nested = {};
        }
        result.nested[propertyName] = recDV(pm);
      });
    }

    if (type === 'object') {
      addProperties(manipulator.propertyManipulators);
    }
    if (type === 'list') {
      var ems = manipulator.elementManipulators;
      result.value = ems.length || undefined;
      result.elements = _.map(ems, recDV);
    }
    if (type === 'patientID') {
      addProperties(manipulator.propertyManipulators);
      result.value = manipulator.value;
    }
    if (type === 'referencelist') {
      result.value = manipulator.getValue();
      result.labels = manipulator.allLanguages();
    }
    if (type === 'text') {
      result.value = manipulator.value;
    }
    if (type === 'number') {
      result.value = manipulator.value;
    }
    if (type === 'choice') {
      var selected = manipulator.selectedOption;
      if (selected) {
        result.value = selected;
        var optionDescription = description.optionDescriptions[selected];
        if (optionDescription) {
          result.selectedOptionLabel = allLanguages(optionDescription.label || selected);
        }
      }
      _.each(manipulator.optionManipulators, function (optionManipulator) {
        addProperties(optionManipulator.propertyManipulators);
      });
    }
    if (type === 'multichoice') {
      result.values = [];
      result.selectedOptionsLabels = [];
      _.each(manipulator.optionManipulators, function (optionManipulator, optionName) {
        if (optionManipulator.isSelected) {
          result.values.push(optionName);
          var label = manipulator.description.optionDescriptions[optionName].label;
          result.selectedOptionsLabels.push(allLanguages(label || optionName));
        }
        addProperties(optionManipulator.propertyManipulators);
      });
      if (result.values.length) {
        result.value = result.values[0];
      }
    }
    if (type === 'boolean') {
      result.value = manipulator.value;
    }
    if (type === 'date') {
      if (manipulator.value instanceof Date) {
        var m = moment(manipulator.value);
        if (m.isValid()) {
          result.value = m.format('YYYY-MM-DD');
          result.rendered = m.format('DD-MM-YYYY');
        }
      }
    }
    if (type === 'question') {
      var answer = manipulator.lookupAnswer();
      if (answer) {
        result.answerLabel = allLanguages(answer.label);
      }
    }
    return result;
  }
}

function displayErrorWarningInfo(info) {
  var m = info.manipulator;
  var description = m.description;
  var assertion = info.assertion;
  var message = description.messageFor(assertion) ||
                'error code "' + assertion + '"';
  var label = description.label;
  var result = {
    location: m.path.join('|'),
    assertion: assertion,
    message: (label ? (label + ': ') : '') + message
  };
  if (m.validationFailureInfo) {
    result.technicalInfo = m.validationFailureInfo;
  }
  return result;
}
