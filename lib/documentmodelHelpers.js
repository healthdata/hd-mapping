/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment');
var uuid = require('node-uuid');
var documentmodel = require('documentmodel');

var makeHDDataSource = require('./makeHDDataSource');
var makePendingAttachmentService = require('./attachment/pending');

module.exports = {
  compile: compile,
  progressInfo: progressInfo,
  validationInfo: validationInfo,
  waitForDocumentReady: waitForDocumentReady,
  affectedFields: affectedFields,
  openDocument: openDocument
};

function compile(documentDefinition, env) {
  // due to circular dependency between this file and hdProfile, we need
  // to load it lazily
  var hdProfile = require('./hdProfile');
  documentmodel.parseDocumentDefinition(documentDefinition, hdProfile).compile(env);
  if (env.skrDescription) {
    env.skrDescription.requiresSupportForMultiplePatientIDs =
      documentDefinition.requiresSupportForMultiplePatientIDs;
  }
}

function progressInfo(document) {
  var result = {};
  _.extend(result, document.progress);
  result.errors = document.errors.length;
  result.warnings = document.warnings.length;
  return result;
}

function affectedFields(document) {
  return _.chain(document.registeredManipulators)
    .map(function (m) {
      var source = _.reduce(m.editContext,
        function (soFar, name) {
          // defaultValue should win over initialize
          if (soFar === 'defaultValue' && name === 'initialize') {
            return soFar;
          }
          if (soFar === 'stableData' && name === 'merge') {
            return soFar;
          }
          return name;
        },
        null);
      if (source === 'edit') {
        source = 'manual';
      }
      // we ignore intialization
      if (source && source !== 'initialize') {
        return [ m.path.join('.'), { reason: source } ];
      }
    })
    .compact()
    .object()
    .value();
}

function waitForDocumentReady(document, timeout, cb) {
  if (_.isFunction(timeout)) {
    cb = timeout;
    timeout = 10000;
  }
  var tmo = setTimeout(continueAnyway, timeout);
  var unsub = document.listen(function () {
    if (document.isReady()) {
      unsub();
      clearTimeout(tmo);
      // it is necessary to invoke the callback in a separate tick, otherwise
      // we might be interrupting whatever logic that triggered the listener
      setTimeout(function () {
        cb(null, document);
      });
    }
  });
  function continueAnyway() {
    unsub();
    cb(null, document);
  }
  // needed to trigger the listener at least once
  document.invalidate();
}

function getPath(obj, path) {
  return _.reduce(path.split('.'), function (memo, propertyName) {
    return (typeof memo === 'undefined' || memo === null) ? memo : memo[propertyName];
  }, obj);
}

function openDocument(dataCollectionDefinition, data, options, cb) {
  if (_.isFunction(data) && _.isUndefined(options) && _.isUndefined(cb)) {
    cb = data;
    data = undefined;
    options = undefined;
  }
  if (_.isUndefined(cb) && _.isFunction(options)) {
    cb = options;
    options = undefined;
  }
  var requiresSupportForMultiplePatientIDs = dataCollectionDefinition.
    requiresSupportForMultiplePatientIDs;
  options = options || {};
  data = data || {};
  var context = data.context;
  var activeFollowUps = _.object(_.map((data.context || {}).followUps, function (followUp) {
    return [ String(followUp.name), Boolean(followUp.active) ];
  }));
  var documentData = {
    content: data.content,
    patientID: data.patientID,
    private: data.private,
    coded: data.coded
  };
  var dataSource = makeHDDataSource(
    documentData,
    requiresSupportForMultiplePatientIDs);
  var isPseudonymised = dataSource.isPseudonymised();
  var hdWorkflowProps = dataSource.hdWorkflowProps();
  var defaultOptions = {
    readOnly: true,
    isPseudonymised: isPseudonymised,
    computedExpressionArguments: _.extend(
      documentmodel.helpers.defaultComputedExpressionArguments(), {
        'generateUUID': _.constant(generateUUID),
        'moment': _.constant(moment),
        'ctx': _.constant(hdWorkflowProps),
        'followUpActive': _.constant(function (name) {
          return Boolean(activeFollowUps[name]);
        }),
        'context': _.constant(_.partial(getPath, context))
      })
  };
  var services = options.services || {};
  services.attachment = services.attachment || makePendingAttachmentService();
  var doc = documentmodel.makeDocument(dataSource, dataCollectionDefinition,
    _.extend(defaultOptions, options));
  doc.documentData = documentData;
  doc.context = context;
  doc.services = services;
  doc.toBeCoded = _.partial(computeToBeCoded, doc, dataSource);
  // The "target" property on documents is used in computed expressions and can therefore
  // not be removed.
  doc.target = documentData.content;
  if (!_.isUndefined(cb)) {
    var waitForReadyTimeout = _.isNumber(options.waitForReadyTimeout) ?
      options.waitForReadyTimeout : 10000;
    waitForDocumentReady(doc, waitForReadyTimeout, _.partial(cb, null, doc));
  }
  return doc;
}

function computeToBeCoded(document, dataSource) {
  var codedFields = _.chain(document.registeredManipulators)
    .map(function (manipulator) {
      var behaviour = manipulator.description.behaviour;
      if (!behaviour.$provides('toBeCoded')) { return []; }
      return behaviour.toBeCoded(manipulator);
    })
    .flatten()
    .value();
  return dataSource.formatToBeCodedData(codedFields);
}

function generateUUID() {
  return uuid.v4();
}

function validationInfo(document) {

  function present(errorInfo) {
    var m = errorInfo.manipulator;
    var description = m.description;
    var assertion = errorInfo.assertion;
    var message = description.messageFor(assertion) ||
                  'error code "' + assertion + '"';
    var label = description.label;
    var result = {
      location: m.path,
      assertion: assertion,
      message: (label ? (label + ': ') : '') + message,
      value: m.read()
    };
    if (m.validationFailureInfo) {
      result.technicalInfo = m.validationFailureInfo;
    }
    return result;
  }

  var foundErrors = _.map(document.errors, present);
  var foundWarnings = _.map(document.warnings, present);

  return {
    has_validation_errors: foundErrors.length > 0,
    validation_errors: foundErrors,
    has_validation_warnings: foundWarnings.length > 0,
    validation_warnings: foundWarnings
  };
}
