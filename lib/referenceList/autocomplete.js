/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = {
  simpleAutocompleteMixin: simpleAutocompleteMixin,
  filteredAutocompleteMixin: filteredAutocompleteMixin
};

function simpleAutocompleteMixin(base, refList) {
  base.autocompleteFor = function (query, cb) {
    refList.autocomplete(query, cb);
  };
}

function filteredAutocompleteMixin(base, refList) {

  var currentFilter;

  base.isFiltered = false;
  base.activateFilter = function (filter) {
    base.isFiltered = true;
    currentFilter = filter;
  };
  base.clearFilter = function () {
    base.isFiltered = false;
    currentFilter = undefined;
  };

  base.checkFilter = function (match) {
    if (!base.isFiltered) { return true; }
    return currentFilter(match);
  };

  base.autocompleteFor = function (query, cb) {
    refList.autocomplete(query, function (err, matches) {
      if (err) { return cb(err); }
      return cb(undefined, _.filter(matches, currentFilter));
    });
  };
}
