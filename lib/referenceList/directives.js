/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var csvMapping = require('../mappings/csvMapping');
var currentLanguage = require('../currentLanguage');

var asyncState = require('../asyncState');
var basicState = require('./basicState');
var bnmdr = require('./bnmdr');
var referenceList = require('./referenceList');
var autocomplete = require('./autocomplete');

function queryStateDescription(description, binding) {

  // assertion for checking whether the query corresponds to a match
  // the directive typeahead-editable="false" does a similar job, but
  // does not play well with document validation.
  description.assertions.queryMatch = {
    check: function (data, manipulator) {
      var query = manipulator.getQuery();
      if (query === Object(query)) {
        // query is an object
        if (manipulator.state.isFiltered) {
          return manipulator.state.checkFilter(query) ? 'ok' : 'failed';
        }
        return 'ok';
      }
      if (query === '' || typeof query === 'undefined') {
        return 'pending';
      }
      return binding ? 'failed' : 'ok';
    }
  };

  description.messages.queryMatch = 'Please select a valid value from the list';

  if (!binding) {
    description.emptyPredicates.push(function (data, manipulator) {
      return manipulator.getQuery() === '';
    });
  }

}

function renderMatchMixin(base) {
  base.renderMatch = function (match) {
    if (typeof match !== 'object' || match === null) { return ''; }
    return match.value;
  };
}

function referenceListState(manipulator, refListName, getLang, filter, binding) {

  var limit = manipulator.description.limit;

  if (refListName === 'BNMDR' && typeof limit === 'undefined') {
    limit = 0;
  }

  if (filter) {
    limit = 0;
  }

  var refList = referenceList(refListName, getLang, limit);
  var result = basicState(refList, function () { manipulator.invalidate(); });

  // model for the search text field
  var query;

  // initialize query when non-binding
  // otherwise, the initialization will occur via processMatch()
  if (!binding && typeof manipulator.read() === 'string') {
    query = manipulator.read();
  }

  _.extend(manipulator, {
    getQuery: function () { return query; },
    inputQuery: function (q) {
      manipulator.edit(function () {
        query = q;
        return binding ? undefined : q;
      });
    }
  });

  manipulator.setCode = function (code) {
    manipulator.edit(function () {
      query = undefined;
      return binding ? { code: code } : code;
    });
  };

  manipulator.setMatch = function (match) {
    manipulator.edit(function () {
      query = match;
      return { code: match.code };
    });
  };

  manipulator.clearSpecific = function () {
    // avoid unneeded edit when already empty
    if (_.isUndefined(query) && _.isUndefined(manipulator.read())) {
      return;
    }
    manipulator.edit(function () {
      query = undefined;
      return undefined;
    });
  };

  if (refListName === 'BNMDR') {
    autocomplete.filteredAutocompleteMixin(result, refList);
    bnmdr.crampMixin(result, refList, manipulator);
    manipulator.parent.nihdiCrampLink = manipulator.parent.nihdiCrampLink || {};
    manipulator.parent.nihdiCrampLink.cramp = manipulator;
    result.becomeMatch = function (match) {
      result.refTree.setCurrentMatch(match);
    };
  } else {
    if (filter || refListName === 'NIHDI') {
      autocomplete.filteredAutocompleteMixin(result, refList);
    } else {
      autocomplete.simpleAutocompleteMixin(result, refList);
    }
    result.becomeMatch = function (match) {
      manipulator.setMatch(match);
    };
  }

  renderMatchMixin(result);

  if (refListName === 'NIHDI') {
    manipulator.parent.nihdiCrampLink = manipulator.parent.nihdiCrampLink || {};
    manipulator.parent.nihdiCrampLink.nihdi = manipulator;
  }

  // we only allow one filter per manipulator
  var computeFilter;
  function setupReferenceListFilter(link, filterValue, reverse, filterSatisfied) {
    computeFilter = setupComputeFilter(link, filterValue, reverse,
      function () { manipulator.invalidate(); },
      filterSatisfied);
  }

  if (filter && filter.value && filter.link) {
    setupReferenceListFilter(
      filter.link,
      function () { return manipulator.computedExpressions[filter.value]; },
      filter.reverse);
  } else if (refListName === 'NIHDI') {
    setupReferenceListFilter(
      'BNMDR-NIHDI-CODE',
      function () {
        if (!manipulator.parent.nihdiCrampLink.cramp) { return; }
        return manipulator.parent.nihdiCrampLink.cramp.state.refTree.currentNode.match;
      },
      false /* not reversed */);
  } else if (refListName === 'BNMDR') {
    setupReferenceListFilter(
      'BNMDR-NIHDI-CODE',
      function () {
        if (!manipulator.parent.nihdiCrampLink.nihdi) { return; }
        return manipulator.parent.nihdiCrampLink.nihdi.read();
      },
      true /* reversed */,
      // For the BNMDR reference list, we apply a custom filter test that takes
      // into account the tree structure. A match satisfies a filter not only
      // when the exact code appears in the list, but also when it is a parent
      // of a valid filtered code.
      // See WDC-2519.
      function (codes, code) {
        // remove trailing .0
        var strippedCode = code.replace(/(\.0)+$/, '');
        // search for a code in the filter that starts with strippedCode
        return _.some(codes, function (filterCode) {
          return filterCode === strippedCode ||
                 filterCode.lastIndexOf(strippedCode + '.', 0) === 0;
        });
      });
  }

  var appliedUniqueMatch;

  function reinit() {

    // load match
    var value = manipulator.read();
    var code = value;
    if (_.isObject(value)) {
      code = value.code;
    }
    result.becomeCode(code);
    var currentMatch = result.currentMatch();
    if (result.currentMatch() && !_.isObject(value)) {
      // a code in the non-binding case that matches is automatically converted
      manipulator.edit(function () {
        return { code: code };
      });
    }

    var queryIsEmpty = _.isUndefined(query) || _.isNull(query) || _.isEmpty(query);
    if (currentMatch && queryIsEmpty) {
      query = currentMatch;
    }

    manipulator.refListReady = result.isReady();

    var failMatchInfo = result.failMatchInfo();
    if (failMatchInfo) {
      var err = failMatchInfo.error;
      var matches = failMatchInfo.matches;
      if (err) {
        manipulator.validationFailureInfo = err.causeInfo ? err.causeInfo : err.message;
      }
      if (matches && matches.length !== 1) {
        manipulator.validationFailureInfo = 'Found no unique match for code ' + code +
          '. Result was ' + JSON.stringify(matches);
      }
      // do not display the error message if we were trying to resolve
      // a non-bound value
      manipulator.failMatch = _.isObject(value) ? code : undefined;
    } else {
      manipulator.failMatch = undefined;
    }

     // If we want to perform some prefilling based on a filter, we prefer to do it
     // at the end of invalidation.
    var filterCascadingAction;
    if (computeFilter) {
      var computedFilter = computeFilter();
      // make other operations wait until the filter is loaded
      manipulator.refListReady = manipulator.refListReady && !computedFilter.busy;
      // manage the filter state for autocompletion
      var resolvedFilter = computedFilter.resolvedFilter;
      if (resolvedFilter) {
        result.activateFilter(resolvedFilter.filterPredicate);
      } else {
        result.clearFilter();
      }
      if (result.refTree) {
        result.refTree.setFilter(resolvedFilter ? resolvedFilter.filterCodes : undefined);
      }
      // If there is a unique match, that we want to apply just once. We do this by
      // tracking the unique match that has already been applied. Furthermore,
      // we only want to apply the unique match if the field was empty.
      // Otherwise, if there is no unique match to apply, we want to make sure that a
      // previously applied unique match is cleared again, but only when it still
      // corresponds to the unique match.
      if (computedFilter.resolvedFilter && computedFilter.resolvedFilter.uniqueMatch) {
        var uniqueMatch = computedFilter.resolvedFilter.uniqueMatch;
        if (appliedUniqueMatch !== uniqueMatch) {
          appliedUniqueMatch = uniqueMatch;
          if (_.isUndefined(manipulator.read())) {
            filterCascadingAction = function () { manipulator.setCode(uniqueMatch); };
          }
        }
      } else {
        if (appliedUniqueMatch) {
          var toClear = appliedUniqueMatch;
          appliedUniqueMatch = undefined;
          if (code === toClear) {
            filterCascadingAction = function () { manipulator.clear(); };
          }
        }
      }
    }

    var refTreeCascadingAction;
    if (result.refTree) {
      // We want to keep the tree in sync with the query field: if it is a match, the tree should
      // show that match; if the query is empty, or a string, we want the tree to be empty.
      var singleNextNode = result.refTree.updateTree(_.isObject(query) ? query : undefined);
      // To avoid a race condition between applying a single next node due to
      // filtering, and resolving the code already available, we only apply the
      // single next node if the code is resolved.
      if (singleNextNode && !manipulator.options.readOnly && result.isReady()) {
        refTreeCascadingAction = function () {
          result.refTree.setCurrentNode(singleNextNode);
        };
      }
    }

    if (filterCascadingAction) {
      filterCascadingAction();
    } else if (refTreeCascadingAction) {
      refTreeCascadingAction();
    }
  }

  manipulator.onPostInvalidate(reinit);
  reinit();

  return result;
}

function setupComputeFilter(link, valuef, reverse, notify, filterSatisfied) {
  var linkRefList = referenceList(link, function () { return 'en'; }, 0 /* no limit */);
  var resolve = reverse ? linkRefList.exactValue : linkRefList.exactCode;
  var internal = asyncState(codeEquals, resolveCode, notify);
  function codeEquals(code1, code2) { return code1 === code2; }
  function resolveCode(code, cb) {
    resolve(code, function (err, matches) {
      if (err || matches.length === 0) {
        return cb({ error: err, matches: matches });
      }
      var codes = _.pluck(matches, reverse ? 'code' : 'value');
      function predicate(match) {
        var test = filterSatisfied || _.contains;
        return test(codes, match.code);
      }
      var uniqueMatch;
      if (matches.length === 1) {
        var match = matches[0];
        uniqueMatch = reverse ? match.code : match.value;
      }
      return cb(null, {
        uniqueMatch: uniqueMatch,
        filterCodes: codes,
        filterPredicate: predicate
      });
    });
  }
  return function () {
    var filterValue = valuef();
    // If the new filter value is a code or match, we use it to load the
    // linked codes and apply them as a filter.
    // We take care to apply the reversals required to allow filtering in
    // both directions.
    var code = _.isObject(filterValue) ? filterValue.code : filterValue;
    if (!_.isString(code) || code === '') {
      code = undefined;
    }
    internal.withState(code);
    return {
      filterValue: code,
      busy: internal.busy(),
      resolvedFilter: internal.resolvedResult()
    };
  };
}

var referencelistCompileStep = {
  on: 'hdReferenceList',
  require: [ 'skrDescription' ],
  compile: function (env, config, description) {
    var refListName = description.referenceList = config.referencelist;

    description.type = 'referencelist';
    description.label = config.label || config.name;
    description.help = config.help;

    // Explicitly check for 'false' value. Default is true.
    var binding = config.binding !== false;

    var filter = config.filter;

    description.isReadyPredicates.push(function (manipulator) {
      return manipulator.refListReady;
    });

    queryStateDescription(description, binding);

    // assertion for checking whether the code could be resolved
    description.assertions.codeResolved = {
      check: function (data, manipulator) {
        return manipulator.failMatch ? 'failed' : 'ok';
      }
    };

    description.messages.codeResolved = 'The provided code could not be resolved.';

    description.addManipulatorBuilder(function (manipulator) {

      /**
       * Check whether the current value represents a code
       **/
      manipulator.hasCode = function () {
        var currentValue = manipulator.read();
        // is it an object? does it have a 'code' property?
        return currentValue === Object(currentValue) &&
               Object.hasOwnProperty.call(currentValue, 'code');
      };

      var state = manipulator.state =
        referenceListState(manipulator, refListName, currentLanguage.getCurrentLanguage,
          filter, binding);

      manipulator.setValue = function (value) {
        // skip if the value matches the current value
        // this is not (just) an optimization; in the case of non-binding fields, proceeding might
        // replace { code: 'xyz' } with just 'xyz' which we want to avoid
        // relevant ticket: WDC-1003
        if (value === manipulator.getValue()) { return; }
        manipulator.edit(function () {
          // If there is a match, the manipulator will be reset to { code: ... }
          return binding ? { code: value } : value;
        });
      };

      manipulator.getValue = function () {
        var value = manipulator.read();
        return manipulator.hasCode() ? value.code : value;
      };

      manipulator.allLanguages = function () {
        var data = state.currentMatchAllLanguages();
        return _.object(_.map(_.indexBy(data, 'language'), function (match, key) {
          return [ key, match.value ];
        }));
      };

      manipulator.currentLanguage = function () {
        var result = manipulator.getValue();
        if (manipulator.hasCode()) {
          var data = state.currentMatch();
          if (data && data.value) {
            result = data.value;
          }
        }
        return result;
      };

    });
  }
};

var referencelistCSVTrait = csvMapping.simpleValueCSV({
  on: 'hdReferenceList',
  exportToCSV: function (manipulator) {
    return manipulator.getValue();
  },
  importFromCSV: function (manipulator, value) {
    if (typeof value === 'number') {
      value = String(value);
    }
    if (typeof value !== 'string') { return; }
    if (value === '') { return; }
    manipulator.setValue(value);
    return true;
  }
});

module.exports = {
  referencelistCompileStep: referencelistCompileStep,
  referencelistCSVTrait: referencelistCSVTrait
};
