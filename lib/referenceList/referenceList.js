/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var url = require('url');
var config = require('../../config');
var _ = require('underscore');

referenceList.httpGET = function (uri, cb) {
  return cb(new Error('referenceList.httpGET not set'));
};

referenceList.WORKFLOW_ENGINE_BASE_URL = config.WORKFLOW_ENGINE_BASE_URL;

function referenceList(type, getLang, limit) {

  var httpGET = referenceList.httpGET;

  return {
    children: children,
    exactCode: exactCode,
    exactCodeAllLanguages: exactCodeAllLanguages,
    exactValue: exactValue,
    autocomplete: autocomplete
  };

  function request(params, cb) {
    params.type = type;
    if (typeof limit !== 'undefined') {
      params.limit = limit;
    }
    var baseURL = referenceList.WORKFLOW_ENGINE_BASE_URL;
    var queryURL = baseURL + url.format({ pathname: '/datareference', query: params });
    httpGET(queryURL, function (err, result) {
      if (err) {
        err.causeInfo = 'Could not get reference list data. URL used is ' + queryURL +
          ' and the error message is ' + err.message;
      }
      return cb(err, result);
    });
  }

  function children(code, cb) {
    request({ language: getLang(), parentCode: code }, cb);
  }

  function exactCode(code, cb) {
    request({ language: getLang(), code: code }, cb);
  }

  function exactCodeAllLanguages(code, cb) {
    request({ code: code }, function (err, result) {
      if (err) { return cb(err); }
      var lang = getLang();
      var matches = _.filter(result, function (match) { return match.language === lang; });
      return cb(undefined, matches, result);
    });
  }

  function autocomplete(query, cb) {
    request({
      language: getLang(),
      value: query,
      parentCode: null
    }, cb);
  }

  function exactValue(value, cb) {
    request({
      language: getLang(),
      value: value,
      filter: 'EXACT'
    }, cb);
  }
}

module.exports = referenceList;
