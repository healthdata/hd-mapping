/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = function referenceTree(refList, updateValue, invalidate) {

  var nodeByCodeCache = {};

  var filter;

  var treeRoot = {
    isTreeRoot: true,
    parent: null,
    parents: [],
    label: 'Tree root'
  };

  setChildren(treeRoot, 'NULL');
  applyFilter(treeRoot);

  var self = {
    setCurrentMatch: setCurrentMatch,
    setCurrentNode: setCurrentNode,
    updateTree: updateTree,
    treeRoot: treeRoot,
    currentNode: treeRoot,
    setFilter: setFilter
  };

  return self;

  function setFilter(f) {
    filter = f;
  }

  function applyFilter(node) {
    // Load the children. Note that in the rest of the function, we may not have
    // the actual children yet. When the actual children are available, we count
    // on updateTree() to be invoked, to recompute the correct filtered result.
    node.loadChildrenIfNotLoaded();
    var children = node.children;
    // We keep track of which filter has already been applied to which children.
    // That way we avoid computing the filteredChildren over and over again.
    var skip = _.has(node, 'lastAppliedFilter') &&
               _.has(node, 'filterLastAppliedOn') &&
               node.lastAppliedFilter === filter &&
               node.filterLastAppliedOn === children;
    if (skip) { return; }

    node.lastAppliedFilter = filter;
    node.filterLastAppliedOn = children;

    var filteredChildren = children;
    if (filter instanceof Array) {
      filteredChildren = _.filter(children, function (child) {
        return matchesFilter(child.match.code);
      });
    }
    node.filteredChildren = filteredChildren;

    function matchesFilter(code) {
      // remove trailing .0
      var strippedCode = code.replace(/(\.0)+$/, '');
      // search for a code in the filter that starts with strippedCode
      return _.find(filter, function (filterCode) {
        return filterCode === strippedCode ||
               filterCode.lastIndexOf(strippedCode + '.', 0) === 0;
      });
    }
  }

  function setCurrentNode(node) {
    self.currentNode = node;
    updateValue(node);
    applyFilter(self.currentNode);
  }

  function setCurrentMatch(match) {
    var node = self.currentNode.match === match ? self.currentNode : makeTree(match);
    setCurrentNode(node);
  }

  function updateTree(match) {
    if (_.isUndefined(match)) {
      self.currentNode = treeRoot;
    } else {
      if (self.currentNode.match !== match) {
        self.currentNode = makeTree(match);
      }
    }
    applyFilter(self.currentNode);
    if (self.currentNode.filteredChildren && self.currentNode.filteredChildren.length === 1) {
      return self.currentNode.filteredChildren[0];
    }
  }

  function setComputedParents(treeNode) {
    // Only recompute parents list when parent changes
    var lastSeen;
    var cachedParents = [];
    Object.defineProperty(treeNode, 'parents', {
      get: function () {
        var parent = this.parent;
        if (typeof parent === 'undefined') {
          return cachedParents;
        }
        var paparents = parent.parents;
        if (paparents !== lastSeen) {
          lastSeen = paparents;
          cachedParents = paparents.concat([ parent ]);
        }
        return cachedParents;
      }
    });
  }

  function makeTree(match, parent) {
    var code = match.code;
    if (_.has(nodeByCodeCache, code)) { return nodeByCodeCache[code]; }

    var result = {
      match: match,
      label: match.value
    };
    setParent(result, parent, match.parentCode);
    setChildren(result, match.code);

    nodeByCodeCache[code] = result;

    return result;
  }

  function setParent(treeNode, parent, parentCode) {
    var isRoot = typeof parentCode === 'undefined' ||
                 parentCode === null;
    if (!parent && isRoot) {
      parent = treeRoot;
    }
    if (parent) {
      treeNode.parent = parent;
    } else {
      refList.exactCode(parentCode, function (err, parent) {
        // exactCode() should produce a list with exactly one element
        if (err || parent.length !== 1) {
          // TODO find a better way to do error handling
          parent = {
            value: '[error] could not load value',
            code: parentCode,
            parentCode: null
          };
        } else {
          parent = parent[0];
        }
        var parentTreeNode = makeTree(parent);
        // this will trigger a recomputation of the parents array
        treeNode.parent = parentTreeNode;
        invalidate();
      });
    }
    setComputedParents(treeNode);
  }

  function setChildren(treeNode, code) {
    treeNode.childrenLoading = false;
    treeNode.childrenLoaded = false;
    treeNode.children = [];
    treeNode.isLeaf = true;
    // The only purpose of this function is to allow lazy loading of children.
    // We could also run the code immediately, but then we would basically load
    // the entire CRAMP tree.
    treeNode.loadChildrenIfNotLoaded = function () {
      // If we are already loading, or have already loaded them, there is
      // nothing else to do.
      if (treeNode.childrenLoading || treeNode.childrenLoaded) { return; }
      // Otherwise, we load them, once and for all.
      treeNode.childrenLoading = true;
      refList.children(code, function (err, children) {
        treeNode.childrenLoading = false;
        treeNode.childrenLoaded = true;
        if (err) {
          // TODO do proper error handling
          children = [];
        }
        children = children.map(function (child) {
          return makeTree(child, treeNode);
        });
        treeNode.children = children;
        treeNode.isLeaf = children.length === 0;
        // To trigger any updates, or cascading actions, we notify that
        // something has changed.
        invalidate();
      });
    };
  }
};
