/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var asyncState = require('../asyncState');

module.exports = function (refList, notify) {

  var internal = asyncState(codeEquals, resolveCode, notify);
  function codeEquals(code1, code2) { return code1 === code2; }
  function resolveCode(code, cb) {
    refList.exactCodeAllLanguages(code, function (err, matches, allLanguages) {
      if (err || matches.length !== 1) { return cb({ error: err, matches: matches }); }
      return cb(null, { match: matches[0], allLanguages: allLanguages });
    });
  }

  var self = {

    isReady: function () {
      return !internal.busy();
    },

    currentCode: function () { return internal.currentState(); },
    currentMatch: function () {
      return internal.resolvedResult() ? internal.resolvedResult().match : undefined;
    },
    currentMatchAllLanguages: function () {
      return internal.resolvedResult() ? internal.resolvedResult().allLanguages : undefined;
    },
    failMatchInfo: function () { return internal.resolvedError(); },

    becomeCode: function (code) {
      internal.withState(code);
    }
  };

  return self;
};
