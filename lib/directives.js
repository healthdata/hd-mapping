/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var documentmodel = require('documentmodel');
var autocompleteTrait = documentmodel.autocomplete.autocompleteTrait;
var csvMapping = require('./mappings/csvMapping');

require('./patientID/directives');
require('./referenceList/directives');

// http://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffle(array) {
  var currentIndex = array.length;
  var temporaryValue;
  var randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

var questionnaireCompileStep = {
  on: 'hdQuestionnaire',
  require: [ 'skrDescription' ],
  compile: function (env, config, description) {
    var answers = config.answers;
    var predefinedOrders = config.orders;
    var answerNames = answers.map(function (answer) {
      return answer.name;
    });

    // Unless explicitly disallowed, random orders are allowed.
    var allowRandomOrder = config.allowRandomOrder !== false;

    var questionnaireName = config.name;
    var questionNames = [];

    description.type = 'questionnaire';
    description.label = config.label || config.name;
    description.help = config.help;

    env.hdQuestionnaire = {
      questionnaireDescription: description,
      name: questionnaireName,
      lookupAnswer: function (answerName) {
        return _.find(answers, function (answer) {
          return answer.name === answerName;
        });
      },
      validAnswer: function (answer) {
        return answerNames.indexOf(answer) >= 0;
      },
      validAnswers: answers,
      predefinedOrders: predefinedOrders,
      questionNames: questionNames
    };

    description.addManipulatorBuilder(function (manipulator) {
      var path = manipulator.path;

      // shallow copy the array before shuffling
      var initialOrder = (allowRandomOrder ? shuffle : _.identity)(questionNames.slice());

      var numberOfPredefinedOrders = predefinedOrders ? predefinedOrders.length : 0;
      var firstPredefinedOrder = numberOfPredefinedOrders ?
        predefinedOrders[0] : undefined;

      var hasFixedOrder = (allowRandomOrder ? 1 : 0) + numberOfPredefinedOrders <= 1;

      function applyOrder(on, order) {
        delete on.predefined;
        delete on.questions;
        if (order) {
          on.predefined = order.name;
        } else {
          if (allowRandomOrder) {
            on.questions = initialOrder;
          } else {
            if (firstPredefinedOrder) {
              on.predefined = firstPredefinedOrder.name;
            }
            // if a random order is not allowed, and there is no first predefined order
            // we leave the info empty
          }
        }
        return on;
      }

      // retrieve or initialize selected question order
      // questionsOrderInfo either specifies a predefined order, like so
      //   { predefined: 'Order 3' }
      // or specifies a custom (ramdomized) order, like so
      //   { questions: [ 'Bla?', 'Foo?', ... ] }
      var questionsOrderInfo = manipulator.document.metadataFor('questionsOrder', path)(
        function () {
          return applyOrder({});
        });

      function resolveOrderName(name) {
        return _.find(predefinedOrders, function (predefinedOrder) {
          return predefinedOrder.name === name;
        });
      }

      // if a predefined order is currently used, we need to match it up with
      // one of the predefinedOrders
      // if a randomized order is already present in the document, we need to reuse it
      if (typeof questionsOrderInfo.predefined === 'undefined') {
        manipulator.selectedOrder = null;
        if (questionsOrderInfo.questions) {
          initialOrder = questionsOrderInfo.questions;
        }
      } else {
        manipulator.selectedOrder = resolveOrderName(questionsOrderInfo.predefined);
      }

      manipulator.allowRandomOrder = allowRandomOrder;
      manipulator.hasFixedOrder = hasFixedOrder;
      manipulator.initialOrder = initialOrder;
      manipulator.resolveOrderName = resolveOrderName;

      function updateQuestionsOrder(order) {
        manipulator.selectedOrder = order;
        applyOrder(questionsOrderInfo, order);
      }

      manipulator.questionsOrder = function () {
        return manipulator.selectedOrder ?
          manipulator.selectedOrder.questions :
          manipulator.initialOrder;
      };
      manipulator.updateQuestionsOrder = updateQuestionsOrder;
      manipulator.predefinedOrders = predefinedOrders;
      manipulator.validAnswers = env.hdQuestionnaire.validAnswers;

    });
  }
};

var questionnaireCSVCompileStep = {
  on: 'hdQuestionnaire',
  require: [ 'hdQuestionnaire' ],
  compile: function (node, env, hdQuestionnaire) {
    hdQuestionnaire.questionnaireDescription.buildCSV = function (manipulator, csvBuilder) {
      var key = csvMapping.pathAsString(manipulator.path.concat([ 'order' ]));
      var value;
      if (manipulator.selectedOrder) {
        value = manipulator.selectedOrder.name;
      } else {
        value = manipulator.allowRandomOrder ? 'Random' : '';
      }
      csvBuilder.addColumn(key, value);
    };
  }
};

var questionnaireAutocompleteTrait = autocompleteTrait({
  on: 'hdQuestionnaire',
  autocomplete: function (manipulator, inputData) {
    if (!inputData.csv) { return; }
    var result = csvMapping.csvData(inputData, manipulator, '|order');
    if (typeof result !== 'string') { return; }
    if (result === '') { return; }
    manipulator.updateQuestionsOrder(manipulator.resolveOrderName(result));
  }
});

var questionCompileStep = {
  on: 'hdQuestion',
  require: [ 'skrDescription', 'hdQuestionnaire' ],
  compile: function (env, config, description, hdQuestionnaire) {
    description.type = 'question';
    description.label = config.label || config.name;
    description.help = config.help;
    description.assertions.question = {
      check: function (data) {
        if (description.isAbsent(data)) {
          return 'pending';
        }
        return hdQuestionnaire.validAnswer(data) ? 'ok' : 'failed';
      }
    };
    var questionName = config.name;
    description.questionName = questionName;
    hdQuestionnaire.questionNames.push(questionName);

    // TODO reuse helper function from documentmodel instead of
    // duplicating the code here
    description.addManipulatorBuilder(
      function (manipulator) {
        Object.defineProperty(manipulator, 'value', {
          get: function () { return manipulator.read(); },
          set: function (v) { manipulator.edit(function () { return v; }); }
        });
        manipulator.lookupAnswer = function () {
          return hdQuestionnaire.lookupAnswer(manipulator.value);
        };
    });
  }
};

var questionAutocompleteTrait = autocompleteTrait({
  on: 'hdQuestion',
  autocomplete: function (manipulator, inputData) {
    if (!inputData.csv) { return; }
    var result = csvMapping.csvData(inputData, manipulator);
    if (typeof result !== 'string') { return; }
    if (result === '') { return; }
    manipulator.value = result;
    return true;
  }
});

var questionCSVCompileStep = {
  on: 'hdQuestion',
  require: [ 'skrDescription' ],
  compile: function (env, config, skrDescription) {
    skrDescription.buildCSV = function (manipulator, csvBuilder) {
      var key = csvMapping.pathAsString(manipulator.path);
      csvBuilder.addColumn(key, manipulator.value);
    };
  }
};

var staticimageCompileStep = {
  on: 'hdStaticImage',
  require: [ 'skrDescription' ],
  compile: function (env, config, description) {
    env.skrDescription = description;
    description.imageSrc = config.src;
    description.imageWidth = config.width;
  }
};

module.exports = {
  staticimageCompileStep: staticimageCompileStep,
  questionnaireCompileStep: questionnaireCompileStep,
  questionnaireCSVTrait: documentmodel.traits.compose(
    questionnaireAutocompleteTrait,
    {
      contributes: [
        { name: 'compileSteps', value: questionnaireCSVCompileStep }
      ]
    }),
  questionCompileStep: questionCompileStep,
  questionCSVTrait: documentmodel.traits.compose(
    questionAutocompleteTrait,
    {
      contributes: [
        { name: 'compileSteps', value: questionCSVCompileStep }
      ]
    })
};
