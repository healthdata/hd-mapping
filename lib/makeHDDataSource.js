/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var documentmodel = require('documentmodel');

var workflowProps = require('./workflowProps');

var PATIENT_ID_KEY = '%id';

var PATIENT_ID = 'PATIENT_IDENTIFIER';
var CODED_ID = 'CODED_PATIENT_IDENTIFIER';

module.exports = makeHDDataSource;

function makeHDDataSource(documentData, usesNewSystem) {

  if (!_.isObject(documentData)) {
    throw new Error('makeHDDataSource: documentData is a required parameter');
  }

  // If usesNewSystem is not _explicitly_ provided, we infer it from the
  // document data if possible, or have it default to false. In the future, this
  // default may change.
  if (!_.isBoolean(usesNewSystem)) {
    usesNewSystem = Boolean(documentData.private || documentData.coded);
  }

  // Either the patient ID data is stored in .patientID, or it is stored in
  // either .private or .coded (but not both). To avoid any ambiguity, we verify
  // these rules and stop if they are not satisfied. We also verify consistency
  // with the value of usesNewSystem.

  if (usesNewSystem && documentData.patientID ||
      !usesNewSystem && (documentData.private || documentData.coded)) {
    throw new Error('makeHDDataSource: cannot use both patient ID systems');
  }

  if (usesNewSystem && documentData.private && documentData.coded) {
    throw new Error('makeHDDataSource: cannot use both private and coded data');
  }

  // Load and if necessary initialize all data fields.

  var publicData = documentData.content = documentData.content || {};
  var patientID;
  var hdWorkflowProps;
  var privateData;
  var codedData;

  if (usesNewSystem) {
    codedData = documentData.coded;
    // Only if there is no coded data we initialize the private data, to
    // maintain the invariant that only one is used.
    if (!codedData) {
      documentData.private = documentData.private || {};
    }
    privateData = documentData.private;
  } else {
    patientID = documentData.patientID = documentData.patientID || {};
    hdWorkflowProps = workflowProps(patientID);
  }

  var isPseudonymised = usesNewSystem ?
    Boolean(codedData) :
    hdWorkflowProps.has(CODED_ID);

  var publicDataSource = documentmodel.makeDocumentDataSource(publicData);
  return {
    isPseudonymised: function () { return isPseudonymised; },
    hdWorkflowProps: function () { return hdWorkflowProps; },
    formatToBeCodedData: formatToBeCodedData,
    accessPath: accessPath,
    metadataFor: publicDataSource.metadataFor
  };

  function processDataPathConfig(dataPathConfig) {
    return _.reduce(dataPathConfig, step, {});
    function step(memo, config) {
      if (!_.isEmpty(
        _.intersection(
          _.keys(config),
          _.keys(memo)))) {
        throw new Error('Conflicting data path config');
      }
      return _.extend(memo, config);
    }
  }

  function accessPath(dataPath, dataPathConfig) {
    var processedConfig = processDataPathConfig(dataPathConfig);
    var publicAccess;
    if (!processedConfig.private || processedConfig.keep) {
      publicAccess = publicDataSource.accessPath(dataPath);
    }
    if (processedConfig.private) {
      if (!_.isEqual(_.initial(dataPath), processedConfig.patientID)) {
        throw new Error('Patient ID can only have direct properties');
      }
      var propertyName = _.last(dataPath);
      if (usesNewSystem) {
        if (isPseudonymised) {
          if (processedConfig.coded) {
            return accessCodedPath(processedConfig.patientID, propertyName);
          }
          if (processedConfig.keep) {
            return publicAccess;
          }
          // When the document is pseudonymised, private fields that are neither
          // kept nor coded, are not accessible.
          return {
            get: _.noop,
            set: _.noop
          };
        }
        // Kept private fields are _only_ stored in the public data.
        if (processedConfig.keep) {
          return publicAccess;
        }
        // Private fields that are not kept, are stored in the private data. It
        // is not necessary to distinguish between coded and non-coded private
        // fields.
        return accessPrivatePath(
          processedConfig.patientID,
          propertyName);
      }

      // The following code deals with the case of private fields in the _old_
      // system (using worklfow props).

      if (propertyName === PATIENT_ID_KEY) {
        propertyName = isPseudonymised ? CODED_ID : PATIENT_ID;
      }
      var skipCoding = processedConfig.skipCoding;
      return {
        get: function () {
          // Usually, hdWorkflowProps takes precedence, but not when the
          // parameter is not present, in which case it may be that the
          // document data knows more.
          if (!hdWorkflowProps.has(propertyName) && publicAccess) {
            return publicAccess.get();
          }
          return hdWorkflowProps.get(propertyName, skipCoding);
        },
        set: function (v) {
          hdWorkflowProps.set(propertyName, v, skipCoding);
          if (publicAccess) {
            publicAccess.set(v);
          }
        }
      };
    }
    // If not private, we simply allow accessing the public data.
    return publicAccess;
  }

  function accessPrivatePath(basePath, propertyName) {
    if (!_.all(basePath, _.isString)) {
      throw new Error('Patient ID fields are not allowed in lists');
    }
    basePath = basePath.join('.');
    var data = privateData[basePath] = privateData[basePath] || {};
    return {
      get: function () {
        return data[propertyName];
      },
      set: function (v) {
        data[propertyName] = v;
      }
    };
  }

  function accessCodedPath(basePath, propertyName) {
    basePath = basePath.concat([ propertyName ]).join('.');
    return {
      get: function () {
        var codedValue = codedData[basePath];
        return _.isObject(codedValue) ? codedValue.value : undefined;
      },
      // coded data cannot be modified
      set: _.noop
    };
  }

  /**
   * Helper function to format data such that it can be coded.
   * @param  {array} items An array of path-value tuples
   * @return {object}      An codable key-value object
   */
  function formatToBeCodedData(items) {
    // We only return something when we are using the new system and we are not
    // already dealing with coded data.
    if (!privateData) { return; }
    return _.object(_.map(items, function (item) {
      var path = item.path.join('.');
      var value = item.value;
      return [ path, value ];
    }));
  }
}
