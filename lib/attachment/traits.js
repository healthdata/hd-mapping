/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var csvMapping = require('../mappings/csvMapping');

var documentmodel = require('documentmodel');

var uuid = require('node-uuid');

var editorInputTrait = documentmodel.components.editorInputTrait;

var attachmentTrait = {
  requires: [ 'field' ],
  provides: [ 'attachmentField' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: function (description) {
        description.type = 'attachment';
        description.addManipulatorBuilder(function (manipulator) {
          manipulator.attachmentID = function () {
            var attachmentData = manipulator.read();
            if (!_.isObject(attachmentData)) { return; }
            return attachmentData.uuid;
          };
          manipulator.generateID = function () {
            return uuid.v4();
          };
          manipulator.clearAttachmentID = function () {
            manipulator.edit(function () {});
          };
          manipulator.setAttachmentID = function (id) {
            manipulator.edit(function () {
              if (!id) { return; } // clear
              return { uuid: id };
            });
          };
          manipulator.becomeAttachment = function (name) {
            // If the name corresponds to the current attachment ID, we do not
            // have to do anything.
            if (name === manipulator.attachmentID()) { return; }
            if (!_.isString(name) || _.isEmpty(name)) {
              manipulator.edit(function () {}); // clear
              return;
            }
            var attachmentService = manipulator.document.services.attachment;
            var attachmentID = attachmentService.encounterAttachment(name);
            manipulator.edit(function () {
              if (!attachmentID) { return; } // clear
              return { uuid: attachmentID };
            });
          };
        });
      }
    }
  ]
};

var attachmentCsvTrait = csvMapping.simpleValueCSV({
  on: 'attachment',
  importFromCSV: function (manipulator, value) {
    if (_.isString(value)) {
      manipulator.becomeAttachment(value);
    }
  },
  exportToCSV: function (manipulator) {
    return manipulator.attachmentID();
  }
});

module.exports = {
  attachment: attachmentTrait,
  attachmentCsv: attachmentCsvTrait,
  attachmentInput: editorInputTrait('attachmentInput')
};
