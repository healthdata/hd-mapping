/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = asyncState;

function asyncState(equals, resolve, notify) {

  var currentState;
  var busy = false;
  var resolvedResult;
  var resolvedError;

  return {
    currentState: function () { return currentState; },
    resolvedResult: function () { return resolvedResult; },
    resolvedError: function () { return resolvedError; },
    busy: function () { return busy; },
    withState: withState
  };

  function withState(newState) {
    // if states are equal, all is fine, nothing to do
    if (equals(currentState, newState)) { return; }
    // if states are difference, there is work to do
    currentState = newState;
    resolvedResult = undefined;
    resolvedError = undefined;
    if (_.isUndefined(currentState)) { return; }
    busy = true;
    resolve(currentState, _.partial(resolveCallback, currentState));
  }

  function resolveCallback(originalState, err, result) {
    // if the current state if different from the original state,
    // we are no longer interested in the result
    if (!equals(originalState, currentState)) { return; }
    if (err) {
      resolvedError = err;
    } else {
      resolvedResult = result;
    }
    busy = false;
    notify();
  }
}
