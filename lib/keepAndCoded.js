/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var compileStep = {
  require: [ 'skrDescription' ],
  compile: function (env, node, skrDescription) {

    var isPrivateByDefault = env.skrObject && env.skrObject.privateByDefault;
    var isPrivate = _.has(node, 'keep') || isPrivateByDefault;
    var keep = node.keep === true;
    var coded = node.keep === 'coded';

    skrDescription.addManipulatorBuilder(function (manipulator) {
      manipulator.toBeCoded = function () {
        if (!isPrivate || !coded) { return []; }
        if (!_.isString(manipulator.value)) { return []; }
        return [
          {
            path: manipulator.path,
            value: { type: 'text', value: manipulator.value }
          }
        ];
      };
    });

    skrDescription.pathConfig = function (path) {
      if (!isPrivate) { return; }
      return {
        // the property name 'patientID' is weird here,
        // 'basePath' would be better
        patientID: _.initial(path),
        private: true,
        keep: keep,
        coded: coded
      };
    };
  }
};

var codedTextTrait = {
  requires: [ 'field' ],
  provides: [ 'toBeCoded' ],
  behaviour: {
    toBeCoded: function (manipulator) {
      return manipulator.toBeCoded();
    }
  },
  contributes: [
    {
      name: 'compileSteps',
      value: compileStep
    }
  ]
};

module.exports = codedTextTrait;
