/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = pipeline;

function pipeline(stepDefinitions) {

  _.each(stepDefinitions, function (stepDefinition) {
    var requiredProps = [ 'input', 'output', 'run' ];
    _.each(requiredProps, function (requiredProp) {
      if (!_.has(stepDefinition, requiredProp)) {
        throw new Error('pipeline: invalid step definition ' +
          JSON.stringify(stepDefinition, null, 2));
      }
    });
  });

  return compile;

  function compile(spec) {
    if (!_.isArray(spec)) { spec = [ spec ]; }
    return _.reduce(_.map(spec, compileSingle), composeSteps);
  }

  function compileSingle(spec) {
    var stepDefinition = _.find(stepDefinitions, _.partial(match, spec));
    if (!stepDefinition) {
      throw new Error('pipeline: could not resolve step specification ' +
        JSON.stringify(spec, null, 2));
    }
    return {
      input: stepDefinition.input,
      output: stepDefinition.output,
      run: _.partial(stepDefinition.run, spec)
    };
  }
}

function match(spec, stepDefinition) {
  if (_.has(stepDefinition, 'keys')) {
    return _.all(stepDefinition.keys, _.partial(_.has, spec));
  }
  if (_.has(stepDefinition, 'properties')) {
    return _.all(stepDefinition.properties, function (value, key) {
      return spec[key] === value;
    });
  }
  return false;
}

function composeSteps(step1, step2) {
  if (!_.isEqual(step1.output, step2.input)) {
    throw new Error('pipeline: non-matching steps');
  }
  return {
    input: step1.input,
    output: step2.output,
    run: _.compose(step2.run, step1.run)
  };
}
