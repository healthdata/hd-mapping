/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var pipeline = require('./pipeline');

module.exports = cddm;

/**
 * Configuration-Driven Data Mapping (cddm)
 * @param  elementary    The native steps available in pipelines
 * @param  pipelineSpecs JSON pipeline specifications
 * @return               A cddm instance to construct and run data mappers
 */
function cddm(elementary, pipelineSpecs) {
  var compilePipeline = pipeline(elementary);
  var compiledPipelines = _.mapObject(pipelineSpecs, compilePipeline);
  var self = {

    noop: function () {
      return {
        simplify: function (contextualAll) {
          return contextualAll ? [] : this;
        },
        toJSON: function () {
          return { noop: null };
        },
        run: function () {
          return [];
        }
      };
    },

    all: function (ms) {
      return {
        simplify: function (contextualAll) {
          var newMs = _.flatten(_.map(ms, function (m) {
            return m.simplify(true);
          }));
          return contextualAll ? newMs : self.all(newMs);
        },
        toJSON: function () {
          return {
            all: _.map(ms, function (m) { return m.toJSON(); })
          };
        },
        run: function (inputData) {
          return _.flatten(_.map(ms, function (m) {
            return m.run(inputData);
          }));
        }
      };
    },

    system: function (pipelineName, info) {
      if (!_.has(pipelineSpecs, pipelineName)) {
        throw new Error('cddm: unknown pipeline ' + pipelineName);
      }
      var pipelineSpec = pipelineSpecs[pipelineName];
      var compiledPipeline = compiledPipelines[pipelineName];

      return {
        simplify: function () {
          return this;
        },
        toJSON: function () {
          return {
            system: pipelineName,
            pipeline: pipelineSpec,
            info: info
          };
        },
        run: function (inputData) {
          return [
            {
              info: info,
              result: compiledPipeline.run(inputData)
            }
          ];
        }
      };
    },

    run: function (dataMapper, inputData) {
      return dataMapper.run(inputData);
    }
  };
  return self;
}
