/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = {
  generateHandlebarsTemplate: generateHandlebarsTemplate,
  makeHbsHelpers: makeHbsHelpers,
  handlebarsTrait: handlebarsTrait
};

function generateHandlebarsTemplate(component, hbsHelpers, language) {
  language = language || 'en';
  hbsHelpers = hbsHelpers || makeHbsHelpers(language);
  // Sanity check: if the component corresponds to a field, the description
  // parameter should match with the component.
  // This check can be removed once components always have a link to the relevant
  // description.
  if (!component) {
    throw new Error('generateHandlebarsTemplate: missing argument "component"');
  }
  // Invoke the content type specific logic for generating the
  // handlebars template. If the content type does not provide
  // this functionality, we return an empty string as result.
  var genHbs = component.contentType.generateHandlebarsTemplate;
  if (!genHbs) {
    // The warning is useful for development and debugging, but can be removed later.
    console.warn('No generateHandlebarsTemplate implemented for', component.componentName);
    return '';
  }
  var result = genHbs(component, hbsHelpers, language);
  return result;
}

function makeHbsHelpers(language, context) {
  context = context || {};
  language = language || 'en';
  var self = {
    withContext: function (extraContext) {
      var newContext = _.extend({}, context, extraContext);
      return makeHbsHelpers(language, newContext);
    },
    sequence: function (/* var_args */) {
      return _.toArray(arguments).join('\n');
    },
    comment: function (/* var_args */) {
      return '<!---- ' + _.toArray(arguments).join('\n') + ' ---->';
    },
    tag: function (tagName, attributes, content, displayAsGroup) {
      // attributes are optional, so we need to reshuffle arguments
      if (!content) {
        content = attributes;
        attributes = undefined;
      }
      var attrsResult = _.map(attributes, function (value, name) {
        return name + '="' + value + '"';
      }).join(' ');
      // add space before attributes, except when empty
      if (attrsResult) { attrsResult = ' ' + attrsResult; }

      // Add newline if you want to display tag as row
      if (displayAsGroup) {
        content = '\n' + content + '\n';
      }

      // Add space at back to beautify code
      return '<' + tagName + attrsResult + '>' + content + '</' + tagName + '>';
    },
    nested: function (component) {
      return generateHandlebarsTemplate(component, self);
    },
    currentPath: function () {
      return context.path || 'document';
    },
    parentPaths: function () {
      return context.parentPaths || [];
    },
    withBasePath: function (basePath) {
      // We reset the parent paths because they do not always make sense when
      // using an arbitrary new base path.
      return self.withContext({
        parentPaths: [],
        path: basePath
      });
    },
    withPath: function (fieldName) {
      var currentPath = self.currentPath();
      return self.withContext({
        parentPaths: self.parentPaths().concat([ currentPath ]),
        path: currentPath + '.nested.[' + fieldName + ']'
      });
    },
    withParentPath: function () {
      var parentPaths = self.parentPaths();
      if (parentPaths.length < 1) {
        throw new Error('withParentPath: no parent paths available');
      }
      var parentPath = _.last(parentPaths);
      var remainingParentPaths = _.initial(parentPaths);
      return self.withContext({
        parentPaths: remainingParentPaths,
        path: parentPath
      });
    },
    fieldValue: function () {
      return '{{ ' + self.currentPath() + '.value' + ' }}';
    },
    fieldLabelValue: function () {
      return '{{ ' + self.currentPath() + '.labels.' + language  + ' }}';
    },
    fieldLabel: function () {
      return '{{ ' + self.currentPath() + '.fieldLabel.' + language  + ' }}';
    },
    multiOptions: function (name) {
      return '{{ inArray ' + self.currentPath() + '.values \'' + name + '\' }}';
    },
    selectedOption: function () {
      return '{{ ' + self.currentPath() + '.selectedOptionLabel.'  + language + ' }}';
    },
    answer: function () {
      return '{{ ' + self.currentPath() + '.answerLabel.'  + language + ' }}';
    },
    dateRendered: function () {
      return '{{ ' + self.currentPath() + '.rendered' + ' }}';
    },
    forEachElement: function (content, property) {
      return '{{#each ' + self.currentPath() + '.' + property + '}}\n' + content + '\n{{/each}}';
    },
    group: function (group, nestedDescriptions) {
      nestedDescriptions = nestedDescriptions || group.description.nestedDescriptions;
      var nestedFieldNames = _.keys(nestedDescriptions);
      return _.map(group.components, nested).join('\n');
      function nested(nestedComponent) {
        // For objects we need to track the property names of the nested fields
        var fieldName = lookupFieldName(nestedComponent);
        if (!fieldName) {
          return self.nested(nestedComponent);
        }

        // Using withPath() we extend the path in the context with the field name.
        return self
          .withPath(fieldName)
          .nested(nestedComponent);
      }
      // Helper function to determine the field name of a nested field component.
      // We iterater through the fieldnames and determine a match when the corresponding
      // descriptions match.
      function lookupFieldName(field) {
        return _.find(nestedFieldNames, function (fieldName) {
          return field.description === nestedDescriptions[fieldName];
        });
      }
    }
  };
  return self;
}

function handlebarsTrait(f) {
  return {
    provides: [ 'handlebars' ],
    behaviour: {
      generateHandlebarsTemplate: f
    }
  };
}
