/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var handlebars = require('./core');
var _ = require('underscore');

var handlebarsTrait = handlebars.handlebarsTrait;

module.exports = {
  section: handlebarsTrait(sectionHbs),
  object: handlebarsTrait(objectHbs),
  number: handlebarsTrait(numberHbs),
  text: handlebarsTrait(textHbs),
  choice: handlebarsTrait(choiceHbs),
  multiline: handlebarsTrait(multilineHbs),
  date: handlebarsTrait(dateHbs),
  fieldset: handlebarsTrait(fieldsetHbs),
  multichoice: handlebarsTrait(multichoiceHbs),
  boolean: handlebarsTrait(booleanHbs),
  referencelist: handlebarsTrait(referencelistHbs),
  list: handlebarsTrait(listHbs),
  patientId: handlebarsTrait(patientIdHbs),
  questionnaire: handlebarsTrait(questionnaireHbs),
  question: handlebarsTrait(questionHbs)
};

function sectionHbs(section, hbs) {
  var depth = section.depth + 1; // depth starts counting from zero instead of one
  var sectionNumber = section.sectionNumber.join('.');
  var styleSection = (section.isTopLevel ? 'pdfTableTopLevel' : 'pdfTableSubLevel');
  var styleTitle = (section.isTopLevel ? ' pdfTopLevel' : '');

  return hbs.sequence(
    hbs.comment('Start section ' + sectionNumber + ' - ' + section.title),
    hbs.tag('div', { class: styleSection },
      hbs.sequence(
        hbs.tag('div', { class: 'pdf-table-head pdf-lvl-' + depth + styleTitle },
          sectionNumber + ' - ' + section.title),
        hbs.tag('div', { class: 'section-content' },
          hbs.group(section), true)),
      true),
    hbs.comment('End section ' + sectionNumber)
    );
}

function objectHbs(object, hbs) {
  return hbs.group(object);
}

function numberHbs(numberInput, hbs) {
  return hbs.sequence(
    hbs.comment('Number field: ' + numberInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldValue())
        ), true
      ), true
    )
  );
}

function referencelistHbs(referenceInput, hbs) {
  return hbs.sequence(
    hbs.comment('Referencelist field: ' + referenceInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldLabelValue())
        ), true
      ), true
    )
  );
}

function booleanHbs(booleanInput, hbs) {
  return hbs.sequence(
    hbs.comment('Boolean field: ' + booleanInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldValue())
        ), true
      ), true
    )
  );
}

function multilineHbs(multilineInput, hbs) {
  return hbs.sequence(
    hbs.comment('Multiline field: ' + multilineInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldValue())
        ), true
      ), true
    )
  );
}

function textHbs(textInput, hbs) {
  return hbs.sequence(
    hbs.comment('Text field: ' + textInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldValue())
        ), true
      ), true
    )
  );
}

function dateHbs(dateInput, hbs) {
  return hbs.sequence(
    hbs.comment('Date field: ' + dateInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.dateRendered())
        ), true
      ), true
    )
  );
}

function choiceHbs(choiceInput, hbs) {
  var choiceDescr = choiceInput.description;

  return hbs.sequence(
    hbs.comment('Choice field: ' + choiceInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.sequence(
        hbs.tag('div', { class: 'pdf-row' },
          hbs.sequence(
            hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
            hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.selectedOption())
          ), true
        )
      ), true
    ),
    hbs.comment('Start Nested fields under choice'),
    hbs.tag('div', { class: 'pdf-fieldset' },
      _.map(choiceInput.options, function (option) {
        // The descriptions for the nested fields are not accessible via the
        // option components, but via the choice field description. Therefore,
        // we manually provide them here.
        var nestedDescriptions = choiceDescr.optionProperties[option.name];
        return hbs.group(option.nestedComponent, nestedDescriptions);
      }).join('\n'), true),
    hbs.comment('End nesting under choice')
  );
}

function fieldsetHbs(fieldsetInput, hbs) {
  return hbs.sequence(
    hbs.comment('Start Fieldset: ' + fieldsetInput.label),
    hbs.tag('div', { class: 'pdf-fieldset' },
      hbs.sequence(
        hbs.tag('div', { class: 'pdf-fieldset-head' }, hbs.fieldLabel()),
        hbs.tag('div', { class: 'pdf-fieldset-content' }, hbs.group(fieldsetInput), true)
      ), true
    ),
    hbs.comment('End Fieldset')
  );
}

function multichoiceHbs(multichoiceInput, hbs) {
  var items = _.map(multichoiceInput.options, function (option) {
    return hbs.tag('li', { class: hbs.multiOptions(option.name) }, option.label);
  }).join('\n');

  return hbs.sequence(
    hbs.comment('Choice field: ' + multichoiceInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' },
            hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value pre-wrap-off' },
            hbs.tag('ul', {}, items, true), true)
        ), true
      ), true
    )
  );
}

function listHbs(listInput, hbs) {
  return hbs.sequence(
    hbs.comment('Start List: ' + listInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.sequence(
        hbs.tag('div', { class: 'pdf-row' },
          hbs.sequence(
            hbs.tag('div', { class: 'pdf-column pdf-column-label listlabel' }, hbs.fieldLabel()),
            hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldValue())
          ), true
        ),
        hbs.forEachElement(
          hbs.tag('div', { class: 'pdf-row' },
            hbs.sequence(
              hbs.tag('div', { class: 'pdf-column pdf-column-label listlabelPad pre-wrap-off' },
                hbs.fieldLabel() + ' nr. ' + '{{math @index "+" 1}}'),
              hbs.tag('div', { class: 'pdf-column pdf-column-value pdf-table-list' },
                hbs
                  .withBasePath('this')
                  .group(listInput.listElementComponent), true)
            ), true
          ),
          'elements'
        )
      ), true
    ),
    hbs.comment('End List')
    );
}

function patientIdHbs(patientIdInput, hbs) {
  return hbs.sequence(
    hbs.comment('Patient Id field: ' + patientIdInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.sequence(
        hbs.tag('div', { class: 'pdf-row' },
          hbs.sequence(
            hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
            hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.fieldValue())
          ), true
        )
      ), true
    ),
    hbs.tag('div', { class: 'pdf-fieldset' }, hbs.group(patientIdInput), true)
  );
}

function questionnaireHbs(questionnaireInput, hbs) {
  // The question descriptions are not nested under the the questionnaire
  // description, so we need to reconstruct the required information. We combine
  // the question names from the questionnaire with the descriptions found on
  // the question components themselves.
  var questionNames = questionnaireInput.hdQuestionnaire.questionNames;
  var questionDescriptions = _.pluck(questionnaireInput.components, 'description');
  var nestedDescriptions = _.object(questionNames, questionDescriptions);
  return hbs.sequence(
    hbs.comment('Questionnaire field: ' + questionnaireInput.label),
    hbs.tag('div', { class: 'pdf-fieldset' },
      hbs.sequence(
        hbs.tag('div', { class: 'pdf-fieldset-head' }, hbs.fieldLabel()),
        hbs.tag('div', { class: 'pdf-fieldset-content' },
          hbs
            .withParentPath() // We need to go up one level because questions
                              // are not nested under questionnaires.
            .group(questionnaireInput, nestedDescriptions),
          true)
      )
    )
  );
}

function questionHbs(questionInput, hbs) {
  return hbs.sequence(
    hbs.comment('Question field: ' + questionInput.label),
    hbs.tag('div', { class: 'pdf-field' },
      hbs.tag('div', { class: 'pdf-row' },
        hbs.sequence(
          hbs.tag('div', { class: 'pdf-column pdf-column-label' }, hbs.fieldLabel()),
          hbs.tag('div', { class: 'pdf-column pdf-column-value' }, hbs.answer())
        ), true
      ), true
    )
  );
}
