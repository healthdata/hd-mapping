/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var request = require('request');

module.exports = jsonRequest;

function jsonRequest(uri, cb) {
  request.get(uri, function (err, response, body) {
    if (err) { return cb(err); }
    if (response.statusCode !== 200) {
      return cb(new Error('Request to ' + uri + ' returned ' +
        response.statusCode + ' and ' + body));
    }
    var result;
    try {
      result = JSON.parse(body);
    } catch (e) {
      return cb(new Error('Request to ' + uri + ' returned invalid JSON: ' + body));
    }
    return cb(undefined, result);
  });
}
