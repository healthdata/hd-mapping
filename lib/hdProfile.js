/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var documentmodel = require('documentmodel');

var compileTrait = documentmodel.parseDocumentDefinition.compileTrait;

var cbb = require('./mappings/cbb');

var keepAndCoded = require('./keepAndCoded');

var typeTraits = _.extend({}, documentmodel.typeTraits, require('./typeTraits'));
var modalityTraits = require('./modalityTraits').concat([
  documentmodel.modalityTraits.computedExpressionsTrait,
  documentmodel.modalityTraits.computedWithTrait,
  documentmodel.modalityTraits.conditionsTrait,
  documentmodel.modalityTraits.defaultTrait,
  documentmodel.modalityTraits.onlyWhenTrait,
  documentmodel.modalityTraits.readOnlyTrait,
  documentmodel.modalityTraits.requiredTrait,
  documentmodel.modalityTraits.propertyNameTrait,
  documentmodel.modalityTraits.labelTrait,
  documentmodel.modalityTraits.helpTrait,
  documentmodel.modalityTraits.stickyTrait,
  compileTrait,
  documentmodel.components.traits.propertyNameTrait,
  cbb.cbbTrait
]);

var attachment = require('./attachment/traits');

var makeDescriptionTrait = documentmodel.parseDocumentDefinition.makeDescriptionTrait;

var csvTraits = documentmodel.csvMapping.traits;

var components = documentmodel.components;

var hdComponents = require('./components');

var patientID = require('./patientID/directives');
var referenceList = require('./referenceList/directives');
var otherTraits = require('./directives');

var handlebars = require('./handlebars/traits');

var typeCompositions = {
  staticimage: [
    makeDescriptionTrait,
    typeTraits.staticimageTrait,
    hdComponents.hdStaticImageInput
  ],
  number: [
    makeDescriptionTrait,
    typeTraits.numberTrait,
    csvTraits.skrNumber,
    keepAndCoded,
    components.traits.skrNumberInput,
    handlebars.number,
    cbb.traits.number
  ],
  object: [
    makeDescriptionTrait,
    typeTraits.objectTrait,
    components.groups.skrObject,
    components.traits.skrObject,
    handlebars.object
  ],
  text: [
    makeDescriptionTrait,
    typeTraits.textTrait,
    keepAndCoded,
    csvTraits.skrText,
    components.traits.skrTextInput,
    handlebars.text,
    cbb.traits.text
  ],
  choice: [
    makeDescriptionTrait,
    typeTraits.choiceTrait,
    csvTraits.skrChoice,
    keepAndCoded,
    components.traits.skrChoiceInput,
    handlebars.choice,
    cbb.traits.choice
  ],
  multiline: [
    makeDescriptionTrait,
    typeTraits.textTrait,
    csvTraits.skrText,
    components.traits.skrMultilineInput,
    handlebars.multiline
  ],
  boolean: [
    makeDescriptionTrait,
    typeTraits.booleanTrait,
    csvTraits.skrBoolean,
    keepAndCoded,
    components.traits.skrBooleanInput,
    handlebars.boolean,
    cbb.traits.boolean
  ],
  date: [
    makeDescriptionTrait,
    typeTraits.dateTrait,
    csvTraits.skrDate,
    keepAndCoded,
    components.traits.skrDateInput,
    handlebars.date,
    cbb.traits.date
  ],
  referencelist: [
    makeDescriptionTrait,
    typeTraits.referencelistTrait,
    keepAndCoded,
    referenceList.referencelistCSVTrait,
    hdComponents.hdReferenceListInput,
    handlebars.referencelist,
    cbb.traits.referencelist
  ],
  patientID: [
    makeDescriptionTrait,
    typeTraits.patientIDTrait,
    patientID.hdPatientIDCSVTrait,
    hdComponents.hdPatientIDInput,
    handlebars.patientId,
    cbb.traits.text
  ],
  multichoice: [
    makeDescriptionTrait,
    typeTraits.multichoiceTrait,
    csvTraits.skrMultiChoice,
    keepAndCoded,
    components.traits.skrMultiChoiceInput,
    handlebars.multichoice,
    cbb.traits.multichoice
  ],
  questionnaire: [
    makeDescriptionTrait,
    typeTraits.questionnaireTrait,
    otherTraits.questionnaireCSVTrait,
    hdComponents.hdQuestionnaire,
    handlebars.questionnaire
  ],
  fieldset: [
    makeDescriptionTrait,
    typeTraits.fieldsetTrait,
    components.groups.skrFieldSet,
    handlebars.fieldset
  ],
  list: [
    makeDescriptionTrait,
    typeTraits.listTrait,
    csvTraits.skrList,
    components.traits.skrListInput,
    handlebars.list,
    cbb.traits.list
  ],
  section: [
    typeTraits.sectionTrait,
    components.groups.skrSection,
    handlebars.section
  ],
  question: [
    makeDescriptionTrait,
    typeTraits.questionTrait,
    otherTraits.questionCSVTrait,
    hdComponents.hdQuestionInput,
    handlebars.question
  ],
  attachment: [
    makeDescriptionTrait,
    attachment.attachment,
    attachment.attachmentCsv,
    attachment.attachmentInput
  ],
  option: [
    typeTraits.optionTrait,
    components.groups.skrOptionInput,
    components.traits.skrOptionInput
  ]
};

var contexts = {
  section: {
    defaultType: 'section'
  },
  option: {
    defaultType: 'option'
  },
  question: {
    defaultType: 'question',
    types: {
      question: 'question',
      // For some reason, this type annotation has been used sometimes.
      questionnaire: 'question'
    }
  },
  field: {
    defaultType: 'object',
    types: {
      attachment: 'attachment',
      staticimage: 'staticimage',
      number: 'number',
      object: 'object',
      section: 'section',
      text: 'text',
      referencelist: 'referencelist',
      choice: 'choice',
      multiline: 'multiline',
      boolean: 'boolean',
      date: 'date',
      patientID: 'patientID',
      multichoice: 'multichoice',
      questionnaire: 'questionnaire',
      fieldset: 'fieldset',
      list: 'list'
    },
    overrides: {
      referencelist: 'referencelist'
    }
  }
};

module.exports = documentmodel.makeProfile({
  defaultContext: 'field',
  contexts: contexts,
  modalities: modalityTraits,
  types: typeCompositions
});
