/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment-timezone');

module.exports = {
  migrateCommentsToNotes: migrateCommentsToNotes,
  documentContentWithoutNotes: documentContentWithoutNotes
};

function migrateCommentsToNotes(document) {
  var notes = [];
  _.each(document.registeredManipulators, function (manipulator) {
    _.each(manipulator.comments, function (comment) {
      notes.push(migrateCommentToNote(comment, manipulator));
    });
  });
  return notes;
}

function migrateCommentToNote(comment, manipulator) {
  var fieldPath = manipulator.path.join('.');
  var createdOn = convertDate(comment.created);
  var firstComment;
  if (!_.isEmpty(comment.message)) {
    firstComment = {
      content: comment.message,
      username: comment.username,
      platform: comment.platform,
      createdOn: createdOn,
      updatedOn: createdOn
    };
  }
  var resolved = _.isBoolean(comment.solved) ?
    comment.solved :
    comment.solved === 'reopen';
  var note = {
    fieldPath: _.isEmpty(fieldPath) ? undefined : fieldPath,
    title: comment.title,
    resolved: resolved,
    createdOn: createdOn,
    updatedOn: createdOn,
    comments: (firstComment ? [ firstComment ] : []).concat(
      _.map(comment.replies, migrateReplyToComment))
  };
  return note;
}

function migrateReplyToComment(reply) {
  var createdOn = convertDate(reply.created);
  return {
    content: reply.message,
    username: reply.username,
    platform: reply.platform,
    createdOn: createdOn,
    updatedOn: createdOn
  };
}

/**
 * Helper function to convert the timestamps used in old comments to ISO8601
 * format. Because timestamps do not contain timezone information, we assume
 * Europe/Brussels timezone.
 */
function convertDate(dateString) {
  if (_.isEmpty(dateString)) {
    return dateString;
  }
  var m = moment.tz(
    dateString,
    'DD/MM/YYYY hh:mm',
    'Europe/Brussels');
  if (!m.isValid()) { return; }
  return m.format();
}

function documentContentWithoutNotes(document) {
  var documentContent = _.extend({}, document.documentData.content);
  delete documentContent.comments;
  return documentContent;
}
