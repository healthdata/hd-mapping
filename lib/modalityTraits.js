/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var patientID = require('./patientID/directives');

// function addInputSize(resultBuilder) {
//   resultBuilder
//     .acceptEditorInput('inputsize');
// }

// function addStable(resultBuilder) {
//   resultBuilder
//     .accept('stable', 'hdStable');
// }

// function addSticky(resultBuilder) {
//   resultBuilder
//     .accept('sticky', 'hdSticky');
// }

// function addKeep(resultBuilder) {
//   resultBuilder
//     .accept('keep', 'hdKeep');
// }

// var modalities = [
//   { requires: [ 'inputsize' ], fold: addInputSize },
//   { requires: [ 'field' ], fold: addSticky }
// ];

var modalityTraits = [];

modalityTraits.push({
  requires: [ 'field' ],
  provides: [ 'stable' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: function (skrDescription, config) {
        if (config.stable) {
          skrDescription.isStable = true;
        }
      }
    }
  ]
});

modalityTraits.push({
  requires: [ 'field' ],
  provides: [ 'keep' ],
  contributes: [
    {
      name: 'compileSteps',
      value: patientID.hdKeepCompileStep
    }
  ]
});

module.exports = modalityTraits;
