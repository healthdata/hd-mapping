/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var patientID = require('./patientID/directives');
var normalizeNestedPatientIDFields = require('./patientID/normalizeNestedFields');
var referenceList = require('./referenceList/directives');

var staticimageTrait = {
  requires: [ 'field' ],
  provides: [ 'staticimageField' ],
  contributes: [
    {
      name: 'compileSteps',
      value: require('./directives').staticimageCompileStep
    }
  ]
};

var referencelistTrait = {
  requires: [ 'field' ],
  provides: [ 'referencelistField' ],
  contributes: [
    {
      name: 'compileSteps',
      value: referenceList.referencelistCompileStep
    }
  ]
};

var questionnaireTrait = {
  requires: [ 'field' ],
  provides: [ 'obtainNested', 'questionnaireField' ],
  behaviour: {
    obtainNested: function (input) {
      return {
        questions: {
          context: 'question',
          nested: input.questions
        }
      };
    }
  },
  contributes: [
    {
      name: 'compileSteps',
      value: require('./directives').questionnaireCompileStep
    }
  ]
};

var questionTrait = {
  requires: [ 'field' ],
  provides: [ 'questionField' ],
  contributes: [
    {
      name: 'compileSteps',
      value: require('./directives').questionCompileStep
    }
  ]
};

var patientIDTrait = {
  requires: [ 'field' ],
  provides: [ 'obtainNested', 'patientIDField', 'toBeCoded' ],
  behaviour: {
    toBeCoded: function (manipulator) {
      return manipulator.toBeCoded();
    },
    obtainNested: function (input) {
      var patientIDFields = normalizeNestedPatientIDFields(input.fields);
      return {
        patientIDFields: {
          context: 'field',
          nested: patientIDFields
        }
      };
    }
  },
  contributes: [
    {
      name: 'compileSteps',
      value: patientID.hdPatientIDCompileStep
    }
  ]
};

module.exports = {
  staticimageTrait: staticimageTrait,
  referencelistTrait: referencelistTrait,
  questionnaireTrait: questionnaireTrait,
  questionTrait: questionTrait,
  patientIDTrait: patientIDTrait
};
