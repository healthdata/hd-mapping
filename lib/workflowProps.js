/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

function encode(value, skipCoding) {
  var isString = typeof value === 'string';
  if (skipCoding) {
    if (isString) {
      return value;
    }
    throw new Error('Cannot skip coding for non-string values');
  }
  return JSON.stringify(value);
}

function decode(value, skipCoding) {
  if (skipCoding) { return value; }
  try {
    return JSON.parse(value);
  } catch (err) {
    var error = new Error('Invalid metadata value: ', value);
    error.badRequest = true;
    throw error;
  }
}

function has(obj, key) {
  return Object.hasOwnProperty.call(obj, key);
}

module.exports = function (encodedData) {
  var decodedData = {};
  return {
    has: function (key) {
      return has(encodedData, key);
    },
    get: function (key, skipCoding) {
      if (!has(decodedData, key)) {
        if (!has(encodedData, key)) {
          return undefined;
        }
        decodedData[key] =  decode(encodedData[key], skipCoding);
      }
      return decodedData[key];
    },
    set: function (key, value, skipCoding) {
      if (typeof value === 'undefined') {
        delete decodedData[key];
        delete encodedData[key];
      } else {
        decodedData[key] = value;
        encodedData[key] = encode(value, skipCoding);
      }
    }
  };
};
