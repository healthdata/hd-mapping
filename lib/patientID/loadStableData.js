/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var documentmodelHelpers = require('../documentmodelHelpers');
var csvMapping = require('../mappings/csvMapping');
var mergeMapping = require('../mappings/mergeMapping');

module.exports = loadStableData;

function loadStableData(manipulator, requestIt) {

  var state = {};

  return {
    isDone: isDone,
    stableDataHasBeenApplied: stableDataHasBeenApplied,
    loadStableDataOnChanged: loadStableDataOnChanged
  };

  function isDone() {
    if (isPending()) { return false; }
    var patientID = manipulator.value;
    if (_.isEmpty(patientID)) { return true; }

    // the stable data has not been requested
    if (state.previouslyRequested !== patientID) { return true; }

    return stableDataHasBeenApplied() ||
      stableDataCouldNotBeLoaded();
  }

  function isPending() {
    return !_.isEmpty(state.currentlyRequesting);
  }

  function stableDataHasBeenApplied() {
    return Boolean(state.stableDataHasBeenApplied);
  }

  function stableDataCouldNotBeLoaded() {
    return Boolean(state.hasError);
  }

  function loadStableDataOnChanged() {
    var patientID = manipulator.value;
    if (!_.isString(patientID)) { return; }
    if (patientID === '') { return; }
    if (state.currentlyRequesting === patientID) { return; }
    if (state.previouslyRequested === patientID) { return; }
    state.stableDataHasBeenApplied = false;
    state.hasError = false;
    state.currentlyRequesting = patientID;
    state.previouslyRequested = undefined;
    requestIt(patientID, function (err, result) {
      // check whether this call is still relevant
      if (state.currentlyRequesting !== patientID) { return; }
      state.previouslyRequested = patientID;
      state.currentlyRequesting = undefined;
      if (err || !_.isObject(result) ||
        !_.isString(result.csv) || _.isEmpty(result.csv)) {
        return done(err || { /* something went wrong */ });
      }

      var description = manipulator.document.root.description;
      var context = manipulator.document.context;

      var csvDocument = documentmodelHelpers.openDocument(
        description,
        { context: context }, // no intial content
        {
          readOnly: false,
          loadStableData: null, // We do not want to enrich the patient ID now,
          enrichPatientID: null // becuase we are already doing that.
        });

      csvMapping.mapCsvToDocument(csvDocument, result.csv, function (err, stableDoc) {
        if (err) {
          return done(err);
        }
        // we merge the current document with the stable data document, giving
        // precendence to the current document
        manipulator.document.inExtendedEditContext('stableData', function () {
          mergeMapping.merge(manipulator.document, stableDoc);
          done();
        });
      });
    });
    function done(err) {
      state.hasError = Boolean(err);
      state.stableDataHasBeenApplied = !state.hasError;
      manipulator.invalidate();
    }
  }
}
