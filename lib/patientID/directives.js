/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var moment = require('moment');
var documentmodel = require('documentmodel');
var autocompleteTrait = documentmodel.autocomplete.autocompleteTrait;
var _ = require('underscore');
var codepat = require('./codepat');
var ssin = require('./ssin');
var csvMapping = require('../mappings/csvMapping');
var loadSSINEnrichment = require('./loadSSINEnrichment');
var loadStableData = require('./loadStableData');

/**
 * The constant METHOD defines the property name used in the document data
 * to mark which method for patient identification is used. The possible
 * methods include:
 *   - ssin
 *   - codepat
 *   - other
 **/
var METHOD = '%method';

/**
 * The constance GENERATED defines the property name used in the document data
 * to indicate whether the patient ID was automatically generated or not.
 **/
var GENERATED = '%generated';

var PATIENT_ID = '%id';

function initializePatientID(manipulator) {
  var initial = {};
  initial[METHOD] = 'other';
  initial[GENERATED] = false;
  manipulator.initialize(initial, true);
}

function clearManipulators(manipulators) {
  _.each(manipulators, function (manipulator) {
    manipulator.clear();
  });
}

function buildPatientIDmanipulator(manipulator, allowCodepat, supportCodepat) {
  var description = manipulator.description;

  initializePatientID(manipulator);
  manipulator.countAsField = false;
  manipulator.clearSpecific = function () {
    clearManipulators(manipulator.propertyManipulators);
    manipulator.value = '';
  };

  var isPseudonymised =
    manipulator.patientIDisCoded =         // old name
    manipulator.isPseudonymised =          // new name
    manipulator.options.isPseudonymised;

  initializePropertyManipulators();

  var accessID = manipulator.document.compilePath(
    manipulator.path.concat([ PATIENT_ID ]),
    [
      { patientID: manipulator.path,
        private: true,
        keep: false,
        coded: true,
        skipCoding: true
      }
    ]);

  Object.defineProperty(manipulator, 'value', {
    get: function () { return accessID.get(); },
    set: function (v) { setPatientID(v); }
  });

  // This call is necessary to setup the correct mapping from the patient ID
  // property in the workflow props. It invokes .get(..., true), fixing the
  // second parameter for all subsequent calls.
  _.identity(manipulator.value);

  manipulator.patientIDisGenerated = patientIDisGenerated;
  manipulator.getMethod = getMethod;
  manipulator.isValidSSIN = ssin.isValidSSIN;
  manipulator.isValidSSINbis = ssin.isValidSSINbis;
  manipulator.extractDateOfBirth = ssin.extractDateOfBirth;

  manipulator.deriveDateOfBirthFromSSIN = function () {
    var birthDateManipulator = manipulator.propertyManipulators.date_of_birth;
    var sexManipulator = manipulator.propertyManipulators.sex;
    if (birthDateManipulator.value) { return; }
    if (manipulator.getMethod() !== 'SSIN') { return; }
    sexManipulator.selectedOption = ssin.extractSex(manipulator.value);
    var date = ssin.extractDateOfBirth(manipulator.value);
    if (!date) { return; }
    birthDateManipulator.value = date;
  };

  if (allowCodepat) {
    setupCodepatComputation();
  }

  manipulator.enrichSSIN = function (data) {
    manipulator.document.inExtendedEditContext('patientID', function () {
      enrichSSIN(data);
    });
  };

  manipulator.enrichmentState = {};
  if (manipulator.options.enrichPatientID) {
    manipulator.enricher = loadSSINEnrichment(manipulator,
      manipulator.options.enrichPatientID);
  }

  if (manipulator.options.loadStableData) {
    manipulator.stableDataLoader = loadStableData(manipulator,
      manipulator.options.loadStableData);
  }

  manipulator.toBeCoded = function () {
    var result = [];
    var id = manipulator.value;
    if (_.isString(id)) {
      result.push({
        path: manipulator.path.concat([ '%id' ]),
        value: { type: 'patientID', value: id }
      });
    }
    return result;
  };

  // the rest of the function body is helper functions

  function getMethod() {
    return manipulator.read()[METHOD];
  }

  function setPatientID(v, isGenerated) {
    if (isPseudonymised) { return; }
    manipulator.edit(function (obj) {
      accessID.set(v);
      var method = 'other';
      if (ssin.isValidSSIN(v)) {
        method = 'SSIN';
      } else if (ssin.isValidSSINbis(v)) {
        method = 'SSINbis';
      } else if (codepat.isValidCodepat(v)) {
        method = 'codepat';
      }
      obj[METHOD] = method;
      obj[GENERATED] = Boolean(isGenerated);
      return obj;
    });
  }

  function patientIDisGenerated() {
    return Boolean(manipulator.read()[GENERATED]);
  }

  function initializePropertyManipulators() {
    var propertyManipulators = documentmodel.helpers.propertyManipulators(
      description.nestedDescriptions,
      manipulator);
    manipulator.propertyManipulators = propertyManipulators;
    manipulator.nestedManipulators = _.values(propertyManipulators);
  }

  function has(obj, propertyName) {
    return Object.hasOwnProperty.call(obj, propertyName);
  }

  function enrichSSIN(data) {
    var propMs = manipulator.propertyManipulators;
    function transfer(propName, manipulatorName, f) {
      if (has(data, propName)) {
        f(propMs[manipulatorName], data[propName]);
      }
    }

    if (has(data, 'inss')) {
      if (typeof data.inss === 'string' && data.inss !== '') {
        manipulator.value = data.inss;
      }
    }

    transfer('internalId', 'internal_patient_id', function (m, value) {
      if (typeof value === 'string' && value !== '') {
        m.value = value;
      }
    });

    transfer('lastName', 'name', function (m, value) {
      if (typeof value === 'string' && value !== '') {
        m.value = value;
      }
    });

    transfer('firstName', 'first_name', function (m, value) {
      if (typeof value === 'string' && value !== '') {
        m.value = value;
      }
    });

    transfer('dateOfBirth', 'date_of_birth', function (m, value) {
      if (typeof value === 'string' && value.length >= 10) {
        m.value = moment(value.substr(0, 10), 'YYYY-MM-DD').toDate();
      }
    });

    transfer('gender', 'sex', function (m, value) {
      if (value === 'FEMALE') { m.selectedOption = 'F'; }
      if (value === 'MALE') { m.selectedOption = 'M'; }
      if (value === 'UNKNOWN') { m.selectedOption = 'U'; }
    });

    transfer('deceased', 'deceased', function (m, value) {
      if (typeof value === 'boolean') {
        m.value = value;
      }
    });

    transfer('dateOfDeath', 'date_of_death', function (m, value) {
      if (typeof value === 'string' && value.length >= 10) {
        m.value = moment(value.substr(0, 10), 'YYYY-MM-DD').toDate();
      }
    });

    transfer('district', 'place_of_residence', function (m, value) {
      if (typeof value === 'string' && value !== '') {
        m.setValue(value);
      }
    });
  }

  function setupCodepatComputation() {
    // codepat stuff
    var memoized = manipulator.computedExpressionsMemoized.codepat;

    /**
     * Return the codepat that would be used when we accept a generated codepat.
     * If there is already some other patient ID value, we will not use a codepat.
     */
    manipulator.generatedCodepat = function () {
      var current = accessID.get();
      var ok = patientIDisGenerated() ||
               current === null ||
               typeof current === 'undefined' ||
               current === '';
      return ok ? memoized.value() : undefined;
    };

    manipulator.acceptGeneratedCodepat = function () {
      var codepat = manipulator.generatedCodepat();
      if (typeof codepat === 'string') {
        setPatientID(codepat, true);
      }
    };

    manipulator.rejectGeneratedCodepat = function () {
      if (patientIDisGenerated()) {
        // clear the generated codepat identifier
        setPatientID('', false);
      }
    };

    // no codepat computation when the document is read-only
    if (manipulator.document.readOnly || isPseudonymised) { return; }

    var accessCodepat = manipulator.document.compilePath(
      manipulator.path.concat([ 'codepat' ]),
      [ { patientID: manipulator.path, private: true, keep: supportCodepat } ]);

    // The following code make sure the codepat is kept up to date.

    // This boolean prevents infinite loops: updating the manipulator
    // triggers an invalidation, which in turn triggers the onInvalidate
    // listener again.
    var isUpdating = false;
    manipulator.onPostInvalidate(function () {
      // When the invalidation is caused by assigning the computed value to
      // manipulator.value, we do nothing.
      if (isUpdating) { return; }
      if (!manipulator.isActive()) { return; }
      var codepat = memoized.value();
      isUpdating = true;
      accessCodepat.set(codepat);
      if (accessID.get() !== codepat) {
        if (patientIDisGenerated()) {
          if (typeof codepat === 'string') {
            setPatientID(codepat, true);
          } else {
            // clear the generated codepat identifier
            setPatientID('', false);
          }
        } else {
          if (manipulator.csvGeneratedCodepat) {
            if (typeof codepat === 'string' && _.isEmpty(manipulator.value)) {
              setPatientID(codepat, true);
              manipulator.csvGeneratedCodepat = null;
            }
          }
        }
      } else {
        // when the patient id is equal to the generated codepat, we might still
        // want to update the 'generated' flag
        if (manipulator.csvGeneratedCodepat) {
          setPatientID(codepat, true);
          manipulator.csvGeneratedCodepat = null;
        }
      }
      isUpdating = false;
    });
  }
}

function codepatComputedExpression(description) {
  description.computedExpressions.codepat = {
    compute: function (data, manipulator) {
      if (manipulator.isPseudonymised) { return null; }
      var props = manipulator.propertyManipulators;
      var firstName = props.first_name.value;
      var lastName = props.name.value;
      var birthdate = props.date_of_birth.value;
      if (birthdate) {
        birthdate = moment(birthdate).format('YYYY-MM-DD');
      }
      var sex = props.sex.selectedOption;
      return codepat.generateCodepat(firstName, lastName, birthdate, sex);
    }
  };
}

function validatePatientID(description, data, manipulator, allowCodepat) {
  if (description.isAbsent(data, manipulator)) {
    return 'pending';
  }
  if (description.isEmpty(data, manipulator)) {
    return 'pending';
  }
  if (data !== Object(data)) {
    return 'failed';
  }
  if (!manipulator.isPseudonymised &&
    manipulator.getMethod() !== 'SSIN' &&
    manipulator.getMethod() !== 'SSINbis' &&
    (!allowCodepat ||
      manipulator.value !== manipulator.computedExpressions.codepat)) {
    return 'failed';
  }
  return 'ok';
}

var hdPatientIDCompileStep = {
  on: 'hdPatientID',
  require: [ 'skrDescription' ],
  compile: function (env, config, description) {

    // the use of codepat IDs is allowed unless explicitly disallowed
    var allowCodepat = _.has(config, 'allowCodepat') ? Boolean(config.allowCodepat) : true;
    description.allowCodepat = allowCodepat;

    description.assertions.patientID = {
      check: function (data, manipulator) {
        return validatePatientID(description, data, manipulator, allowCodepat);
      }
    };

    description.assertions.belgianResident = {
      level: 'warning',
      check: function (data, manipulator) {
        var por = manipulator.propertyManipulators.place_of_residence;
        if (!por) {
          return 'pending';
        }
        var usesBelgianPostalCode = por.hasCode();
        if (usesBelgianPostalCode) {
          var code = por.getValue();
          if (code === '999' || code === '9999') {
            return 'ok';
          }
        }
        if (usesBelgianPostalCode &&
          manipulator.getMethod() !== 'SSIN' &&
          manipulator.getMethod() !== 'SSINbis') {
          return 'failed';
        }
        return 'ok';
      }
    };

    description.messages.patientID = 'This is not a recognised identifier. Please use ' +
      'a social security identification number (SSIN) when available. If not available, please ' +
      'leave this field empty for an automatically generated identifier, based on name, ' +
      'first name, birth date and sex.';

    description.messages.belgianResident = 'No valid social security identification ' +
      'number (SSIN) was encoded for this Belgian resident. Please use a SSIN number for ' +
      'Belgian residents.';

    description.emptyPredicates.push(function (data, manipulator) {
      var patID = manipulator.value;
      return typeof patID !== 'string' || patID.length < 1;
    });

    description.isReadyPredicates.push(function (manipulator) {
      if (!manipulator.enricher) { return true; }
      var enricher = manipulator.enricher;
      var isBusy = enricher.requestBySSINPending() ||
                   enricher.requestByInternalPending();
      return !isBusy;
    });

    description.isReadyPredicates.push(function (manipulator) {
      if (!manipulator.stableDataLoader) { return true; }
      return manipulator.stableDataLoader.isDone();
    });

    var defaultHelpText =
      'Please encode a social security identification number (SSIN). Please note that ' +
      'the patient ID is always pseudonymised before being transferred to Healthdata.';

    description.type = 'patientID';
    description.label = config.label || config.name;
    description.help = config.help || defaultHelpText;

    env.skrObject = {
      privateByDefault: true,
      addProperty: function addProperty(property, propertyName) {
        // sanity check: avoids infinite loops
        if (property === description) {
          throw new Error(
            'Circular description when adding ' + propertyName +
            '. Perhaps a missing data directive?');
        }
        description.nestedDescriptions[propertyName] = property;
        property.isActivePredicates.push(function (manipulator) {
          return manipulator.description.hdKeep ||
                 manipulator.description.hdCoded ||
                 !manipulator.options.isPseudonymised;
        });
      }
    };

    var supportCodepat = config.codepat;

    if (allowCodepat) {
      codepatComputedExpression(description);
    }

    description.addManipulatorBuilder(function (manipulator) {
      buildPatientIDmanipulator(manipulator, allowCodepat, supportCodepat);
    });

    description.assertions.generated = {
      level: 'warning',
      check: function (data, manipulator) {
        return manipulator.patientIDisGenerated() ? 'failed' : 'ok';
      }
    };
    description.messages.generated =
      'No identifier was encoded manually. An automated identifier is generated based ' +
      'on name, first name, date of birth and sex. Please enter a social security identification ' +
      'number (SSIN) if available.';

    documentmodel.helpers.defineComputedExpression(description,
      'noSSIN', '$["%method"] !== "SSIN" && $["%method"] !== "SSINbis"');
    documentmodel.helpers.defineComputedExpression(description,
      'deceased', '$["deceased"] === true');
  }
};

var hdKeepCompileStep = {
  on: 'hdKeep',
  require: [ 'skrDescription' ],
  compile: function (env, node, skrDescription) {
    if (node.keep === true) {
      skrDescription.hdKeep = true;
    } else {
      skrDescription.hdKeep = false;
    }
    if (node.keep === 'coded') {
      skrDescription.hdCoded = true;
    } else {
      skrDescription.hdCoded = false;
    }
  }
};

var hdPatientIDautocompleteTrait = autocompleteTrait({
  on: 'hdPatientID',
  autocomplete: function (manipulator, inputData) {
    if (!inputData.csv) { return; }

    var useGenerated = 'TRUE' === csvMapping.csvData(
      inputData, manipulator, '|generated');

    if (useGenerated) {
      manipulator.csvGeneratedCodepat = true;
    }

    var result = csvMapping.csvData(inputData, manipulator);

    if (typeof result === 'number') {
      result = String(result);
    }
    if (typeof result !== 'string') { return; }
    if (result === '') { return; }

    // If the input can be interpreted as a valid SSIN by
    // removing any non-numeric characters, we retain only
    // the numbers (see WDC-1937).
    var numbersOnly = result.replace(/\D/g, '');
    if (manipulator.isValidSSIN(numbersOnly) || manipulator.isValidSSINbis(numbersOnly)) {
      result = numbersOnly;
    }

    manipulator.value = result;

    if (manipulator.enricher) {
      manipulator.enricher.enrichSSINOnChanged();
    }

    if (manipulator.stableDataLoader) {
      manipulator.stableDataLoader.loadStableDataOnChanged();
    }

    return true;
  }
});

var hdPatientIDCSVCompileStep = {
  on: 'hdPatientID',
  require: [ 'skrDescription' ],
  compile: function (env, node, skrDescription) {
    var supportCodepat = node.codepat;
    skrDescription.buildCSV = function (manipulator, csvBuilder) {
      var key = csvMapping.pathAsString(manipulator.path);
      var methodKey = csvMapping.pathAsString(manipulator.path.concat([ 'method' ]));
      var isGeneratedKey = csvMapping.pathAsString(manipulator.path.concat([ 'generated' ]));
      csvBuilder.addColumn(key, manipulator.value);
      var method = manipulator.getMethod();
      function generatedCodepat() {
        if (method === 'codepat') {
          csvBuilder.addColumn(isGeneratedKey,
            manipulator.patientIDisGenerated() ? 'TRUE' : 'FALSE');
        }
      }
      if (manipulator.isPseudonymised) {
        csvBuilder.addColumn(methodKey, method);
        generatedCodepat();
        if (supportCodepat) {
          var codepatKey = csvMapping.pathAsString(manipulator.path.concat([ 'codepat' ]));
          csvBuilder.addColumn(codepatKey, manipulator.read().codepat);
        }
      } else {
        generatedCodepat();
      }
    };
  }
};

module.exports = {
  hdKeepCompileStep: hdKeepCompileStep,
  hdPatientIDCompileStep: hdPatientIDCompileStep,
  hdPatientIDCSVTrait: documentmodel.traits.compose(
    hdPatientIDautocompleteTrait,
    {
      contributes: [
        {
          name: 'compileSteps',
          value: hdPatientIDCSVCompileStep
        }
      ]
    })
};
