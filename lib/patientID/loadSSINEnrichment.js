/* Copyright 2014-2017 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

module.exports = loadSSINEnrichment;

function loadSSINEnrichment(manipulator, performRequest) {

  var enrichmentState = manipulator.enrichmentState;

  return {
    enrichSSINOnChanged: enrichSSINOnChanged,
    enrichInternalOnChanged: enrichInternalOnChanged,
    requestBySSINPending: requestBySSINPending,
    requestBySSINFailed: requestBySSINFailed,
    requestBySSINSuccess: requestBySSINSuccess,
    requestByInternalPending: requestByInternalPending,
    requestByInternalFailed: requestByInternalFailed,
    requestByInternalSuccess: requestByInternalSuccess,
    reapplyResult: reapplyResult
  };

  function enrichSSINOnChanged() {
    // Here, we know that the user has changed the SSIN via the editor
    if (manipulator.getMethod() !== 'SSIN' &&
        manipulator.getMethod() !== 'SSINbis') { return; }
    if (enrichmentState.enrichedSSIN === manipulator.value) { return; }
    requestBySSIN(manipulator.value);
  }

  function enrichInternalOnChanged(internalPatientID) {
    if (typeof internalPatientID !== 'string') { return; }
    if (internalPatientID.length === 0) { return; }
    if (requestBySSINPending()) { return; }
    if (enrichmentState.currentlyRequesting === internalPatientID) { return; }
    if (enrichmentState.enrichedInternal === internalPatientID) { return; }
    requestByInternal(internalPatientID);
  }

  function request(id, prop) {
    enrichmentState.currentlyRequesting = id;
    enrichmentState.noMatchSSIN = undefined;
    enrichmentState.noMatchInternal = undefined;
    enrichmentState.enrichmentResult = undefined;
    enrichmentState.successSSIN = undefined;
    enrichmentState.successInternal = undefined;
    performRequest(id, function (err, result) {
      if (id !== enrichmentState.currentlyRequesting) {
        // this call is outdated, so we ignore the response
        return;
      }
      enrichmentState.currentlyRequesting = undefined;
      if (err) {
        noMatch(id, prop);
        manipulator.invalidate();
        return;
      }
      if (Object(result) !== result || result[prop] !== id) {
        noMatch(id, prop);
      } else {
        processResult(result, prop);
      }
      manipulator.invalidate();
    });
  }

  function requestBySSIN(ssin) {
    request(ssin, 'inss');
  }

  function requestByInternal(internal) {
    request(internal, 'internalId');
  }

  function requestBySSINPending() {
    var ssin = manipulator.value;
    var requesting = enrichmentState.currentlyRequesting;
    return requesting && requesting === ssin;
  }

  function requestBySSINFailed() {
    return enrichmentState.noMatchSSIN;
  }

  function requestBySSINSuccess() {
    return enrichmentState.successSSIN;
  }

  function requestByInternalPending() {
    var internal = manipulator.propertyManipulators.internal_patient_id.value;
    var requesting = enrichmentState.currentlyRequesting;
    return requesting && requesting === internal;
  }

  function requestByInternalFailed() {
    return enrichmentState.noMatchInternal;
  }

  function requestByInternalSuccess() {
    return enrichmentState.successInternal;
  }

  function reapplyResult() {
    if (!enrichmentState.enrichmentResult) { return; }
    manipulator.enrichSSIN(enrichmentState.enrichmentResult);
  }

  function noMatch(id, prop) {
    if (prop === 'inss') {
      enrichmentState.noMatchSSIN = id;
    } else {
      enrichmentState.noMatchInternal = id;
    }
  }

  function processResult(result, prop) {
    manipulator.enrichSSIN(result);
    if (prop === 'inss') {
      enrichmentState.successSSIN = true;
    } else {
      enrichmentState.successInternal = true;
    }
    enrichmentState.enrichmentResult = result;
    enrichmentState.enrichedSSIN = manipulator.value;
    enrichmentState.enrichedInternal = manipulator.propertyManipulators.internal_patient_id.value;
  }
}
