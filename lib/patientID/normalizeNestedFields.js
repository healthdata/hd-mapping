/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = normalizeNestedFields;

/**
 * The nested fields of a patient ID field in a DCD are not required to be
 * fully specified. This function takes an array of nested fields and extends
 * it to the full array of nested fields.
 */
function normalizeNestedFields(nestedPatientIDFields) {

  var patientIDFields = [
    {
      name: 'internal_patient_id',
      label: 'Internal patient ID',
      help: 'This field can be used to enter an internal ID or reference number. It is ' +
        'available for internal use only and is not transferred to Healthdata. If ' +
        'there is an integration between HD4DP and your organisation\'s patient ' +
        'administration system, additional patient identification information such as date ' +
        'of birth, sex and SSIN can be automatically enriched based on this internal patient ' +
        'ID. Please do not encode the social security identification number (SSIN) in this ' +
        'field. The SSIN must be encoded in the field \'National registry ID of the patient\'.',
      type: 'text',
      keep: false,
      required: false
    },
    {
      name: 'name',
      label: 'Last name',
      help: 'The name of the patient is never transferred to Healthdata and is available for ' +
        'your information only.',
      type: 'text',
      keep: false,
      required: 'noSSIN'
    },
    {
      name: 'first_name',
      label: 'First name',
      help: 'The first name of the patient is never transferred and is available for your ' +
        'information only.',
      type: 'text',
      keep: false,
      required: 'noSSIN'
    },
    {
      name: 'date_of_birth',
      label: 'Date of birth',
      type: 'date',
      keep: true,
      required: 'noSSIN'
    },
    {
      name: 'sex',
      label: 'Sex',
      type: 'choice',
      keep: true,
      required: 'noSSIN',
      choices: [
        {
          name: 'F',
          label: 'Female'
        },
        {
          name: 'M',
          label: 'Male'
        },
        {
          name: 'U',
          label: 'Unknown'
        }
      ]
    },
    {
      name: 'deceased',
      label: 'Deceased?',
      type: 'boolean',
      keep: true
    },
    {
      name: 'date_of_death',
      label: 'Date of death',
      type: 'date',
      keep: true,
      onlyWhen: 'deceased'
    },
    {
      name: 'place_of_residence',
      label: 'Place of residence',
      type: 'text',
      keep: true,
      referencelist: 'POSTAL_CODE'
    }
  ];

  _.each(nestedPatientIDFields, function (field) {
    var name = field.name;
    var patientIDProperty = _.find(patientIDFields, function (prop) {
      return prop.name === name;
    });
    if (patientIDProperty) {
      _.extend(patientIDProperty, field);
    } else {
      patientIDFields.push(field);
    }
  });

  return patientIDFields;
}

