/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var moment = require('moment');

// Adapted from http://tabeokatech.blogspot.be/2013/05/short-belgian-rijksregisternummer.html
module.exports.isValidSSIN = isValidSSIN;
module.exports.isValidSSINbis = isValidSSINbis;
module.exports.extractDateOfBirth = extractDateOfBirth;
module.exports.extractSex = extractSex;

function isValidSSIN(input) {
  return isValidSSINBase(input) && hasValidMonth(input);
}

function isValidSSINbis(input) {
  return isValidSSINBase(input) && !hasValidMonth(input);
}

function hasValidMonth(input) {
  var month = parseInt(input.substr(2, 2));
  return 1 <= month && month <= 12;
}

function isValidSSINBase(input) {

  if (typeof input !== 'string') { return false; }

  // WDC-1697
  // We only consider "canonical" SSINs valid, i.e., without dots or dashes.
  // This is to ensure that SSINs can safely be used as a unique ID.

  // // Remove non-numeric input
  // input = input.replace(/\D/g, '');

  // RR numbers need to be 11 chars long
  if (input.length !== 11) {
    return false;
  }

  var main = input.substr(0, 9);
  var check = input.substr(9, 2);

  // first check without 2
  // then check with 2 appended for y2k+ births
  return isValid(main, check) || isValid('2' + main, check);
}

function extractDateOfBirth(input) {
  if (!isValidSSIN(input)) { return; }

  // First we need to figure out the century in which the person was born
  var main = input.substr(0, 9);
  var check = input.substr(9, 2);

  var century = isValid('2' + main, check) ? '20' : '19';

  var m = moment(century + main.substr(0, 6), 'YYYYMMDD');
  if (m.isValid()) {
    return m.toDate();
  }
}

function extractSex(input) {
  if (!isValidSSIN(input)) { return; }

  var isFemale = Number(input.substr(6, 3)) % 2 === 0;

  return isFemale ? 'F' : 'M';
}

function isValid(numberString, check) {
  return 97 - (parseInt(numberString) % 97) === parseInt(check);
}
