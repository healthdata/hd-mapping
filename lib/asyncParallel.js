/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = asyncParallel;

function asyncParallel(tasks, cb) {
  var results = {};
  var count = 0;
  var error;
  _.each(tasks, function () {
    count += 1;
  });
  _.each(tasks, function (task, name) {
    // count += 1;
    task(function (err, result) {
      // is this the first error to occur?
      if (err && !error) {
        error = err;
        return cb(err);
      }
      // has another error occurred?
      if (error) { return; }
      // all is fine
      results[name] = result;
      count -= 1;
      if (count === 0) {
        return cb(undefined, results);
      }
    });
  });
}
