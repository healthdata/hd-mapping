/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var documentmodel = require('documentmodel');

var editorGroupTrait = documentmodel.components.editorGroupTrait;
var editorInputTrait = documentmodel.components.editorInputTrait;

var hdPatientIDInput = editorGroupTrait('hdPatientIDInput',
  documentmodel.components.registerComponent,
  { componentName: 'hdPatientIDInput' });

var hdQuestionnaire = editorGroupTrait('hdQuestionnaire',
  documentmodel.components.registerComponent,
  {
    componentName: 'hdQuestionnaire',
    postCompile: function (env, component) {
      component.hdQuestionnaire = env.hdQuestionnaire;

      // because questions are not nested fields of the questionnaire, but
      // rather siblings, we need to search for the question manipulators on
      // the common parent manipulator
      var origPropertyManipulator = env.skrPropertyManipulator;
      env.skrPropertyManipulator = function (propertyName, manipulator) {
        return origPropertyManipulator(propertyName, manipulator.parent);
      };
    }
  });

var hdQuestionInput = editorInputTrait('hdQuestionInput', {
  postCompile: function (env, input) {
    input.validAnswers = env.hdQuestionnaire.validAnswers;
  }
});

module.exports = {
  hdReferenceListInput: editorInputTrait('hdReferenceListInput'),
  hdStaticImageInput: editorInputTrait('hdStaticImageInput'),
  hdPatientIDInput: hdPatientIDInput,
  hdQuestionnaire: hdQuestionnaire,
  hdQuestionInput: hdQuestionInput
};
