/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var request = require('request');

var TTL = 5000;

var dcdCache = {};

function dcGet(key) {
  var match = dcdCache[key];
  if (_.isUndefined(match)) { return; }
  dcdPut(key, match.value);
  return match.value;
}

function dcdPut(key, value) {
  var existing = dcdCache[key];
  if (!_.isUndefined(existing)) {
    clearTimeout(existing.tmo);
  }
  dcdCache[key] = {
    value: value,
    tmo: setTimeout(function () { dcdCache[key] = undefined; }, TTL)
  };
}

function resolveDCD(dataCollectionDefinitionUri, cb) {
  var fromCache = dcGet(dataCollectionDefinitionUri);
  if (fromCache) {
    process.nextTick(_.partial(cb, null, fromCache));
    return;
  }
  request.get(dataCollectionDefinitionUri, function (err, response, body) {
    if (err) { return cb(err); }
    if (response.statusCode !== 200) {
      return cb(new Error('Expected status code 200, got ' + response.statusCode +
        '. URI used was ' +
        dataCollectionDefinitionUri + ', response body was ' + limitSize(body)));
    }
    var dcd;
    try {
      dcd = parseJSONwhenString(JSON.parse(body).content);
    } catch (e) {
      return cb(new Error('Could not parse data collection definition. URI used was ' +
        dataCollectionDefinitionUri + ', response body was ' + limitSize(body) + '\n\n' + e.stack));
    }
    dcdPut(dataCollectionDefinitionUri, dcd);
    return cb(undefined, dcd);
  });
}

function parseJSONwhenString(input) {
  return typeof input === 'string' ? JSON.parse(input) : input;
}

function limitSize(input) {
  var asString = String(input);
  return asString.length < 200 ? asString : asString.substr(0, 200) + '...';
}

module.exports = resolveDCD;
