/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

/* jshint sub: true */

var fs = require('fs');
var xmldom = require('xmldom').DOMParser;
var xpath = require('xpath');
var queries = require('../../lib/mappings/kmehrMapping').queries;
var documentmodel = require('documentmodel');

describe.skip('KMEHR parsing', function () {

  var fileName = './test/fixtures/examples.xml';
  var contents;

  before(function (done) {
    fs.readFile(fileName, function (err, result) {
      if (err) { return done(err); }
      contents = new xmldom().parseFromString(result.toString());
      done();
    });
  });

  function query(q) {
    return xpath.select(q, contents);
  }

  describe('queries', function () {

    it('should extract the INSS', function () {
      expect(queries.kmehr_inss(query)).to.equal('44071154303');
    });

    it('should extract the sex', function () {
      expect(queries.kmehr_sex(query)).to.equal('male');
    });

    it('should extract the nationality', function () {
      expect(queries.kmehr_nationality(query)).to.equal('be');
    });

    it('should extract the birthdate', function () {
      expect(queries.kmehr_birthdate(query)).to.equal('1986-09-14');
    });

    it('should extract the deathdate', function () {
      expect(queries.kmehr_deathdate(query)).to.equal('2999-12-31');
    });

    it('should extract the register', function () {
      expect(queries.kmehr_register(query)).to.equal('IQECAD');
    });

    it('should extract parameter content by name', function () {
      var paramName = 'name_of_parameter';
      expect(queries.kmehr_parameterContent(query, paramName)).to.equal('0.22424');
    });

    it('should extract parameter unit by name', function () {
      var paramName = 'name_of_parameter';
      expect(queries.kmehr_parameterUnit(query, paramName)).to.equal('blabla');
    });

    it('should extract parameter date and time by name', function () {
      var paramName = 'name_of_parameter';
      expect(queries.kmehr_parameterDateTime(query, paramName)).to.equal('2014-07-18T10:00:05');
    });

    it('should extract a boolean by name', function () {
      var paramName = 'communication_issues';
      expect(queries.kmehr_boolean(query, paramName)).to.equal('false');

      paramName = 'core_family';
      expect(queries.kmehr_boolean(query, paramName)).to.equal('true');
    });

    it('should extract a local list value', function () {
      var paramName = 'insuline_scheme';
      var listName = 'CD-HD-INSULINE_SCHEME';
      expect(queries.kmehr_listValue(query, listName, paramName))
        .to.equal('Basal prandial insuline treatment (basal-bolus)');
    });

    it('should extract a text by name', function () {
      var paramName = 'insulineschema';
      expect(queries.kmehr_text(query, paramName))
        .to.equal('standard_mixture_occasional_extra_administration');
    });

    it('should extract a date by name', function () {
      var paramName = 'first_insuline';
      expect(queries.kmehr_date(query, paramName)).to.equal('2000-07-09');
    });

    it('should extract the message date', function () {
      expect(queries.kmehr_messagedate(query)).to.equal('2013-07-09');
    });

    it('should extract a number by name', function () {
      var paramName = 'value_of_pi';
      expect(queries.kmehr_number(query, paramName))
        .to.equal('3.14159265358979323846264338327950');
    });

    it('should extract a year by name', function () {
      var paramName = 'jaartal';
      expect(queries.kmehr_year(query, paramName)).to.equal('2014');
    });

  });

  describe('kmehr directives', function () {

    var env, inputData, result;

    beforeEach(function () {
      env = {};
      inputData = {};
      inputData.kmehr = query;
    });

    function runMapper(dataDefinition) {
      env = documentmodel.compile(documentmodel.directives, dataDefinition);
      var target = {};
      var doc = documentmodel.makeDocument(target, env.skrDescription);
      doc.root.autoCompleteWith(inputData);
      result = target.value;
    }

    it('should map the INSS to a number field', function () {
      runMapper({
        skrNumber: true,
        kmehrINSS: true
      });
      expect(result).to.equal(44071154303);
    });

    it('should map the nationality to a string', function () {
      runMapper({
        skrText: true,
        kmehrNationality: true
      });
      expect(result).to.equal('be');
    });

    it('should map the sex to a string', function () {
      runMapper({
        skrText: true,
        kmehrSex: true
      });
      expect(result).to.equal('male');
    });

    it('should map a parameter to a decimal number', function () {
      runMapper({
        skrNumber: true,
        kmehrParameterContent: { name: 'name_of_parameter' }
      });
      expect(result).to.equal(0.22424);
    });

    it('should map a parameter recordtime to a Date', function () {
      // Currently the time component is discarded
      runMapper({
        skrDate: true,
        kmehrParameterDateTime: { name: 'name_of_parameter' }
      });
      expect(result).to.equal('2014-07-18');
    });

    it('should map a boolean to a boolean', function () {
      runMapper({
        skrBoolean: true,
        kmehrBoolean: { name: 'core_family' }
      });
      expect(result).to.equal(true);
    });

    it('should map a boolean to a multichoice option', function () {
      runMapper({
        skrMultiChoice: [
          {
            skrOption: 'withChild',
            kmehrMultiChoiceOption: { name: 'communication_issues_child' }
          },
          {
            skrOption: 'withParents',
            kmehrMultiChoiceOption: { name: 'communication_issues_parents' }
          }
        ]
      });
      expect(result).to.deep.equal({
        selectedOptions: {
          withChild: true,
          withParents: false
        }
      });
    });

    it('should map a list value to a choice option', function () {
      runMapper({
        skrChoice: [
          {
            skrOption: 'A',
            kmehrOptionValue: 'prepuberteit'
          },
          {
            skrOption: 'B',
            kmehrOptionValue: 'puberteit'
          }
        ],
        kmehrListValue: {
          name: 'puberty',
          listName: 'CD-HD-PUBERTY'
        }
      });
      expect(result).to.deep.equal({
        selectedOption: 'A'
      });
    });

    it('should map a text to a choice option', function () {
      runMapper({
        skrChoice: [
          {
            skrOption: 'A',
            kmehrOptionValue: 'standard_mixture_occasional_extra_administration'
          },
          {
            skrOption: 'B',
            kmehrOptionValue: 'individual_mixture_occasional_extra_administration'
          }
        ],
        kmehrText: {
          name: 'insulineschema'
        }
      });
      expect(result).to.deep.equal({
        selectedOption: 'A'
      });
    });

    it('should map the deathdate to a deceased boolean', function () {
      runMapper({
        skrBoolean: true,
        kmehrDeceased: true
      });
      expect(result).to.be.true;
    });

    it('should map the sex to a choice', function () {
      runMapper({
        skrChoice: [
          { skrOption: 'm', kmehrOptionValue: 'male' },
          { skrOption: 'v', kmehrOptionValue: 'female' }
        ],
        kmehrSex: true
      });
      expect(result).to.deep.equal({ selectedOption: 'm' });
    });

    it('should map the deathdate to a date', function () {
      runMapper({
        skrDate: true,
        kmehrDeathdate: true
      });
      expect(result).to.equal('2999-12-31');
    });

    it('should map a date to a date', function () {
      runMapper({
        skrDate: true,
        kmehrDate: { name: 'first_insuline' }
      });
      expect(result).to.equal('2000-07-09');
    });

    it('should map the message date to a date', function () {
      runMapper({
        skrDate: true,
        kmehrMessageDate: true
      });
      expect(result).to.equal('2013-07-09');
    });

    it('should map a number to a number', function () {
      runMapper({
        skrNumber: true,
        kmehrNumber: {
          name: 'value_of_pi'
        }
      });
      expect(result).to.equal(3.141592653589793);
    });

    it('should map a year to a number', function () {
      runMapper({
        skrNumber: true,
        kmehrYear: { name: 'jaartal' }
      });
      expect(result).to.equal(2014);
    });

  });

});
