/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var fs = require('fs');
var path = require('path');
var request = require('supertest');
var nock = require('nock');

var config = require('../../config');

var mappingsAPI = require('../../lib/mappings/api');
var asyncParallel = require('../../lib/asyncParallel');
var documentmodelHelpers = require('../../lib/documentmodelHelpers');

var mergeMapping = require('../../lib/mappings/mergeMapping');

var handlebars = require('../../lib/handlebars/core');

var csvMapping = require('../../lib/mappings/csvMapping');
var esMapping = require('../../lib/mappings/esMapping');

var basicDataSet = require('../../lib/mappings/basicDataSet');

var SKRYV_TO_CSV = 'skryv to csv';
var SKRYV_VALIDATION = 'skryv validation';
var DCD_TO_ES_MAPPING_FILE = 'dcd to es mapping file';
var DCD_TO_HANDLEBARS = 'dcd to handlebars';
var SKRYV_TO_ES = 'skryv to es';
var MERGE = 'merge';
var BDS_IMPORT = 'basicDataSet import';
var NOTES_MIGRATION = 'noted migration';

// To make it easier to update the test fixtures after code changes,
// this constant indicates wether tests should write the computed fixture to
// file instead of comparing them to an already present value.
var WRITE_FIXTURES = process.env.WRITE === 'true';

var tests = [
  {
    name: 'enrich_patient_id',
    method: SKRYV_TO_CSV,
    exportCSV: false
  },
  {
    'name': 'from_context',
    method: SKRYV_TO_CSV
  },
  {
    name: 'ili_basisdataset',
    method: BDS_IMPORT
  },
  {
    name: 'nested_lists_crash',
    method: SKRYV_TO_CSV
  },
  {
    name: 'concerns',
    method: BDS_IMPORT
  },
  {
    name: 'IQED_basisdataset',
    method: BDS_IMPORT
  },
  {
    name: 'generated_codepat',
    method: SKRYV_TO_CSV
  },
  {
    name: 'notes',
    method: NOTES_MIGRATION
  },
  {
    name: 'comments_count',
    method: NOTES_MIGRATION
  },
  {
    name: 'multi_patid',
    method: 'skryv to csv'
  },
  {
    name: 'comments_count',
    method: 'skryv to es'
  },
  {
    name: 'multi_patid',
    method: 'skryv to es'
  },
  {
    name: 'multi_patid_coded',
    method: 'skryv to csv',
    importCSV: false
  },
  {
    name: 'multi_patid_coded',
    method: 'skryv to es'
  },
  {
    name: 'basisdataset',
    method: BDS_IMPORT
  },
  {
    name: 'handlebars_demo',
    method: 'dcd to handlebars'
  },
  {
    name: 'doctarget',
    method: 'skryv validation'
  },
  {
    name: 'reflist_uniqueid',
    method: 'skryv to csv'
  },
  {
    name: 'fix_bad_dates',
    importCSV: false,
    method: 'skryv to csv'
  },
  {
    name: 'testcollection',
    method: 'skryv to csv',
    importCSV: false
  },
  {
    name: 'follow_up',
    method: 'skryv validation'
  },
  {
    name: 'parsedd',
    method: 'skryv to csv',
    importCSV: false
  },
  {
    name: 'timezones',
    method: 'skryv to csv',
    importCSV: false
  },
  {
    name: 'timezones',
    method: 'skryv to es',
    importCSV: false
  },
  {
    name: 'missing_label_es',
    method: 'skryv to es'
  },
  {
    name: 'codepat',
    method: 'skryv to csv'
  },
  {
    name: 'codepat',
    method: 'skryv validation'
  },
  {
    name: 'codepat_allowed',
    method: 'skryv to csv',
    importCSV: false
  },
  {
    name: 'bnmdr_dod',
    method: 'skryv to csv',
    importCSV: false
  },
  {
    method: 'skryv to csv',
    name: 'compfi_bnmdr',
    importCSV: false
  },
  {
    method: 'skryv to csv',
    name: 'condfi_bnmdr',
    importCSV: false
  },
  {
    method: 'skryv to csv',
    name: 'uniqueid_bug'
  },
  // FIXME these still use the obsole intermediate docdef format
  // {
  //   method: 'skryv to csv',
  //   name: 'iqecad'
  // },
  // {
  //   method: 'skryv validation',
  //   name: 'iqecad'
  // },
  {
    method: 'skryv to csv',
    name: 'list'
  },
  {
    method: 'skryv to csv',
    name: 'patientID'
  },
  {
    method: 'skryv to csv',
    name: 'patientID_hd4res',
    importCSV: false
  },
  {
    method: 'skryv to csv',
    name: 'bad_patientID'
  },
  {
    method: 'skryv to csv',
    name: 'questionnaire'
  },
  {
    method: 'skryv to csv',
    name: 'crrd'
  },
  {
    method: 'skryv to csv',
    name: 'BNMDR1'
  },
  {
    method: 'skryv to csv',
    name: 'bewsd'
  },
  {
    method: 'skryv to csv',
    name: 'BCFR1'
  },
  {
    method: 'skryv to csv',
    name: 'crrd2'
  },
  {
    method: 'skryv to csv',
    name: 'referenceList'
  },
  {
    method: 'skryv validation',
    name: 'referenceList'
  },
  {
    method: 'dcd to es mapping file',
    name: 'crrd'
  },
  {
    method: 'skryv to es',
    name: 'crrd'
  },
  {
    method: 'skryv to es',
    name: 'list'
  },
  {
    method: 'dcd to es mapping file',
    name: 'list'
  },
  {
    method: 'dcd to es mapping file',
    name: 'crrd3'
  },
  {
    method: 'dcd to es mapping file',
    name: 'patientID'
  },
  {
    method: 'skryv to es',
    name: 'invalidChoice'
  },
  {
    method: 'skryv to es',
    name: 'patientID'
  },
  {
    method: 'merge',
    name: 'mergeExamples'
  },
  {
    method: 'merge',
    name: 'BCFR1'
  }
];

// removes undefined fields
function normalizeViaJSON(object) {
  if (_.isUndefined(object)) { return; }
  return JSON.parse(JSON.stringify(object));
}

var FIXTURES = './test/fixtures';

function loadFixture(fileName, options, cb) {
  // options is optional, so if it is not there, the callback is the second
  // argument.
  if (typeof options === 'function') {
    cb = options;
    options = {};
  }
  fs.readFile(path.join(FIXTURES, fileName), function (err, contents) {
    if (err) {
      if (options.optional) { return cb(); }
      return cb(err);
    }
    // convert the buffer to a string
    var result = contents.toString();
    // To make sure tests do not fail due to Windows line endings, we replace
    // all Windows line endings with Unix line endings.
    result = result.replace(/\r\n/g, '\n');
    if (options.json) {
      result = JSON.parse(result);
    }
    return cb(null, result);
  });
}

function storeFixture(fileName, options, data, cb) {
  // options is optional, so if it is not there, the callback is the second
  // argument.
  if (typeof options === 'function') {
    cb = options;
    options = {};
  }
  if (options.json) {
    // add newline to end of file
    data = JSON.stringify(data, null, 2) + '\n';
  }
  fs.writeFile(path.join(FIXTURES, fileName), data, cb);
}

describe('All mappings', function () {

  var api;
  var dataReference;

  var loadedFixtures;
  var documentDefinitionData;
  var documentDefinition;
  var documentDefinitionComponent;

  var WORKFLOW_ENGINE_HOST = 'http://localhost:8080';
  var WORKFLOW_ENGINE_PATH = '/healthdata_hd4dp';
  var DATA_REFERENCE_PATH = WORKFLOW_ENGINE_PATH + '/datareference';
  var TRANSLATIONS_PATH = WORKFLOW_ENGINE_PATH + '/translations';
  var PATIENT_ID_PATH = WORKFLOW_ENGINE_PATH + '/patient/mapping/';
  var STABLE_DATA_PATH = WORKFLOW_ENGINE_PATH + '/stabledata/mapping/';

  it('should use the base URL from the config', function () {
    expect(config.WORKFLOW_ENGINE_BASE_URL)
      .to.equal(WORKFLOW_ENGINE_HOST + WORKFLOW_ENGINE_PATH);
  });

  beforeEach(function () {
    dataReference = nock(WORKFLOW_ENGINE_HOST);
  });

  beforeEach(function () {
    dataReference
      .get(TRANSLATIONS_PATH + '?language=nl')
      .reply(200, {
        'Female': 'Vrouw',
        'First name': 'Voornaam'
      });
    dataReference
      .get(TRANSLATIONS_PATH + '?language=fr')
      .reply(200, {
        'Female': 'Féminin',
        'First name': 'Prénom'
      });
  });

  beforeEach(function () {
    var knownDataReference = {
      '?language=en&code=11111111&type=RIZIV-CAMPUS&limit=0': [
        { language: 'en', code: '11111111', value: '1000' }
      ],
      '?code=2&type=CHROMOSOME': [
        { language: 'en', code: '2', value: '2' },
        { language: 'nl', code: '2', value: '2' },
        { language: 'fr', code: '2', value: '2' }
      ],
      '?code=10000403730&type=PHYSICIAN': [
        { language: 'en', code: '10000403730', value: '10000403730' },
        { language: 'nl', code: '10000403730', value: '10000403730' },
        { language: 'fr', code: '10000403730', value: '10000403730' }
      ],
      '?code=100&type=ORPHA': [
        { language: 'en', code: '100', value: 'Orpha 100' },
        { language: 'nl', code: '100', value: 'Orpha 100' },
        { language: 'fr', code: '100', value: 'Orpha 100' }
      ],
      '?code=102&type=ORPHA': [
        { language: 'en', code: '102', value: 'Orpha 102' },
        { language: 'nl', code: '102', value: 'Orpha 102' },
        { language: 'fr', code: '102', value: 'Orpha 102' }
      ],
      '?language=en&code=1000&type=POSTAL_CODE_NEIGHBOURS&limit=0': [
        { code: '1000', value: '1234' },
        { code: '1000', value: '4321' }
      ],
      '?code=1234&type=POSTAL_CODE&limit=0': [
        { language: 'en', code: '1234', value: 'Wherever' },
        { language: 'nl', code: '1234', value: 'Wherever' },
        { language: 'fr', code: '1234', value: 'Wherever' }
      ],
      '?code=9000&type=POSTAL_CODE&limit=0': [
        { language: 'en', code: '9000', value: 'Ghent' },
        { language: 'nl', code: '9000', value: 'Gent' },
        { language: 'fr', code: '9000', value: 'Gand' }
      ],
      '?code=9000&type=POSTAL_CODE': [
        { language: 'en', code: '9000', value: 'Ghent' },
        { language: 'nl', code: '9000', value: 'Gent' },
        { language: 'fr', code: '9000', value: 'Gand' }
      ],
      '?code=1000&type=POSTAL_CODE': [ { language: 'en', code: '1000' } ]
    };
    _.each(knownDataReference, function (value, query) {
      dataReference
        .persist()
        .get(DATA_REFERENCE_PATH + query)
        .reply(200, value);
    });
  });

  beforeEach(function () {
    dataReference
      .get(PATIENT_ID_PATH + '00000000097')
      .reply(200, {
        internalId: 'JV1891',
        lastName: 'Foo',
        firstName: 'Bar',
        dateOfBirth: '1992-02-17',
        gender: 'MALE',
        district: '9820',
        deceased: false,
        inss: '00000000097'
      });
  });

  beforeEach(function () {
    dataReference
      .get(STABLE_DATA_PATH + '00000000097/ENRICH')
      .reply(200, {
        csv: 'someNumber\n42'
      });
  });

  afterEach(function () {
    nock.cleanAll();
  });

  before(function () {
    api = mappingsAPI();
  });

  beforeEach(function () {
    loadedFixtures = {};
    documentDefinitionData = undefined;
    documentDefinition = undefined;
    documentDefinitionComponent = undefined;
  });

  _.each(tests, function (config) {
    var method = config.method;
    var name = config.name;
    (config.only ? describe.only : describe)('(' + name + ' - ' + method + ')', function () {
      // We always load the document definition based on the test case name
      loadDefinitionBeforeEach(name);
      if (method === SKRYV_TO_CSV) {
        testSkryvToCsv(config);
      } else if (method === SKRYV_VALIDATION) {
        testSkryvValidation(config);
      } else if (method === DCD_TO_ES_MAPPING_FILE) {
        testDcdToMappingFile(config);
      } else if (method === DCD_TO_HANDLEBARS) {
        testDcdToHandlebars(config);
      } else if (method === SKRYV_TO_ES) {
        testSkryvToEs(config);
      } else if (method === MERGE) {
        testMerge(config);
      } else if (method === BDS_IMPORT) {
        testBDSImport(config);
      } else if (method === NOTES_MIGRATION) {
        testNotesMigration(config);
      } else {
        it('should have a recognized test method', function () {
          throw new Error('Unrecognized test method: ' + method);
        });
      }
    });
  });

  function loadDocumentDataBeforeEach(documentDataName) {
    beforeEach(function (done) {
      loadFixture(documentDataName, { json: true }, function (err, documentData) {
        if (err) { return done(err); }
        loadedFixtures[documentDataName] = {
          content: documentData,
          patientID: documentData.__metadata,
          private: documentData.__private,
          coded: documentData.__coded,
          uniqueID: documentData.__uniqueID,
          toBeCoded: documentData.__toBeCoded,
          affectedFields: documentData.__affectedFields,
          context: documentData.__context
        };
        delete documentData.__coded;
        delete documentData.__private;
        delete documentData.__metadata;
        delete documentData.__uniqueID;
        delete documentData.__toBeCoded;
        delete documentData.__affectedFields;
        delete documentData.__context;
        done();
      });
    });
  }

  function loadFixtureBeforeEach(name, options) {
    beforeEach(function (done) {
      loadFixture(name, options || {}, function (err, result) {
        if (err) { return done(err); }
        loadedFixtures[name] = result;
        done();
      });
    });
  }

  function loadDefinitionBeforeEach(name) {
    var documentDefinitionName = name + '.json';
    beforeEach(function (done) {
      loadFixture(documentDefinitionName, { json: true }, function (err, definition) {
        if (err) { return done(err); }
        documentDefinitionData = definition;
        var env = {};
        documentmodelHelpers.compile(definition, env);
        documentDefinition = env.skrDescription;
        documentDefinitionComponent = env.skrEditorComponent;
        done();
      });
    });
  }

  function openFixtureDocument(documentDataName, options, cb) {
    return documentmodelHelpers.openDocument(
      documentDefinition,
      {
        content: loadedFixtures[documentDataName].content,
        private: loadedFixtures[documentDataName].private,
        coded: loadedFixtures[documentDataName].coded,
        patientID: loadedFixtures[documentDataName].patientID,
        context: loadedFixtures[documentDataName].context
      },
      options,
      cb);
  }

  function fixtureDocumentInput(documentDataName) {
    return {
      content: loadedFixtures[documentDataName].content,
      private: loadedFixtures[documentDataName].private,
      coded: loadedFixtures[documentDataName].coded,
      patientID: loadedFixtures[documentDataName].patientID
    };
  }

  function fixtureDocumentContext(documentDataName) {
    return loadedFixtures[documentDataName].context;
  }

  function singleDocumentAPIInput(documentDataName) {
    // here we use the data collection definition directly, instead of providing
    // the URL
    return {
      dataCollectionDefinition: documentDefinitionData,
      document: fixtureDocumentInput(documentDataName),
      context: fixtureDocumentContext(documentDataName)
    };
  }

  function verifyDocumentTarget(documentDataName, targets) {
    expect(targets).to.have.property('document');
    if (documentDefinitionData.requiresSupportForMultiplePatientIDs) {
      expect(targets.document).to.deep.equal({
        content: loadedFixtures[documentDataName].content,
        private: loadedFixtures[documentDataName].private
      });
    } else {
      expect(targets.document).to.deep.equal({
        content: loadedFixtures[documentDataName].content,
        patientID: loadedFixtures[documentDataName].patientID || {}
      });
    }
  }

  function verifyProgressTarget(targets) {
    expect(targets).to.have.property('progress');
    var progress = targets.progress;
    expect(progress).to.have.property('total').that.is.a('number');
    expect(progress).to.have.property('optionalRatio').that.is.a('number');
    expect(progress).to.have.property('errors').that.is.a('number');
    expect(progress).to.have.property('warnings').that.is.a('number');
  }

  function verifyUniqueIDTarget(documentDataName, targets) {
    expect(targets.uniqueID).to.equal(
      loadedFixtures[documentDataName].uniqueID);
  }

  function verifyToBeCodedTarget(documentDataName, targets) {
    expect(targets.toBeCoded).to.deep.equal(
      loadedFixtures[documentDataName].toBeCoded);
  }

  function verifyAffectedFields(documentDataName, targets) {
    expect(targets.affectedFields).to.be.an('object');
    if (loadedFixtures[documentDataName].affectedFields) {
      expect(targets.affectedFields).to.deep.equal(
        loadedFixtures[documentDataName].affectedFields);
    }
  }

  function verifyTarget(targets, target, expected) {
    expect(targets).to.have.property(target);
    expect(targets[target]).to.equal(expected);
  }

  function verifyDocument(documentDataName, document) {
    var expected =
      documentDefinitionData.requiresSupportForMultiplePatientIDs ?
      {
        documentData: {
          content: loadedFixtures[documentDataName].content,
          private: loadedFixtures[documentDataName].private
        },
        uniqueID: loadedFixtures[documentDataName].uniqueID,
        toBeCoded: loadedFixtures[documentDataName].toBeCoded
      } :
      {
        documentData: {
          content: loadedFixtures[documentDataName].content,
          patientID: loadedFixtures[documentDataName].patientID || {}
        },
        uniqueID: loadedFixtures[documentDataName].uniqueID,
        toBeCoded: undefined
      };
    expect({
      documentData: normalizeViaJSON(document.documentData),
      uniqueID: document.root.computedExpressions.uniqueID,
      toBeCoded: document.toBeCoded()
    }).to.deep.equal(expected);
  }

  function expectedJSON(name, result, cb) {
    if (WRITE_FIXTURES) {
      storeFixture(name, { json: true }, result, cb);
    } else {
      expect(normalizeViaJSON(result)).to.deep.equal(loadedFixtures[name]);
      cb();
    }
  }

  function testNotesMigration(config) {
    var name = config.name;

    var documentDataName = name + '_data.json';
    var notesName = name + '_notes.json';

    loadDocumentDataBeforeEach(documentDataName);
    loadFixtureBeforeEach(notesName, { json: true });

    it('should migrate comments to notes via the flow API', function (done) {
      request(api)
        .post('/mappings')
        .send({
          input: {
            dataCollectionDefinition: documentDefinitionData,
            document: loadedFixtures[documentDataName]
          },
          targets: [ 'notesFromComments', 'documentContentWithoutComments' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          var targets = res.body.targets;
          expect(targets.notesFromComments)
            .to.deep.equal(loadedFixtures[notesName]);
          // Verify that the comments have been removed.
          expect(targets.documentContentWithoutComments)
            .to.not.have.property('comments');
          // Verify that the document data has not been modified.
          expect(targets.documentContentWithoutComments.value)
            .to.deep.equal(loadedFixtures[documentDataName].content.value);
        })
        .end(done);
    });

  }

  function testBDSImport(config) {
    var name = config.name;

    var documentDataName = name + '_data.json';
    var xmlInputName = name + '_data.xml';

    loadDocumentDataBeforeEach(documentDataName);
    loadFixtureBeforeEach(xmlInputName);

    it('should import a document from XML using CBB annotations', function (done) {
      var doc = documentmodelHelpers.openDocument(
        documentDefinition,
        { context: loadedFixtures[documentDataName].context }, // empty document
        { readOnly: false });
      basicDataSet.importFromBasicDataSetWithCBBs(doc, loadedFixtures[xmlInputName]);
      documentmodelHelpers.waitForDocumentReady(doc, function () {
        verifyDocument(documentDataName, doc);
        done();
      });
    });

    it('should import a document from XML using CBB annotations via the flow API', function (done) {
      request(api)
        .post('/mappings/importBasicDataset')
        .send({
          input: {
            dataCollectionDefinition: documentDefinitionData,
            context: loadedFixtures[documentDataName].context,
            basicDataset: loadedFixtures[xmlInputName]
          },
          targets: [ 'document', 'progress', 'affectedFields' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          var targets = res.body.targets;
          verifyDocumentTarget(documentDataName, targets);
          verifyProgressTarget(targets);
          verifyAffectedFields(documentDataName, targets);
        })
        .end(done);
    });
  }

  function testMerge(config) {
    var name = config.name;

    var primaryDataName = name + '_data.json';
    var secondaryDataName = name + '_data_sec.json';
    var mergeDataName = name + '_data_merge.json';

    loadDocumentDataBeforeEach(primaryDataName);
    loadDocumentDataBeforeEach(secondaryDataName);
    loadDocumentDataBeforeEach(mergeDataName);

    it('should merge two documents into one', function (done) {
      asyncParallel({
        primaryDoc: function (cb) {
          openFixtureDocument(primaryDataName, { readOnly: false }, cb);
        },
        secondaryDoc: function (cb) {
          openFixtureDocument(secondaryDataName, { readOnly: false }, cb);
        }
      },
      function (err, docs) {
        if (err) { return done(err); }
        mergeMapping.merge(docs.primaryDoc, docs.secondaryDoc);
        documentmodelHelpers.waitForDocumentReady(docs.primaryDoc, function (err, result) {
          if (err) { return done(err); }
          verifyDocument(mergeDataName, result);
          done();
        });
      });
    });

    it('should merge two documents into one via the /mappings flow API', function (done) {
      request(api)
        .post('/mappings/merge')
        .send({
          input: {
            dataCollectionDefinition: documentDefinitionData,
            primary: fixtureDocumentInput(primaryDataName),
            secondary: fixtureDocumentInput(secondaryDataName)
          },
          targets: [ 'document', 'progress', 'affectedFields' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          var targets = res.body.targets;
          verifyDocumentTarget(mergeDataName, targets);
          verifyProgressTarget(targets);
          verifyAffectedFields(mergeDataName, targets);
        })
        .end(done);
    });

  }

  function testDcdToHandlebars(config) {
    var name = config.name;

    var handlebarsName = name + '.hbs';
    var language = 'nl';

    loadFixtureBeforeEach(handlebarsName);

    it('should construct a handlebars template', function () {
      var expected = loadedFixtures[handlebarsName];
      var result = handlebars.generateHandlebarsTemplate(documentDefinitionComponent, undefined, language);
      expect(result).to.equal(expected);
    });

    it('should construct a handlebars template via the /mappings flow API', function (done) {
      request(api)
        .post('/mappings')
        .send({
          input: {
            dataCollectionDefinition: documentDefinitionData,
            language: 'nl'
          },
          targets: [ 'handlebars' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          expect(res.body.targets).to.have.property('handlebars');
          expect(res.body.targets.handlebars).to.deep.equal(loadedFixtures[handlebarsName]);
        })
        .end(done);
    });
  }

  function testDcdToMappingFile(config) {
    var name = config.name;

    var mappingFileName = name + '_esmapping.json';

    loadFixtureBeforeEach(mappingFileName, { json: true });

    it('should construct an elasticsearch mapping file', function (done) {
      var expected = loadedFixtures[mappingFileName];
      esMapping.mappingFromDCDtoEsmapping(documentDefinition, function (err, result) {
        if (err) { return done(err); }
        expect(result).to.have.deep.property(
          'properties.document.properties.documentContent.properties.originalDocumentData');
        delete result.properties.document.properties.documentContent.properties.originalDocumentData;
        expect(result).to.deep.equal(expected);
        done();
      });
    });

    it('should construct an elasticsearch mapping file via the /mappings flow API', function (done) {
      request(api)
        .post('/mappings')
        .send({
          input: {
            dataCollectionDefinition: documentDefinitionData
          },
          targets: [ 'esmapping' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          expect(res.body.targets).to.have.property('esmapping');
          var result = res.body.targets.esmapping;
          expect(result).to.have.deep.property(
            'properties.document.properties.documentContent.properties.originalDocumentData');
          delete result.properties.document.properties.documentContent.properties.originalDocumentData;
          expect(result).to.deep.equal(loadedFixtures[mappingFileName]);
        })
        .end(done);
    });
  }

  function testSkryvToEs(config) {
    var name = config.name;

    var documentDataName = name + '_data.json';
    var edDataName = name + '_data_es.json';

    loadDocumentDataBeforeEach(documentDataName);
    loadFixtureBeforeEach(edDataName, { json: true });

    it('should export document to elasticsearch format', function (done) {
      var doc = openFixtureDocument(documentDataName);
      esMapping.mappingFromDocumentToEs(doc, function (err, result) {
        if (err) { return done(err); }
        expect(result).to.have.property('originalDocumentData');
        delete result.originalDocumentData;
        expectedJSON(edDataName, result, done);
      });
    });

    it('should export document to elasticsearch format via the /mappings flow API', function (done) {
      request(api)
        .post('/mappings')
        .send({
          input: singleDocumentAPIInput(documentDataName),
          targets: [ 'es' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          expect(res.body.targets).to.have.property('es');
          var result = res.body.targets.es;
          expect(result).to.have.property('originalDocumentData');
          delete result.originalDocumentData;
          expect(result).to.deep.equal(loadedFixtures[edDataName]);
        })
        .end(done);
    });
  }

  function testSkryvValidation(config) {

    var name = config.name;

    var documentDataName = name + '_data.json';
    var validationName = name + '_validation.json';

    loadDocumentDataBeforeEach(documentDataName);
    loadFixtureBeforeEach(validationName, { json: true });

    it('should validate the document', function (done) {
      var expectedValidation = loadedFixtures[validationName];
      openFixtureDocument(documentDataName, {}, function (err, doc) {
        if (err) { return done(err); }
        expect(JSON.parse(JSON.stringify(documentmodelHelpers.validationInfo(doc))))
          .to.deep.equal(expectedValidation);
        done();
      });
    });

    it('should validate the document via the /mappings flow API', function (done) {
      var expectedValidation = loadedFixtures[validationName];
      request(api)
        .post('/mappings')
        .send({
          input: singleDocumentAPIInput(documentDataName),
          targets: [ 'validation' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          var targets = res.body.targets;
          expect(targets).to.have.property('validation');
          expect(targets.validation).to.deep.equal(expectedValidation);
        })
        .end(done);
    });
  }

  function testSkryvToCsv(config) {

    var importCSV = !config.hasOwnProperty('importCSV') || config.importCSV;
    var exportCSV = !config.hasOwnProperty('exportCSV') || config.exportCSV;

    var name = config.name;

    var documentDataName = name + '_data.json';
    var csvDataName = name + '_data.csv';
    var stableCsvDataName = name + '_data_stable.csv';
    var validationName = name + '_validation.json';

    loadDocumentDataBeforeEach(documentDataName);
    loadFixtureBeforeEach(csvDataName);
    loadFixtureBeforeEach(stableCsvDataName, { optional: true });
    loadFixtureBeforeEach(validationName, { json: true, optional: true });

    it('should compute unique ID of document', function (done) {
      var doc = openFixtureDocument(documentDataName);
      csvMapping.mapDocumentToUniqueID(doc, function (err, result) {
        if (err) { return done(err); }
        expect(result).to.equal(loadedFixtures[documentDataName].uniqueID);
        done();
      });
    });

    exportCSV && it('should export document to csv', function (done) {
      var expected = loadedFixtures[csvDataName];
      var doc = openFixtureDocument(documentDataName);
      csvMapping.mapDocumentToCSV(doc, function (err, result) {
        if (err) { return done(err); }
        expect(result).to.equal(expected);
        done();
      });
    });

    exportCSV && it('should export document to stable csv', function (done) {
      var expected = loadedFixtures[stableCsvDataName];
      // when the test case does not contain stable fields, we avoid the need for
      // an "empty" csv file
      if (typeof expected === 'undefined') {
        expected = '\n\n'; // this value is the correct csv result when there are no stable fields
      }
      var doc = openFixtureDocument(documentDataName);
      csvMapping.mapDocumentToCSV(doc, { stable: true }, function (err, result) {
        if (err) { return done(err); }
        expect(result).to.equal(expected);
        done();
      });
    });

    if (importCSV) {
      it('should import document from CSV via the /mappings flow API', function (done) {
        request(api)
          .post('/mappings/importCSV')
          .send({
            input: {
              accessToken: 'abc123',
              dataCollectionDefinition: documentDefinitionData,
              context: loadedFixtures[documentDataName].context,
              csv: loadedFixtures[csvDataName]
            },
            targets: [ 'document', 'uniqueID',
                       'progress', 'pendingAttachments',
                       'toBeCoded', 'affectedFields' ]
          })
          .expect('Content-Type', 'application/json')
          .expect(200)
          .expect(function (res) {
            expect(res.body).to.have.property('targets');
            var targets = res.body.targets;
            verifyDocumentTarget(documentDataName, targets);
            verifyProgressTarget(targets);
            verifyUniqueIDTarget(documentDataName, targets);
            verifyToBeCodedTarget(documentDataName, targets);

            expect(targets.pendingAttachments).to.be.empty;
            verifyAffectedFields(documentDataName, targets);
          })
          .end(done);
      });
    }

    it('should compute unique ID via the /mappings flow API', function (done) {
      request(api)
        .post('/mappings')
        .send({
          input: singleDocumentAPIInput(documentDataName),
          targets: [ 'uniqueID', 'computedExpressions' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          expect(res.body.targets).to.have.property('computedExpressions');
          verifyUniqueIDTarget(documentDataName, res.body.targets);
          expect(res.body.targets.computedExpressions.uniqueID).to.equal(loadedFixtures[documentDataName].uniqueID);
        })
        .end(done);
    });

    exportCSV && it('should export document to CSV via the /mappings flow API', function (done) {
      request(api)
        .post('/mappings')
        .send({
          input: singleDocumentAPIInput(documentDataName),
          targets: [ 'csv' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          verifyTarget(res.body.targets, 'csv', loadedFixtures[csvDataName]);
        })
        .end(done);
    });

    exportCSV && it('should export document to stable CSV via the /mappings flow API', function (done) {
      var expected = loadedFixtures[stableCsvDataName];
      // when the test case does not contain stable fields, we avoid the need for
      // an "empty" csv file
      if (typeof expected === 'undefined') {
        expected = '\n\n'; // this value is the correct csv result when there are no stable fields
      }
      request(api)
        .post('/mappings')
        .send({
          input: singleDocumentAPIInput(documentDataName),
          targets: [ 'stableCsv' ]
        })
        .expect('Content-Type', 'application/json')
        .expect(200)
        .expect(function (res) {
          expect(res.body).to.have.property('targets');
          verifyTarget(res.body.targets, 'stableCsv', expected);
        })
        .end(done);
    });
  }

});
