/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var request = require('supertest');
var mappingsAPI = require('../../lib/mappings/api');

describe('/mappings API', function () {

  var api;

  beforeEach(function () {
    api = mappingsAPI();
  });

  it('should respond to a status request', function (done) {
    request(api)
      .get('/mappings/status')
      .expect(204)
      .end(done);
  });

  it('should check for missing parameter "input"', function (done) {
    request(api)
      .post('/mappings')
      .send({ targets: [ 'something' ] })
      .expect('Content-Type', 'application/json')
      .expect(400, {
        code: 'BadRequestError',
        message: 'Missing parameters "input" and "targets"'
      })
      .end(done);
  });

  it('should check for missing parameter "targets"', function (done) {
    request(api)
      .post('/mappings')
      .send({ input: {} })
      .expect('Content-Type', 'application/json')
      .expect(400, {
        code: 'BadRequestError',
        message: 'Missing parameters "input" and "targets"'
      })
      .end(done);
  });

  it('should check for missing input "dataCollectionDefinition"', function (done) {
    request(api)
      .post('/mappings')
      .send({ input: {}, targets: [ 'dataCollectionDefinition' ] })
      .expect('Content-Type', 'application/json')
      .expect(400, {
        code: 'BadRequestError',
        message: 'Error: Missing or invalid input parameter "dataCollectionDefinition"'
      })
      .end(done);
  });

  it('should check for missing parameter "document"', function (done) {
    request(api)
      .post('/mappings')
      .send({
        input: {
          dataCollectionDefinition: {}
        },
        targets: [ 'document' ]
      })
      .expect('Content-Type', 'application/json')
      .expect(400, {
        code: 'BadRequestError',
        message: 'Error: Missing or invalid input parameter "document"'
      })
      .end(done);
  });

  it('should refuse unsupported targets', function (done) {
    request(api)
      .post('/mappings')
      .send({ input: {}, targets: [ 'whatever' ] })
      .expect('Content-Type', 'application/json')
      .expect(400, {
        code: 'BadRequestError',
        message: 'Error: Unrecognized target'
      })
      .end(done);
  });

  it('should return 400 when the data collection definition cannot be parsed', function (done) {
    request(api)
      .post('/mappings')
      .send({
        input: {
          dataCollectionDefinition: 'this is not JSON'
        },
        targets: [ 'dataCollectionDefinition' ]
      })
      .expect('Content-Type', 'application/json')
      .expect(400)
      .expect(function (res) {
        expect(res.body.code).to.equal('BadRequestError');
        expect(res.body.message).to.have.string('Error: Missing or invalid input parameter "dataCollectionDefinition"');
      })
      .end(done);
  });

});
