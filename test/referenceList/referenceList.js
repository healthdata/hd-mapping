/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var nock = require('nock');

var config = require('../../config');
var referenceList = require('../../lib/referenceList/referenceList');

describe('Reference lists', function () {

  var refList;
  var dataReference;

  var WORKFLOW_ENGINE_HOST = 'http://localhost:8080';
  var WORKFLOW_ENGINE_PATH = '/healthdata_hd4dp';
  var DATA_REFERENCE_PATH = WORKFLOW_ENGINE_PATH + '/datareference';
  var DATA_REFERENCE_URL = WORKFLOW_ENGINE_HOST + DATA_REFERENCE_PATH;

  beforeEach(function () {
    refList = referenceList('ABC', function () { return 'en'; });
  });

  beforeEach(function () {
    dataReference = nock(WORKFLOW_ENGINE_HOST);
  });

  afterEach(function () {
    nock.cleanAll();
  });

  it('should use the base URL from the config', function () {
    expect(config.WORKFLOW_ENGINE_BASE_URL)
      .to.equal(WORKFLOW_ENGINE_HOST + WORKFLOW_ENGINE_PATH);
  });

  it('should perform a http request based on the configuration', function (done) {
    dataReference
      .get(DATA_REFERENCE_PATH + '?language=en&parentCode=1234&type=ABC')
      .reply(200, [ 1, 2, 3 ]);
    refList.children(1234, function (err, result) {
      if (err) { return done(err); }
      expect(result).to.deep.equal([ 1, 2, 3 ]);
      dataReference.done();
      done();
    });
  });

  it('should report a non-200 response status code', function (done) {
    dataReference
      .get(DATA_REFERENCE_PATH + '?language=en&value=abc&filter=EXACT&type=ABC')
      .reply(500, 'explanation');
    refList.exactValue('abc', function (err) {
      expect(function () { throw err; })
        .to.throw('Request to ' + DATA_REFERENCE_URL + '?language=en&value=abc&filter=EXACT&type=ABC returned 500 and');
      dataReference.done();
      done();
    });
  });

  it('should report invalid JSON repsonses', function (done) {
    dataReference
      .get(DATA_REFERENCE_PATH + '?language=en&code=xyz&type=ABC')
      .reply(200, 'not JSON');
    refList.exactCode('xyz', function (err) {
      expect(function () { throw err; })
        .to.throw('Request to ' + DATA_REFERENCE_URL + '?language=en&code=xyz&type=ABC returned invalid JSON: not JSON');
      dataReference.done();
      done();
    });
  });

});
