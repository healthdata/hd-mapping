/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var basicState = require('../../lib/referenceList/basicState');

describe('referenceList basicState', function () {

  var state, refList, notify;

  beforeEach(function () {
    refList = {
      exactCodeAllLanguages: function (code, cb) {
        var match = { code: code, value: 'Hello World!' };
        return cb(undefined, [ match ]);
      }
    };
    notify = sinon.spy();
    state = basicState(refList, notify);
  });

  it('should invoke notify on becomeCode', function () {
    state.becomeCode('abc');
    expect(notify).to.have.been.calledOnce;
  });

  it('should lookup exact match on becomeCode', function () {
    var exactCodeAllLanguages = sinon.spy(refList, 'exactCodeAllLanguages');
    state.becomeCode('abc');
    expect(exactCodeAllLanguages).to.have.been.calledOnce;
    expect(exactCodeAllLanguages).to.have.been.calledWith('abc');
  });

  it('should not lookup match twice for the same code', function () {
    var exactCodeAllLanguages = sinon.stub(refList, 'exactCodeAllLanguages').returns();
    state.becomeCode('abc');
    state.becomeCode('abc');
    expect(exactCodeAllLanguages).to.have.been.calledOnce;
    expect(exactCodeAllLanguages.withArgs('abc')).to.have.been.calledOnce;
  });

});
