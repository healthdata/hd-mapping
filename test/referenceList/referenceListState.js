/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var url = require('url');

// var referenceListState = require('../../lib/referenceList/referenceListState');

describe.skip('referenceListState', function () {

  var dataReferenceResponses = {};

  function dataReferenceResponse(query, data) {
    dataReferenceResponses[query] = data;
  }

  function httpGET(uri, cb) {
    var query = url.parse(uri).query;
    var result = dataReferenceResponses[query];
    if (typeof result === 'undefined') {
      throw new Error('No data for "' + query + '"');
    }
    return cb(undefined, result);
  }

  var manipulator, refListState, refList;

  beforeEach(function () {
    manipulator = {
      description: {
        assertions: {},
        messages: {}
      },
      invalidate: function () {},
      edit: function (f) { f(); },
      clear: function () {}
    };
  });

  beforeEach(function () {
    refList = {
      autocomplete: function (query, cb) { return cb(undefined, []); }
    };
  });

  beforeEach(function () {
    refListState = referenceListState.basicState(manipulator, refList);
  });

  it('should add a query validation to the manipulator', function () {
    expect(manipulator.description.assertions).to.have.property('queryMatch');
    expect(manipulator.description.messages).to.have.property('queryMatch');

    var queryMatch = manipulator.description.assertions.queryMatch;
    expect(queryMatch.check()).to.equal('pending');
    refListState.query = 'fubar';
    expect(queryMatch.check()).to.equal('failed');
    // the validation only checks query being an object
    // it is assumed that the object denotes a valid reference list match
    refListState.query = {};
    expect(queryMatch.check()).to.equal('ok');
    refListState.query = '';
    expect(queryMatch.check()).to.equal('pending');
  });

  it('should clear the manipulator on a query modification', function () {
    var clear = sinon.spy(manipulator, 'clear');
    refListState.query = 'bla';
    expect(clear).to.have.been.calledOnce;
  });

  it('should invoke onAutocomplete on autocompletion', function () {
    var cb = function () {};
    var onAutocomplete = sinon.spy(refListState, 'onAutocomplete');
    refListState.autocompleteFor('abc', cb);
    expect(onAutocomplete).to.have.been.calledOnce;
    expect(onAutocomplete).to.always.have.been.calledWithExactly(cb);
  });

  it('should invoke the callback on autocompletion', function () {
    var cb = sinon.spy(function () {});
    refListState.autocompleteFor('abc', cb);
    expect(cb).to.have.been.calledOnce;
    expect(cb).to.have.been.calledWithExactly(undefined, []);
  });

});
