/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var autocomplete = require('../../lib/referenceList/autocomplete');

describe('autocomplete', function () {

  var refList, base;

  beforeEach(function () {
    base = {};
    refList = {};
  });

  describe('simpleAutocompleteMixin()', function () {

    beforeEach(function () {
      autocomplete.simpleAutocompleteMixin(base, refList);
    });

    it('should install autocompleteFor()', function () {
      expect(base).to.respondTo('autocompleteFor');
    });

    it('should simply refer to the reference list', function () {
      var spy = refList.autocomplete = sinon.spy();

      function cb() {}
      base.autocompleteFor('abc', cb);

      expect(spy).to.have.been.calledOnce;
      expect(spy).to.have.been.calledWithExactly('abc', cb);
    });

  });

  describe('filteredAutocompleteMixin()', function () {

    beforeEach(function () {
      base.currentMatch = sinon.stub();
      base.clear = sinon.stub();
      autocomplete.filteredAutocompleteMixin(base, refList);
    });

    it('should install autocompleteFor()', function () {
      expect(base).to.respondTo('autocompleteFor');
    });

    it('should refer to the reference list', function () {
      var spy = refList.autocomplete = sinon.spy();

      function cb() {}
      base.autocompleteFor('abc', cb);

      expect(spy).to.have.been.calledOnce;
      expect(spy).to.have.been.calledWith('abc');
    });

    it('should support filter activation', function () {

      refList.autocomplete = function (code, cb) {
        cb(undefined, [ { code: 3 }, { code: 9 }, { code: 7 } ]);
      };

      expect(base).to.respondTo('activateFilter');
      base.activateFilter(function (match) { return match.code > 5; });

      var cb = sinon.spy();
      base.autocompleteFor('abc', cb);

      expect(cb).to.have.been.calledWith(undefined, [ { code: 9 }, { code: 7 } ]);
    });

  });

});
