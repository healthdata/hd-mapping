/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var url = require('url');

var referenceList = require('../../lib/referenceList/referenceList');
var referenceTree = require('../../lib/referenceList/referenceTree');

describe('referenceTree', function () {

  var refList, updateFinalValue, updateNonFinalValue, refTree, httpGET;
  var lang = 'en';

  var dataReferenceResponses = {};

  function dataReferenceResponse(query, data) {
    dataReferenceResponses[query] = data;
  }

  function httpGET(uri, cb) {
    var query = url.parse(uri).query;
    var result = dataReferenceResponses[query];
    if (typeof result === 'undefined') {
      throw new Error('No data for "' + query + '"');
    }
    return cb(undefined, result);
  }

  beforeEach(function () {
    function getLang() { return lang; }
    refList = referenceList('ABC', getLang, httpGET);
    updateFinalValue = sinon.spy();
    updateNonFinalValue = sinon.spy();
  });

  var roots = [
    { code: 1, value: 'A' },
    { code: 2, value: 'B' },
    { code: 3, value: 'C' }
  ];

  it('should initialize with the tree root', function () {
    dataReferenceResponse('parentCode=NULL&type=ABC&language=en',
      roots);
    refTree = referenceTree(refList, updateFinalValue, updateNonFinalValue);
    expect(refTree.currentNode).to.equal(refTree.treeRoot);
  });

});
