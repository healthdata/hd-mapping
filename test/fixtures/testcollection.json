{
  "label": "TESTCOLLECTION",
  "sections": [
    {
      "label": "Section 1 - Introduction",
      "name": "section_1__introduction",
      "fields": [
        {
          "label": "Text",
          "name": "text",
          "type": "text",
          "default": "Default text",
          "required": true,
          "help": "Required fields are indicated with an asterisk.",
          "inputsize": "200",
          "sticky": true,
          "db_type": "TEST"
        },
        {
          "label": "Date",
          "name": "date",
          "type": "date",
          "help": "This date cannot be a future date.",
          "db_type": "TEST"
        },
        {
          "label": "Boolean",
          "name": "boolean",
          "type": "boolean",
          "db_table": "TEST",
          "db_type": "boolean"
        },
        {
          "label": "Number",
          "name": "number",
          "type": "number",
          "help": "This number must be positive.",
          "db_type": "TEST",
          "validators": [
            {
              "name": "numpos",
              "minimum": 0,
              "errorMessage": "Number cannot be negative",
              "level": "error"
            },
            {
              "name": "numlarge",
              "maximum": 100,
              "errorMessage": "Number is larger than 100. Are you sure?",
              "level": "warning"
            }
          ]
        },
        {
          "label": "Calculated number",
          "name": "calculated_number",
          "type": "number",
          "read-only": true,
          "help": "This field is read-only. It contains the value of the above number divided by 10.",
          "computedWith": "DIV10",
          "db_type": "TEST"
        },
        {
          "label": "Multiline",
          "name": "multiline",
          "type": "multiline",
          "db_table": "TEST",
          "db_type": "char",
          "db_format": "500"
        }
      ]
    },
    {
      "label": "Section 2 - Advanced",
      "name": "section_2__advanced",
      "sections": [
        {
          "label": "Section 2.1 - Fieldset",
          "name": "section_21__fieldset",
          "fields": [
            {
              "label": "Fieldset",
              "name": "fieldset",
              "type": "fieldset",
              "help": "A fieldset is a simple group of fields (below section level)",
              "fields": [
                {
                  "label": "Number",
                  "name": "number",
                  "type": "number",
                  "db_table": "TEST",
                  "db_type": "number",
                  "db_format": "20"
                },
                {
                  "label": "Multiline",
                  "name": "multiline",
                  "type": "multiline",
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "500"
                }
              ]
            }
          ]
        },
        {
          "label": "Section 2.2 - Choice and multichoice lists",
          "name": "section_22__choice_and_multichoice_lists",
          "fields": [
            {
              "label": "Simplechoice list title (without nested fields)",
              "name": "simplechoice_list_title_without_nested_fields",
              "type": "choice",
              "choices": [
                {
                  "label": "Yes",
                  "name": "1"
                },
                {
                  "label": "No",
                  "name": "2"
                },
                {
                  "label": "Unknown",
                  "name": "3"
                }
              ],
              "db_table": "TEST",
              "db_type": "char",
              "db_format": "1"
            },
            {
              "label": "Choice list (with nested fields)",
              "name": "choice_list_with_nested_fields",
              "type": "choice",
              "db_table": "TEST",
              "db_type": "char",
              "db_format": "1",
              "choices": [
                {
                  "label": "Yes",
                  "name": "1",
                  "fields": [
                    {
                      "label": "Nested text field",
                      "name": "nested_text_field",
                      "type": "text",
                      "db_table": "TEST",
                      "db_type": "char",
                      "db_format": "1"
                    }
                  ]
                },
                {
                  "label": "No",
                  "name": "2"
                },
                {
                  "label": "Unknown",
                  "name": "3"
                }
              ]
            },
            {
              "label": "Multichoice list",
              "name": "multichoice_list",
              "type": "multichoice",
              "db_table": "TEST",
              "db_type": "boolean",
              "choices": [
                {
                  "label": "Multichoice option 1",
                  "name": "multichoice_option_1",
                  "db_table": "TEST",
                  "db_type": "boolean"
                },
                {
                  "label": "Multichoice option 2",
                  "name": "multichoice_option_2",
                  "db_table": "TEST",
                  "db_type": "boolean"
                },
                {
                  "label": "Multichoice option 3",
                  "name": "multichoice_option_3",
                  "db_table": "TEST",
                  "db_type": "boolean"
                }
              ]
            }
          ]
        },
        {
          "label": "Section 2.3 - Repeatable List",
          "name": "section_23__repeatable_list",
          "fields": [
            {
              "label": "Repeatable list title",
              "name": "repeatable_list_title",
              "type": "list",
              "fields": [
                {
                  "label": "Number",
                  "name": "number",
                  "type": "number",
                  "db_table": "TEST_LIST",
                  "db_type": "number",
                  "db_format": "20"
                },
                {
                  "label": "Multiline",
                  "name": "multiline",
                  "type": "multiline",
                  "db_table": "TEST_LIST",
                  "db_type": "char",
                  "db_format": "500"
                }
              ]
            }
          ]
        },
        {
          "label": "Section 2.4 - Questionnaire",
          "name": "section_24__questionnaire",
          "computedExpressions": {
            "QUEST": "$['boolean_questionnaire'] === true"
          },
          "fields": [
            {
              "label": "Boolean questionnaire",
              "name": "boolean_questionnaire",
              "type": "boolean",
              "db_table": "TEST",
              "db_type": "boolean"
            },
            {
              "label": "Questionnaire title",
              "name": "questionnaire_title",
              "type": "questionnaire",
              "answers": [
                {
                  "label": "Answer 1",
                  "name": "1"
                },
                {
                  "label": "Answer 2",
                  "name": "2"
                },
                {
                  "label": "Answer 3",
                  "name": "3"
                },
                {
                  "label": "Answer 4",
                  "name": "4"
                }
              ],
              "questions": [
                {
                  "label": "Question 1",
                  "name": "question_1",
                  "type": "questionnaire",
                  "onlyWhen": [
                    "QUEST"
                  ],
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "1"
                },
                {
                  "label": "Question 2",
                  "name": "question_2",
                  "type": "questionnaire",
                  "onlyWhen": [
                    "QUEST"
                  ],
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "1"
                },
                {
                  "label": "Question 3",
                  "name": "question_3",
                  "type": "questionnaire",
                  "onlyWhen": [
                    "QUEST"
                  ],
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "1"
                },
                {
                  "label": "Question 4",
                  "name": "question_4",
                  "type": "questionnaire",
                  "onlyWhen": [
                    "QUEST"
                  ],
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "1"
                }
              ]
            }
          ]
        },
        {
          "label": "Section 2.5 - Patient ID",
          "name": "section_25__patient_id",
          "fields": [
            {
              "label": "Patient ID",
              "name": "patient_id",
              "type": "patientID",
              "inputsize": "30",
              "db_table": "TEST",
              "db_type": "char",
              "db_format": "50",
              "fields": [
                {
                  "label": "Name",
                  "name": "name",
                  "inputsize": "30",
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "50"
                },
                {
                  "label": "First name",
                  "name": "first_name",
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "50"
                },
                {
                  "label": "Date of birth",
                  "name": "date_of_birth",
                  "db_table": "TEST",
                  "db_type": "date"
                },
                {
                  "label": "Sex",
                  "name": "sex",
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "1"
                },
                {
                  "label": "Place of residence",
                  "name": "place_of_residence",
                  "inputsize": "67",
                  "db_table": "TEST",
                  "db_type": "char",
                  "db_format": "5"
                },
                {
                  "label": "Deceased?",
                  "name": "deceased",
                  "db_table": "TEST",
                  "db_type": "boolean"
                },
                {
                  "label": "Date of death",
                  "name": "date_of_death",
                  "db_table": "TEST",
                  "db_type": "date"
                }
              ]
            }
          ]
        }
      ]
    }
  ],
  "conditions": [
    {
      "name": "futuredate",
      "expression": "typeof $['date'] === 'string'?new Date($['date']) <= new Date():true",
      "errorMessage": "{{Date cannot be a future date|['date']}}",
      "level": ""
    }
  ],
  "computedExpressions": {
    "DIV10": "$['number']/10"
  }
}