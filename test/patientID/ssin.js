/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var ssin = require('../../lib/patientID/ssin');

describe('SSIN', function () {

  it('should reject anything that is not a string', function () {
    expect(ssin.isValidSSIN(undefined)).to.be.false;
    expect(ssin.isValidSSIN(null)).to.be.false;
    expect(ssin.isValidSSIN(12345678958)).to.be.false;
    expect(ssin.isValidSSIN({})).to.be.false;
    expect(ssin.isValidSSIN(NaN)).to.be.false;
  });

  it('should recognize valid SSIN numbers', function () {
    expect(ssin.isValidSSINbis('12345678958')).to.be.true;
    expect(ssin.isValidSSIN('93061411307')).to.be.true;
  });

  it('should support the Y2K extension', function () {
    expect(ssin.isValidSSINbis('12345678987')).to.be.true;
  });

  it('should reject invalid SSIN numbers', function () {
    expect(ssin.isValidSSIN('12345678959')).to.be.false;
    expect(ssin.isValidSSIN('92345678958')).to.be.false;
    expect(ssin.isValidSSIN('1')).to.be.false;
    expect(ssin.isValidSSIN('1234567895899')).to.be.false;
    expect(ssin.isValidSSIN('')).to.be.false;
    expect(ssin.isValidSSIN('9306141137')).to.be.false;
  });

  it('should *not* ignore non-numeric input', function () {
    // WDC-1697
    // We only consider "canonical" SSINs valid, i.e., without dots or dashes.
    // This is to ensure that SSINs can safely be used as a unique ID.
    expect(ssin.isValidSSIN('12.34.56-789-58')).to.be.false;
    expect(ssin.isValidSSIN('@12XYZ34?/56<>789+=58#')).to.be.false;
  });

  it('should extract the date of birth', function () {
    expect(ssin.extractDateOfBirth('93061411307')).to.deep.equal(new Date('1993/06/14'));
    expect(ssin.extractDateOfBirth('12345678958')).to.be.undefined;
  });

});
