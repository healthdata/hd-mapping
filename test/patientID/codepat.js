/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var codepat = require('../../lib/patientID/codepat');
var _ = require('underscore');

describe('codepat', function () {

  it('should reject anything that is not a string', function () {
    expect(codepat.isValidCodepat(undefined)).to.be.false;
    expect(codepat.isValidCodepat(null)).to.be.false;
    expect(codepat.isValidCodepat(12345678958)).to.be.false;
    expect(codepat.isValidCodepat({})).to.be.false;
    expect(codepat.isValidCodepat(NaN)).to.be.false;
  });

  it('should recognize valid codepat identifiers', function () {
    expect(codepat.isValidCodepat('19871230MATWM')).to.be.true;
    expect(codepat.isValidCodepat('20110102lkqwF')).to.be.true;
    expect(codepat.isValidCodepat('20110102lkqw?')).to.be.true;
  });

  it('should reject based on length', function () {
    expect(codepat.isValidCodepat('123')).to.be.false;
    expect(codepat.isValidCodepat('XYZ')).to.be.false;
  });

  it('should reject when the start is not a date', function () {
    expect(codepat.isValidCodepat('ABCD1201PIDCM')).to.be.false;
    expect(codepat.isValidCodepat('20009999PIDCM')).to.be.false;
  });

  it('should reject when the sex is not M or F', function () {
    expect(codepat.isValidCodepat('19871230MATWT')).to.be.false;
  });

  describe('generateCodepat', function () {

    var someDate = '1987-12-01T14:03:53+01:00';
    var invalidDate = 'not a date';

    it('should check for invalid dates', function () {
      expect(codepat.generateCodepat('Foo', 'Vanden Bar', invalidDate, 'M'))
        .to.be.undefined;
    });

    it('should only accept M or F or U as sex', function () {
      expect(codepat.generateCodepat('Foo', 'Vanden Bar', someDate, 'abc'))
        .to.be.undefined;
      expect(codepat.generateCodepat('Foo', 'Vanden Bar', someDate, 123))
        .to.be.undefined;
    });

    it('should accept names of a single letter', function () {
      expect(codepat.generateCodepat('A', 'B', someDate, 'F'))
        .to.equal('19871201B?A?F');
    });

    var generated = [
      {
        birthdate: '1999-06-23T00:00:00+01:00',
        firstName: 'Pipo',
        lastName: 'De Clown',
        sex: 'M',
        result: '19990623DCPIM'
      },
      {
        birthdate: '1987-12-01T00:00:00-01:00',
        firstName: 'Pipo',
        lastName: 'De Clown',
        sex: 'M',
        result: '19871201DCPIM'
      },
      {
        birthdate: '1987-12-01',
        firstName: 'Tralala',
        lastName: 'Vande Walle',
        sex: 'F',
        result: '19871201VWTRF'
      },
      {
        birthdate: someDate,
        sex: 'F',
        result: '19871201????F'
      },
      {
        birthdate: someDate,
        sex: 'U',
        result: '19871201?????'
      },
      {
        birthdate: someDate,
        firstName: 'Tralala la',
        lastName: 'Vandewalle',
        sex: 'F',
        result: '19871201VWTRF'
      },
      {
        birthdate: someDate,
        firstName: ' Tralala-la',
        lastName: 'Vandewalle',
        sex: 'F',
        result: '19871201VWTLF'
      },
      {
        birthdate: someDate,
        firstName: 'Bénedict',
        lastName: 'ÄFebe',
        sex: 'F',
        result: '19871201AFBEF'
      },
      {
        birthdate: someDate,
        firstName: ' d\'ilalu',
        lastName: 'l\'Alala',
        sex: 'F',
        result: '19871201LADIF'
       }
    ];

    it('should generate valid codepat identifiers', function () {
      _.each(generated, function (g) {
        expect(codepat.isValidCodepat(g.result)).to.be.true;
        expect(codepat.generateCodepat(g.firstName, g.lastName, g.birthdate, g.sex))
          .to.equal(g.result);
      });
    });

  });

});
