/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var documentmodel = require('documentmodel');

var documentmodelHelpers = require('../../lib/documentmodelHelpers');

var makeHDDataSource = require('../../lib/makeHDDataSource');

describe('Patient identification directives', function () {

  var env, document, manipulator, description, target, patientID, workflowPropsMock;

  function compileInput(input) {
    documentmodelHelpers.compile(input, env);
  }

  function manipulate(target, patientID) {
    if (!env.skrDescription) {
      throw new Error('no description! cannot proceed with test case');
    }
    var dataSource = makeHDDataSource({
      content: target,
      patientID: patientID
    });
    workflowPropsMock = dataSource.hdWorkflowProps();
    var isPseudonymised = dataSource.isPseudonymised();
    document = documentmodel.makeDocument(dataSource, env.skrDescription,
      { isPseudonymised: isPseudonymised });
    manipulator = document.root;
    description = manipulator.description;
  }

  function initialize(input, someTarget, somePatientID) {
    target = someTarget || {};
    patientID = somePatientID || {};
    compileInput(input);
    manipulate(target, patientID);
  }

  beforeEach(function () {
    env = {};
  });

  describe('hdPatientID', function () {

    describe('without a coded id', function () {

      beforeEach(function () {
        initialize({
          type: 'patientID',
          required: true,
          fields: [
            { type: 'number', name: 'someNumber' },
            { type: 'number', name: 'someOtherNumber', keep: true }
          ]
        });
      });

      it('should initialize the method used', function () {
        expect(manipulator.getMethod()).to.equal('other');
        expect(target.value).to.have.property('%method', 'other');
      });

      it('should initialize generated to false', function () {
        expect(manipulator.patientIDisGenerated()).to.be.false;
        expect(target.value).to.have.property('%generated', false);
      });

      it('should store the value in the workflow props', function () {
        manipulator.value = 'abc123';
        expect(manipulator.value).to.equal('abc123');
        expect(workflowPropsMock.get('PATIENT_IDENTIFIER')).to.equal('abc123');
      });

      it('should accept undefined as value', function () {
        manipulator.value = undefined;
        expect(manipulator.value).to.be.undefined;
        expect(workflowPropsMock.get('PATIENT_IDENTIFIER')).to.be.undefined;
      });

      it('should store nested fields only in the workflow props', function () {
        manipulator.propertyManipulators.someNumber.value = 42;
        expect(workflowPropsMock.get('someNumber')).to.equal(42);
        expect(manipulator.read()).to.not.have.property('someNumber');
      });

      it('should store kept nested fields both in the workflow props and the document', function () {
        manipulator.propertyManipulators.someOtherNumber.value = 42;
        expect(workflowPropsMock.get('someOtherNumber')).to.equal(42);
        expect(manipulator.read()).to.have.property('someOtherNumber', 42);
      });

      it('should *not* clear kept nested fields when SSIN is used', function () {
        // This behavior will change in the future, when these fields should be cleared.
        // Then there should also be a test
        // 'should restore kept nested fields when SSIN is changed'
        manipulator.propertyManipulators.someOtherNumber.value = 42;
        manipulator.value = '93061411307';
        expect(manipulator.getMethod()).to.equal('SSIN');
        expect(manipulator.read()).to.have.property('someOtherNumber', 42);
      });

      it('should recognize SSIN numbers', function () {
        manipulator.value = '93061411307';
        expect(manipulator.getMethod()).to.equal('SSIN');
      });

      it('should recognize SSINbis numbers', function () {
        manipulator.value = '12345678958';
        expect(manipulator.getMethod()).to.equal('SSINbis');
      });

      it('should recognize codepat identifiers', function () {
        manipulator.value = '19871230MATWM';
        expect(manipulator.getMethod()).to.equal('codepat');
      });

      it('should generate a codepat using info from the workflow props', function () {
        workflowPropsMock.set('date_of_birth', '2014-12-05T08:55:18+0100');
        workflowPropsMock.set('sex', { 'selectedOption': 'F' });
        // invalidation is needed to trigger recomputation
        manipulator.invalidate();
        expect(workflowPropsMock.get('codepat')).to.equal('20141205????F');
        expect(manipulator.value).to.be.undefined;
        expect(manipulator.getMethod()).to.equal('other');
        manipulator.acceptGeneratedCodepat();
        expect(manipulator.value).to.equal('20141205????F');
        expect(manipulator.getMethod()).to.equal('codepat');
        expect(target.value).to.have.property('%generated', true);
        workflowPropsMock.set('first_name', 'Foo');
        workflowPropsMock.set('name', 'Vanden Bar');
        // invalidation is needed to trigger recomputation
        manipulator.invalidate();
        expect(manipulator.value).to.equal('20141205VBFOF');
      });

      it('should clear the generated codepat when date of birth disappears', function () {
        workflowPropsMock.set('date_of_birth', '2014-12-05T08:55:18+0100');
        workflowPropsMock.set('sex', { 'selectedOption': 'F' });
        // invalidation is needed to trigger recomputation
        manipulator.invalidate();
        expect(workflowPropsMock.get('codepat')).to.equal('20141205????F');

        workflowPropsMock.set('date_of_birth', null);
        // invalidation is needed to trigger recomputation
        manipulator.invalidate();
        expect(target.value).to.have.property('%generated', false);
        expect(workflowPropsMock.get('codepat')).be.undefined;
      });

      it('should correctly indicate absence', function () {
        var manipulator = Object.create(Object.prototype, {
          value: {
            get: function () {
              return workflowPropsMock.get('PATIENT_IDENTIFIER', true);
            }
          }
        });
        expect(description.isEmpty({}, manipulator)).to.be.true;
        workflowPropsMock.set('PATIENT_IDENTIFIER', '123');
        expect(description.isEmpty({}, manipulator)).to.be.false;
      });

      it('should reject a non-SSIN identifier', function () {
        manipulator.value = 'ABC';
        expect(manipulator.errors).to.deep.equal([ 'patientID' ]);
        manipulator.value = '93061411307';
        expect(manipulator.errors).to.deep.equal([]);
      });

      it('should warn against using a generated indentifier', function () {
        workflowPropsMock.set('date_of_birth', '2014-12-05T08:55:18+0100');
        workflowPropsMock.set('sex', { 'selectedOption': 'F' });
        // invalidation is needed to trigger recomputation
        manipulator.invalidate();
        expect(manipulator.warnings).to.deep.equal([]);
        manipulator.acceptGeneratedCodepat();
        expect(manipulator.warnings).to.deep.equal([ 'generated' ]);
      });

      it('should accept an identifier when required', function () {
        workflowPropsMock.set('PATIENT_IDENTIFIER', '123');
        // note the absence of 'required' error
        expect(manipulator.errors).to.deep.equal([ 'patientID' ]);
      });

      it('should raise a validation error when patient id is required and missing', function () {
        workflowPropsMock.set('PATIENT_IDENTIFIER', '');
        expect(manipulator.errors).to.deep.equal([ 'required' ]);
      });

    });

    describe('with a coded id', function () {

      beforeEach(function () {
        initialize({
          type: 'patientID',
          required: true,
          fields: [
            { type: 'number', name: 'someNumber' },
            { type: 'number', name: 'someOtherNumber', keep: true }
          ]
        },
        {},
        {
          CODED_PATIENT_IDENTIFIER: 'abc',
          someNumber: '42'
        });
      });

      it('should indicate having a coded id', function () {
        expect(manipulator.isPseudonymised).to.be.true;
        expect(manipulator.patientIDisCoded).to.be.true;
      });

      it('should make nested manipulators inactive', function () {
        expect(manipulator.propertyManipulators.someNumber.isActive()).to.be.false;
      });

      it('should accept a coded identifier when required', function () {
        expect(manipulator.errors).to.have.lengthOf(0);
      });

      it('should raise a validation error when coded id is required and missing', function () {
        workflowPropsMock.set('CODED_PATIENT_IDENTIFIER', '');
        expect(manipulator.errors).to.deep.equal([ 'required' ]);
      });

    });

  });

  describe('Full patient ID', function () {

    var input;
    var lastNameManipulator, firstNameManipulator, birthdateManipulator,
        placeOfResidenceManipulator, sexManipulator, deceasedManipulator,
        deathdateManipulator, internalPatientIDManipulator;

    before(function () {
      input = {
        type: 'patientID',
        fields: [
          {
            name: 'place_of_residence',
            required: true
          }
        ]
      };
    });

    beforeEach(function () {
      initialize(input);
      internalPatientIDManipulator = manipulator.propertyManipulators.internal_patient_id;
      lastNameManipulator = manipulator.propertyManipulators.name;
      firstNameManipulator = manipulator.propertyManipulators.first_name;
      birthdateManipulator = manipulator.propertyManipulators.date_of_birth;
      sexManipulator = manipulator.propertyManipulators.sex;
      placeOfResidenceManipulator = manipulator.propertyManipulators.place_of_residence;
      deceasedManipulator = manipulator.propertyManipulators.deceased;
      deathdateManipulator = manipulator.propertyManipulators.date_of_death;
    });

    it('should expose manipulators for all properties', function () {
      expect(internalPatientIDManipulator).to.be.ok;
      expect(lastNameManipulator).to.be.ok;
      expect(firstNameManipulator).to.be.ok;
      expect(birthdateManipulator).to.be.ok;
      expect(sexManipulator).to.be.ok;
      expect(placeOfResidenceManipulator).to.be.ok;
      expect(deceasedManipulator).to.be.ok;
      expect(deathdateManipulator).to.be.ok;
    });

    it('should have nested properties required only when no SSIN is provided', function () {
      expect(birthdateManipulator.errors).to.deep.equal([ 'required' ]);
      manipulator.value = '93061411307';
      expect(manipulator.getMethod()).to.equal('SSIN');
      expect(birthdateManipulator.errors).to.deep.equal([]);
    });

    it('should generate a codepat when sufficient info is provided', function () {
      sexManipulator.selectedOption = 'F';
      birthdateManipulator.value = new Date('Fri Dec 05 2014 08:55:18 GMT+0100 (CET)');
      expect(workflowPropsMock.get('codepat')).to.equal('20141205????F');

      expect(manipulator.getMethod()).to.equal('other');
      expect(manipulator.patientIDisGenerated()).to.be.false;

      manipulator.acceptGeneratedCodepat();

      expect(manipulator.getMethod()).to.equal('codepat');
      expect(manipulator.patientIDisGenerated()).to.be.true;
      expect(manipulator.value).to.equal('20141205????F');
      firstNameManipulator.value = 'Foo';
      lastNameManipulator.value = 'Van Bar';
      expect(manipulator.value).to.equal('20141205VBFOF');
    });

    it('should accept a correct codepat', function () {
      manipulator.value = '20141205????F';
      expect(manipulator.errors).to.deep.equal([ 'patientID' ]);
      sexManipulator.selectedOption = 'F';
      birthdateManipulator.value = new Date('Fri Dec 05 2014 08:55:18 GMT+0100 (CET)');
      expect(manipulator.errors).to.deep.equal([]);
      expect(workflowPropsMock.get('codepat')).to.equal('20141205????F');
      expect(manipulator.getMethod()).to.equal('codepat');
      expect(manipulator.patientIDisGenerated()).to.be.false;
      expect(manipulator.value).to.equal('20141205????F');
    });

    it('should make date of death dependent on deceased', function () {
      expect(deathdateManipulator.isActive()).to.be.false;
      deceasedManipulator.value = true;
      expect(deathdateManipulator.isActive()).to.be.true;
    });

    it('should process SSIN based enrichtment', function () {
      manipulator.enrichSSIN({
        inss: '12345678958',
        internalId: 'ABC123',
        lastName: 'Merckx',
        firstName: 'Robin',
        dateOfBirth: '1985-10-11 00:00:00',
        gender: 'MALE',
        district: '1830',
        deceased: true,
        dateOfDeath: '1995-10-11 00:00:00'
      });
      expect(manipulator.value).to.equal('12345678958');
      expect(internalPatientIDManipulator.value).to.equal('ABC123');
      expect(lastNameManipulator.value).to.equal('Merckx');
      expect(firstNameManipulator.value).to.equal('Robin');
      expect(birthdateManipulator.value).to.deep.equal(new Date('1985/10/11'));
      expect(sexManipulator.selectedOption).to.equal('M');
      expect(placeOfResidenceManipulator.getValue()).to.equal('1830');
      expect(deceasedManipulator.value).to.equal(true);
      expect(deathdateManipulator.value).to.deep.equal(new Date('1995/10/11'));
    });

    it('should derive the date of birth from a SSIN', function () {
      manipulator.value = '93061411307';
      expect(manipulator.getMethod()).to.equal('SSIN');
      expect(birthdateManipulator.value).to.be.undefined;
      manipulator.deriveDateOfBirthFromSSIN();
      expect(birthdateManipulator.value).to.deep.equal(new Date('1993/06/14'));
    });

    it('should not override the date of birth from a SSIN', function () {
      manipulator.value = '93061411307';
      expect(manipulator.getMethod()).to.equal('SSIN');
      birthdateManipulator.value = new Date('1987/11/11');
      manipulator.deriveDateOfBirthFromSSIN();
      expect(birthdateManipulator.value).to.deep.equal(new Date('1987/11/11'));
    });

    it('should mark the place of residence as required', function () {
      expect(placeOfResidenceManipulator.errors).to.deep.equal([ 'required' ]);
      placeOfResidenceManipulator.setValue(123);
      expect(placeOfResidenceManipulator.errors).to.deep.equal([]);
    });

    it('should store date of birth in document when SSIN is used', function () {
      manipulator.value = '93061411307';
      expect(manipulator.getMethod()).to.equal('SSIN');
      var now = new Date('Sat Mar 11 2000 15:27:12 GMT+0100 (CET)');
      birthdateManipulator.value = now;
      expect(manipulator.read())
        .to.have.property('date_of_birth', '2000-03-11');
    });

    it('should warn about a Belgian place of residence when place of residence is in Belgium', function () {
      placeOfResidenceManipulator.edit(function () { return { code: 1000 }; });
      expect(placeOfResidenceManipulator.hasCode()).to.be.true;
      expect(manipulator.warnings).to.deep.equal([ 'belgianResident' ]);
    });

    it('should not warn about a Belgian place of residence when place of residence is 999', function () {
      placeOfResidenceManipulator.edit(function () { return { code: '999' }; });
      expect(placeOfResidenceManipulator.hasCode()).to.be.true;
      expect(manipulator.warnings).to.deep.equal([]);
    });

    it('should not warn about a Belgian place of residence when place of residence is 9999', function () {
      placeOfResidenceManipulator.edit(function () { return { code: '9999' }; });
      expect(placeOfResidenceManipulator.hasCode()).to.be.true;
      expect(manipulator.warnings).to.deep.equal([]);
    });
  });

  describe('Patient ID with onlyWhen', function () {

    var input;
    var booleanManipulator, patientIDManipulator;
    var lastNameManipulator, firstNameManipulator, birthdateManipulator,
        placeOfResidenceManipulator, sexManipulator, deceasedManipulator,
        deathdateManipulator;

    before(function () {
      input = {
        fields: [
          {
            type: 'boolean',
            name: 'include_patientID'
          },
          {
            type: 'patientID',
            name: 'patientID',
            onlyWhen: 'zot'
          }
        ],
        computedExpressions: {
          zot: '$.include_patientID'
        }
      };
    });

    function initializePatientIDwithOnlyWhen(target) {
      initialize(input, target);
      booleanManipulator = manipulator.propertyManipulators.include_patientID;
      patientIDManipulator = manipulator.propertyManipulators.patientID;
      lastNameManipulator = patientIDManipulator.propertyManipulators.name;
      firstNameManipulator = patientIDManipulator.propertyManipulators.first_name;
      birthdateManipulator = patientIDManipulator.propertyManipulators.date_of_birth;
      sexManipulator = patientIDManipulator.propertyManipulators.sex;
      placeOfResidenceManipulator = patientIDManipulator.propertyManipulators.place_of_residence;
      deceasedManipulator = patientIDManipulator.propertyManipulators.deceased;
      deathdateManipulator = patientIDManipulator.propertyManipulators.date_of_death;
    }

    it('should work when the patient ID starts as incactive', function () {
      initializePatientIDwithOnlyWhen({ value: { include_patientID: false }});
      expect(patientIDManipulator.isActive()).to.be.false;
    });

    it('should clear patient ID fields when they becomes inactive', function () {
      initializePatientIDwithOnlyWhen();
      patientIDManipulator.value = 'XYX123';
      birthdateManipulator.value = new Date();
      expect(birthdateManipulator.value).to.be.an.instanceof(Date);
      booleanManipulator.value = false;
      expect(patientIDManipulator.isActive()).to.be.false;
      expect(birthdateManipulator.isActive()).to.be.false;
      expect(birthdateManipulator.value).to.be.undefined;
      expect(patientIDManipulator.value).to.equal('');
    });
  });

});
